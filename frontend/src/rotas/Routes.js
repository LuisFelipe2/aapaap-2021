import { Switch, Route } from "react-router-dom";
import { RoutesProfessor } from "./RoutsProfessor";
import { RoutesStudent } from "./RoutsStudent";

export function Routes() {
  return (
    <Switch>
      <Route path="/professor">
        <RoutesProfessor />
      </Route>
      <Route path="/student">
        <RoutesStudent />
      </Route>
      <Route path='/'>
        <RoutesStudent />
      </Route>
    </Switch>
  );
}
