import { Redirect, Route, Switch } from "react-router-dom";
import { SelecaoDeTurma } from "../ui/professor/pages/selecao_de_turmas";
import { SelecaoDeQuestao } from "../ui/professor/pages/selecao_de_questao";

import { LoginScreen } from "../ui/professor/pages/login/LoginScreen";
import { Procedimentos } from "../ui/professor/pages/procedimento";
import { Home } from "../ui/professor/pages/home";
import { SelecaoDeAjudas } from "../ui/professor/pages/selecao_de_ajuda";
import { Ajudas } from "../ui/professor/pages/ajuda"
import { SelecaoDeProcedimentos } from "../ui/professor/pages/selecao_de_procedimento";
import { Estrategias } from "../ui/professor/pages/estrategia";
import { Relatorio } from "../ui/professor/pages/relatorio";
import { useGlobalUser } from "../context/user.context";

function PrivateRoute({ children, ...props }) {
  const [user] = useGlobalUser();

  if (user === "")
    return <Redirect to="/professor/authentication" />;
  else
    return <Route {...props}>{children}</Route>;
}

export function RoutesProfessor() {
  
  return (
    <Switch>
      <Route path="/professor/authentication" exact>
        <LoginScreen />
      </Route>
      <PrivateRoute path="/professor/home" exact>
        <Home />
      </PrivateRoute>
      <PrivateRoute path="/professor/turma" exact>
        <SelecaoDeTurma />
      </PrivateRoute>
      <PrivateRoute path="/professor/questao" exact>
        <SelecaoDeQuestao />
      </PrivateRoute>
      <PrivateRoute path="/professor/ajuda" exact>
        <SelecaoDeAjudas />
      </PrivateRoute>
      <PrivateRoute path="/professor/ajuda/criar" exact>
        <Ajudas />
      </PrivateRoute>
      <PrivateRoute path="/professor/procedimento" exact>
        <SelecaoDeProcedimentos />
      </PrivateRoute>
      <PrivateRoute path="/professor/procedimento/criar" exact>
        <Procedimentos />
      </PrivateRoute>
      <PrivateRoute path="/professor/estrategia/criar" exact>
        <Estrategias />
      </PrivateRoute>
      <PrivateRoute path="/professor/relatorio" exact>
        <Relatorio />
      </PrivateRoute>
      <Route path="/">
        <LoginScreen />
      </Route>
    </Switch>
  );
}
