import { useState } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { useGlobalUser } from "../context/user.context";
import { LoginScreen } from "../ui/students/pages/login/LoginScreen";
import { RecuperarSenha } from "../ui/students/pages/recuperarSenha/index";
import { Home } from "../ui/students/pages/home/index";
import { Curso } from "../ui/students/pages/meus-cursos/index";
import { QuestaoManager } from "../ui/students/pages/appAAP/index";
import { ScreenHelp } from "../ui/students/pages/help-screen/index";
import { ScreenProcedimentos } from "../ui/students/pages/procedimentos-screen/index";
import { ScreenEstrategia } from "../ui/students/pages/estrategias/index"

function PrivateRoute({ children, ...props }) {
  const [user] = useGlobalUser();

  if (user === "")
    return <Redirect to="/authentication" />;
  else
    return <Route {...props}>{children}</Route>;
}

export function RoutesStudent() {
  const [idQuestao, setIdQuestao] = useState();

  return (
    <Switch>
      <Route path="/authentication" exact>
        <LoginScreen />
      </Route>
      <Route path="/resetSenha" >
        <RecuperarSenha />
      </Route>
      <PrivateRoute path="/home" exact>
        <Home />
      </PrivateRoute>
      <PrivateRoute path="/atividades" exact>
        <Curso setIdQuestao={setIdQuestao} />
      </PrivateRoute>
      <PrivateRoute path="/questao" exact>
        <QuestaoManager idQuestao={idQuestao} />
      </PrivateRoute>
      <PrivateRoute path="/ajudas" exact>
        <ScreenHelp />
      </PrivateRoute>
      <PrivateRoute path="/procedimentos" exact>
        <ScreenProcedimentos />
      </PrivateRoute>
      <PrivateRoute path="/estrategia" exact>
        <ScreenEstrategia />
      </PrivateRoute>
      <Route path="/" exact>
        <LoginScreen />
      </Route>
    </Switch>
  );
}
