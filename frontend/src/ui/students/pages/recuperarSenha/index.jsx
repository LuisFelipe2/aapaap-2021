import { Button } from "@mui/material";
import { useState } from "react";
import { api } from "../../../../requestApi/api";
import { CampoSenha } from "../login/componentes.login.screen/componentes.box.login.screen/CampoSenha";
import { CampoConfirmarSenha } from "../login/componentes.login.screen/componentes.box.login.screen/CampoConfirmarSenha";
import { useHistory } from "react-router-dom";


export function RecuperarSenha(){
  const queryParams = new URLSearchParams(window.location.search);

  const [senha, setSenha] = useState("")
  const [confirmarSenha, setConfirmarSenha] = useState("")

  const useApi = api()
  const {push} = useHistory()
  const token = queryParams.get("token")


  function handleChange(event) {
    const { name, value } = event.target;
    if (name === "senha") setSenha(value);
    else if (name === "confirmarSenha")setConfirmarSenha(value);
  }

  function handleClick(){
    if(senha !== "" && senha === confirmarSenha)
      resetSenha()
    else alert("As senhas não estão iguai")
  }

  async function resetSenha(){
    try{
      await useApi.resetarSenha({token, senha})
      alert("Senha alterada com sucesso")
      push("/authentication")
    }catch(error){
      alert("Não foi possível alterar senha")
      console.log("Erro ao tentar alterar senha: ", error)
    }
  }

  return(
    <div>
      <div className="container-loginScreen">
      <h1>appAAP</h1>
      <p>Digite uma nova senha</p>
      <CampoSenha value={senha} changeValue={handleChange} />
      <CampoConfirmarSenha value={confirmarSenha} changeValue={handleChange} />
      <input hidden value={token}/>
      <Button onClick={handleClick}>Alterar senha</Button>
      </div>
    </div>
  )
}