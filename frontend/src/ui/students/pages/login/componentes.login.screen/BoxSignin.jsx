import { Box, Button } from "@mui/material";
import { CampoEmail } from "./componentes.box.login.screen/CampoEmail";

import TEXT_LOGIN_SCREEN from "../tilemap/TextolLoginScreen.json";
import TELA from "../tilemap/TelaAtualCode.json";
import { CampoNome } from "./componentes.box.login.screen/CampoNome";
import { CampoTurma } from "./componentes.box.login.screen/CampoTurma";
import { CampoSenha } from "./componentes.box.login.screen/CampoSenha";
import { CampoConfirmarSenha } from "./componentes.box.login.screen/CampoConfirmarSenha";
import { useState } from "react";

export function BoxSignin({
  inputsText,
  handleChange,
  handleClick,
  handleSubmit,
  isButtonDisable
}) {

  const [acceptedTerms, setAcceptedTerms] = useState(false)

  return (
    <div className="container-loginScreen">
      <h1>appAAP</h1>
      <p>{TEXT_LOGIN_SCREEN[TELA.SIGNIN].text_to_content_page}</p>

      <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
        <CampoEmail value={inputsText.email} changeValue={handleChange} />
        <CampoNome value={inputsText.name} changeValue={handleChange} />
        <CampoTurma value={inputsText.turma} changeValue={handleChange} />
        <CampoSenha value={inputsText.senha} changeValue={handleChange} />
        <CampoConfirmarSenha value={inputsText.confirmarSenha} changeValue={handleChange} />
        <input type="checkbox" name="accept" onChange={() => setAcceptedTerms(!acceptedTerms)}/> 
        <label>Li e aceito os <a href="https://drive.google.com/file/d/1EuO1-3cGSG3v342HAJp2V3TPmu-DN56H/view?usp=drivesdk">termos da política de privacidade</a> do appAAP</label>
        <Button
          disabled={acceptedTerms && inputsText.senha === inputsText.confirmarSenha? isButtonDisable : true}
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          {TEXT_LOGIN_SCREEN[TELA.SIGNIN].text_to_title_page}
        </Button>

        <div className="change-login-container--loginScreen">
          <button
            className="change-login-button--loginScreen"
            type="button"
            name="changeLogin"
            onClick={handleClick}
          >
            {TEXT_LOGIN_SCREEN[TELA.SIGNIN].text_to_change_login}
          </button>
        </div>
      </Box>
    </div>
  );
}
