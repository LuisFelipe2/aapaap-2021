import { TextField } from "@mui/material";

export function CampoNome({ value, changeValue }) {
  return (
    <TextField
      margin="normal"
      required
      fullWidth
      id="nome"
      label="Nome completo"
      name="nome"
      autoComplete="nome"
      value={value}
      onChange={changeValue}
    />
  );
}
