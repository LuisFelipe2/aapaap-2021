import { Button, Modal } from "@mui/material";
import { Box } from "@mui/system";
import { useState } from "react";
import { api } from "../../../../../../requestApi/api";
import { CampoEmail } from "./CampoEmail";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "white",
  border: "2px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

export function ModalEsqueciSenha({ isOpen, handleClose }) {
  const useApi = api();
  const [emailValue, setEmailValue] = useState("");

  function handleChange(e) {
    setEmailValue(e.target.value);
  }

  function sendEmail(e) {
    e.preventDefault();

    try {
      useApi.sendEmail(emailValue);
      alert("Email enviado com sucesso")
    } catch (error) {
      alert("Ocorreu um erro");
      console.log(error);
    }
  }

  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="parent-modal-title"
      aria-describedby="parent-modal-description"
    >
      <Box sx={{ ...style, width: 400 }}>
        <h2 id="parent-modal-title">
          Digite seu email de recuperação de senha
        </h2>
        <form onSubmit={sendEmail}>
          <CampoEmail value={emailValue} changeValue={handleChange} />
          <Button type="submit">Clique aqui</Button>
        </form>
      </Box>
    </Modal>
  );
}
