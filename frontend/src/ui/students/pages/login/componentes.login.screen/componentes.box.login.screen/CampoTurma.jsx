/* eslint-disable react-hooks/exhaustive-deps */
import { InputLabel, MenuItem, Select } from "@mui/material";
import { useEffect, useState } from "react";
import { api } from "../../../../../../requestApi/api";

export function CampoTurma({ value, changeValue }) {

  const [turmas, setTurmas] = useState([]);
  const useApi = api();

  useEffect(() => {
    async function buscarTodasAsTurma() {
      const turmasResponse = await useApi.buscarTurmas();
      setTurmas(turmasResponse);
    }
    buscarTodasAsTurma();
  }, []);


  return (
    <>
      <InputLabel id="demo-simple-select-outlined-label">
        Selecione sua turma/curso
      </InputLabel>
      <Select
        className="select_login"
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        value={value}
        onChange={changeValue}
        name="idTurma"
        label="Selecione sua turma/curso"
      >
        <MenuItem value={0}></MenuItem>
        {turmas.map((turma) => (
          <MenuItem value={turma.id}>{turma.nome}</MenuItem>
        ))}
      </Select>
    </>
  );
}
