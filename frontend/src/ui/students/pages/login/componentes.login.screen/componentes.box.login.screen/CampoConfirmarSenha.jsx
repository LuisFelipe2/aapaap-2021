import { TextField } from "@mui/material";

export function CampoConfirmarSenha({value, changeValue}) {
  return (
    <TextField
      margin="normal"
      required
      fullWidth
      id="confirmarSenha"
      label="confirmar senha"
      name="confirmarSenha"
      autoComplete="senha"
      type="password"
      value={value}
      onChange={changeValue}
    />
  );
}