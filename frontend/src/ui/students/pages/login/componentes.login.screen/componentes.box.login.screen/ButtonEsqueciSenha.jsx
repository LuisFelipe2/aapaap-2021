import { Button } from "@mui/material";

export function ButtonEsqueciSenha({handleClick}){

  return (
    <Button hidden={true} variant="text" name="forgotPassword" onClick={handleClick}>Esqueci minha senha</Button>
  );

}