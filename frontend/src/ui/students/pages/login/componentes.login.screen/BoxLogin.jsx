
import TEXT_LOGIN_SCREEN from "../tilemap/TextolLoginScreen.json"
import TELA from "../tilemap/TelaAtualCode.json"
import { CampoEmail } from "./componentes.box.login.screen/CampoEmail";
import { Box } from "@mui/system";
import { Button } from "@mui/material";
import { CampoSenha } from "./componentes.box.login.screen/CampoSenha";
import { ButtonEsqueciSenha } from "./componentes.box.login.screen/ButtonEsqueciSenha";
import { ModalEsqueciSenha } from "./componentes.box.login.screen/ModalEsqueciSenha";
import { useState } from "react";

export function BoxLogin({inputsText, handleChange, handleClick, handleSubmit, isButtonDisable}) {

  const [isOpen, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);



  return ( 
    <div className="container-loginScreen">
      <h1>appAAP</h1>
      <p>{TEXT_LOGIN_SCREEN[TELA.LOGIN].text_to_content_page}</p>

      <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
        
        <CampoEmail value={inputsText.email} changeValue={handleChange}/>
        <CampoSenha value={inputsText.senha} changeValue={handleChange}/>
        <Button
          disabled={isButtonDisable}
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          {TEXT_LOGIN_SCREEN[TELA.LOGIN].text_to_title_page}
        </Button>

        <div>
          <ButtonEsqueciSenha handleClick={handleOpen} />
        </div>

        <div className="change-login-container--loginScreen">
          <button
            className="change-login-button--loginScreen"
            type="button"
            name="changeLogin"
            onClick={handleClick}
          >
            {TEXT_LOGIN_SCREEN[TELA.LOGIN].text_to_change_login}
          </button>
        </div>
      </Box>

      <ModalEsqueciSenha isOpen={isOpen} handleClose={handleClose}  />
    </div>
  );
}
