/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";

import "./LoginScreen.css";
import { useHistory } from "react-router";
import { Alert } from "@mui/material";
import { cadastrarUsuario, gerarToken } from "./functions/verificarInput";
import { BoxLogin } from "./componentes.login.screen/BoxLogin";
import { BoxSignin } from "./componentes.login.screen/BoxSignin";
import TELA from "./tilemap/TelaAtualCode.json";
import { api } from "../../../../requestApi/api";
import { useGlobalUser } from "../../../../context/user.context";

export function LoginScreen() {
  const useApi = api();
  const { push } = useHistory();
  const [telaAtual, setTelaAtual] = useState(TELA.LOGIN);
  const [error, setError] = useState([]);
  const [isButtonDisable, setButtonDisebled] = useState(false);
  //Refere-se as mensagens estilizadas que aparecerão para o usuário. Ainda não implementado
  const [alertUser, setAlertUser] = useState([]);
  const [globalUser, setGlobalUser] = useGlobalUser();
  const [inputsText, setInputsText] = useState({
    nome: "",
    email: "",
    idTurma: 0,
    senha: "",
    confirmarSenha: ""
  });

  useEffect(() => {
    if (globalUser !== "") push("/home");
  }, [globalUser]);

  function handleChange(event) {
    const { name, value } = event.target;
    if (name === "nome") setInputsText({ ...inputsText, nome: value });
    else if (name === "email") setInputsText({ ...inputsText, email: value });
    else if (name === "idTurma") setInputsText({ ...inputsText, idTurma: value });
    else if (name === "senha") setInputsText({ ...inputsText, senha: value });
    else if (name === "confirmarSenha")setInputsText({ ...inputsText, confirmarSenha: value }); 
  }

  function handleSubmit(event) {
    event.preventDefault();
    setButtonDisebled(true);
    setError([]);
    setAlertUser([]);

    if (telaAtual === TELA.SIGNIN)
      cadastrarUsuario(inputsText, useApi).then((retorno) => {
        !!retorno
          ? setAlertUser(["Usuário cadastrado com sucesso, pode fazer login"])
          : setError(["Ocorreu um erro ao cadastrar o usuário"]);
        setButtonDisebled(false);
      });

    if (telaAtual === TELA.LOGIN)
      gerarToken(inputsText, useApi).then((token) => {
        !!token ? setGlobalUser(token.token) : setError(["Ocorreu um erro"]);
        setButtonDisebled(false);
      });
  }

  function handleClick(event) {
    const {name} = event.target

    if(name==="changeLogin"){
      setError([]);
      setAlertUser([]);
      if (telaAtual === TELA.LOGIN) setTelaAtual(TELA.SIGNIN);
      else if (telaAtual === TELA.SIGNIN) setTelaAtual(TELA.LOGIN);
    }
    else if (name==="forgotPassword"){
      
    }
  }

  return (
    <>
      <div className="background--loginScreen">
        <main>
          <div className="alert-container">
            {error.map((erro) => (
              <Alert key={erro} severity="error">
                {erro}
              </Alert>
            ))}

            {alertUser.map((alerta) => (
              <Alert key={alerta} severity="success">
                {alerta}
              </Alert>
            ))}
          </div>

          {telaAtual === TELA.LOGIN ? (
            <BoxLogin
              inputsText={inputsText}
              handleChange={handleChange}
              handleClick={handleClick}
              handleSubmit={handleSubmit}
              isButtonDisable={isButtonDisable}
            />
          ) : (
            <BoxSignin
              inputsText={inputsText}
              handleChange={handleChange}
              handleClick={handleClick}
              handleSubmit={handleSubmit}
              isButtonDisable={isButtonDisable}
            />
          )}
        </main>
      </div>
    </>
  );
}
