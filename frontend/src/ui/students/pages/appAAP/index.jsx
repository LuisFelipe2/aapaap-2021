import { useEffect, useState } from "react";
import { api } from "../../../../requestApi/api";
import { ScreenEstrategia } from "../estrategias";
import { ScreenHelp } from "../help-screen";
import { ScreenProcedimentos } from "../procedimentos-screen";
import { ScreenQuestion } from "../question-screen";



const TELAS = {
  question: 1,
  help: 2,
  procedimentos: 3,
  estrategia: 4,
};

export function QuestaoManager() {

  const [telaAtual, setTelaAtual] = useState();
  const [tentativaId, setTentativaId] = useState();
  const [tentativa, setTentativa] = useState();
  const useApi = api();

  useEffect(() => {
    async function iniciarTentativa() {
      const id = localStorage.getItem("questaoId");
      const questaoId = parseInt(id);
      const tentativaIdResponse = await useApi.iniciarTentativa(questaoId);

      if (!!!tentativaIdResponse) {
        const tentativaResponse = await useApi.buscarTentativa(questaoId);
        setTentativa(tentativaResponse);
        setTentativaId(tentativaResponse.id);
      }
      else{
        setTentativaId(tentativaIdResponse);
      }

      setTelaAtual(TELAS.question);
    }

    iniciarTentativa();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function switchTela() {
    switch (telaAtual) {
      case TELAS.question:
        return (
          <ScreenQuestion
            tentativaId={tentativaId}
            setTelaAtual={setTelaAtual}
            tentativa={tentativa}
          />
        );
      case TELAS.help:
        return (
          <ScreenHelp tentativaId={tentativaId} setTelaAtual={setTelaAtual} />
        );
      case TELAS.procedimentos:
        return (
          <ScreenProcedimentos
            tentativaId={tentativaId}
            setTelaAtual={setTelaAtual}
          />
        );
      case TELAS.estrategia:
        return (
          <ScreenEstrategia
            tentativaId={tentativaId}
            setTelaAtual={setTelaAtual}
          />
        );
      default:
        return (
          <ScreenQuestion
            tentativaId={tentativaId}
            setTelaAtual={setTelaAtual}
          />
        );
    }
  }

  return <>{switchTela()}</>;
}
