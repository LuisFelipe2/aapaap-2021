/* eslint-disable react-hooks/exhaustive-deps */
import { Button, Typography } from "@mui/material";
import { useHistory } from "react-router";
import { Header } from "../../../components/header/header";
import "./estilo.css";

export function Home() {

  const { push } = useHistory();

  function handleClick() {
    push("/atividades");
  }

  return (
    <>
      <Header />
      <div className="caminho">
        <ul>
          <li id="cinza">Meus cursos</li>
        </ul>
      </div>
      <main className="main--home">
        <Typography variant="h1" component="div" gutterBottom>
          Este é o AppAAP!
        </Typography>
        <Typography variant="body1" component="div" gutterBottom>
          O appAAP é um Aplicativo de Apoio à Ação Pedagógica que busca
          identificar estratégias utilizadas pelos estudantes durante a
          resolução de situações-problema e indicar, ao professor(a), possíveis
          dificuldades que cada estudante enfrentou durante o processo!
          <br /> <br />
          Para você, o appAAP será um espaço de apoio à resolução de
          situações-problema e onde poderá buscar ajudas, se necessário, para
          esclarecer suas dúvidas!
          <br />
          <br />
          Você pode resolver as atividades e realizar suas anotações da forma
          que quiser! É recomendável que use lápis e papel ou outra forma de
          registro escrito durante o processo de resolução.
          <br />
          <br />
          Explore o appAAP para obter ajudas e, ao encaminhar sua resposta, siga
          as orientações indicadas!
        </Typography>

        <div>
          <Typography variant="h2">Comece por aqui</Typography>
          
          <Button
            onClick={handleClick}
            color="success"
            size="large"
          >
            Começar
          </Button>
        </div>
      </main>
    </>
  );
}
