/* eslint-disable react-hooks/exhaustive-deps */
import { Markup } from "interweave";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { api } from "../../../../requestApi/api";
import { Header } from "../../../components/header/header";
import { Loading } from "../../../components/loading/loading";

import "./estilo.css";

var Latex = require("react-latex");

const TELAS = {
  question: 1,
  help: 2,
  procedimentos: 3,
  estrategia: 4,
};

export function ScreenProcedimentos({ tentativaId, setTelaAtual }) {
  const useApi = api();
  const [procedimentos, setProcedimentos] = useState([]);
  const [procedimentosUsados, setProcedimentosUsados] = useState([]);
  const [disabled, setDisabled] = useState(false);

  const { push } = useHistory();

  useEffect(() => {
    async function buscarProcedimentos() {
      const procedimentosResponse = await useApi.buscarProcedimentos(
        tentativaId
      );
      if (!!procedimentosResponse) {
        setProcedimentos(procedimentosResponse);
      } else {
        const responseEstrategia = await useApi.buscarEstrategia(
          tentativaId,
          []
        );
        localStorage.setItem(
          "estrategia",
          responseEstrategia.map((e) => e.texto)
        );
        setTelaAtual(TELAS.estrategia);
      }
    }
    buscarProcedimentos();
  }, []);

  function setUseProcedimentos(_useProcedimentos) {
    setProcedimentosUsados([..._useProcedimentos]);
  }

  function handleClick() {
    setDisabled(true);
    buscarEstrategia();
  }
  async function buscarEstrategia() {
    try {
      const responseEstrategia = await useApi.buscarEstrategia(
        tentativaId,
        procedimentosUsados
      );
      if (responseEstrategia.length === 1) {
        localStorage.setItem("estrategia", responseEstrategia[0].texto);
      }else{
        localStorage.setItem(
          "estrategia",
          "Ah, que pena! Não foi possível identificar a estratégia usada por você, pois nenhum procedimento foi selecionado! Não se preocupe, as ajudas que você consultou estão salvas e poderão ser acessadas pelo docente para que ele possa auxiliar em suas dificuldades!"
        );
      }
    } catch (erro) {
      localStorage.setItem(
        "estrategia",
        "Ah, que pena! Não foi possível identificar a estratégia usada por você, pois nenhum procedimento foi selecionado! Não se preocupe, as ajudas que você consultou estão salvas e poderão ser acessadas pelo docente para que ele possa auxiliar em suas dificuldades!"
      );
    }
    setTelaAtual(TELAS.estrategia);
  }

  function minhaFunc(procedimento) {
    const selected = procedimentosUsados.some(
      (_procedimento) => _procedimento === procedimento
    );
    if (selected) {
      const newSelected = procedimentosUsados.filter(
        (_procedimento) => _procedimento !== procedimento
      );
      setUseProcedimentos(newSelected);
      return;
    }
    setUseProcedimentos([...procedimentosUsados, procedimento]);
  }

  return (
    <div id="super_layout">
      <Header />

      <main>
        <div className="page">
          <div className="caminho">
            <ul>
              <li onClick={() => push("/home")} className="link">
                Meus cursos
              </li>
              <li id="cinza">/</li>
              <li onClick={() => push("/atividades")} className="link">
                Situações-problema
              </li>
              <li id="cinza">/</li>
              <li className="link" onClick={() => setTelaAtual(TELAS.question)}>
                {localStorage.getItem("questao")}
              </li>
              <li id="cinza">/</li>
              <li id="cinza">Selecionar procedimento</li>
            </ul>
          </div>

          {procedimentos.length === 0 ? (
            <Loading />
          ) : (
            <div className="questao-content">
              <div className="margin-bottom">
                <h1 className="procedimentos-h1">
                  Leia cada um dos procedimentos abaixo e marque aquele(s) que
                  você usou.
                </h1>
                <span>
                  <strong>
                    Importante: Você pode marcar quantos quiser, inclusive não
                    marcar nenhum deles se for o caso!
                  </strong>
                </span>
              </div>
              <div>
                {procedimentos.length > 0 ? (
                  procedimentos.map((procedimento) => (
                    <label className="checkmaster texto" key={procedimento.id}>
                      {procedimento.id < 68 ?
                      <Latex
                        style={{ display: "inline-block" }}
                        displayMode={true}
                      >
                        {procedimento.mensagem}
                      </Latex>
                      :
                      <Markup content={procedimento.mensagem} />
                      }
                      <input
                        type="checkbox"
                        onChange={() => minhaFunc(procedimento.id)}
                      />
                      <span className="truecheck"></span>
                    </label>
                  ))
                ) : (
                  <div></div>
                )}
              </div>

              <button
                onClick={handleClick}
                disabled={disabled}
                className="bt-final"
              >
                Enviar e ver resultado
              </button>
            </div>
          )}
        </div>
      </main>
    </div>
  );
}
