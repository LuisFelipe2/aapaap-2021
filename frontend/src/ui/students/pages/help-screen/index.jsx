/* eslint-disable react-hooks/exhaustive-deps */
import { Typography } from "@mui/material";
import { Markup } from "interweave";
import { useEffect, useState } from "react";
import { api } from "../../../../requestApi/api";
import { Header } from "../../../components/header/header";
import { Loading } from "../../../components/loading/loading";
import { Modal } from "../../../components/modal/modal";
import { Path } from "../../../components/path";

import "./estilo.css";

const TELAS = {
  question: 1,
  help: 2,
  procedimentos: 3,
  estrategia: 4,
};

export function ScreenHelp({ tentativaId, setTelaAtual }) {
  const [todasAjudas, setAjudas] = useState([]);
  const questaoId = localStorage.getItem("questaoId");
  const [useDica, setUseDica] = useState({});
  const [questao, setQuestao] = useState();
  const useApi = api();

  useEffect(() => {
    async function buscarAjudasByQuestao() {
      let ajudasResponse = await useApi.buscarAjudas(questaoId);
      setAjudas(ajudasResponse);
    }

    async function buscarMinhasQuestoes() {
      const questoes = await useApi.buscarTodasQuestoes();
      const questao = questoes.find(
        (questao) => questao.id === parseInt(questaoId)
      );
      setQuestao(questao);
    }

    buscarAjudasByQuestao();
    buscarMinhasQuestoes();
  }, [questaoId]);

  function handleClick(event) {
    const { name } = event.target;

    let ajudaEscolhida;
    todasAjudas.find(
      (conceito) =>
        (ajudaEscolhida = conceito.ajudas.find((ajuda) => ajuda.id === name))
    );
    
    if (ajudaEscolhida) {
      setUseDica(ajudaEscolhida);
      buscarLog(ajudaEscolhida.id);
    }
  }
  function handleFecharModal() {
    setTelaAtual(TELAS.question);
  }

  async function buscarLog(name) {
    try {
      await useApi.salvarAjudaLog(tentativaId, name);
    } catch (exception) {
      console.log(
        "houve um erro ao tentar salvar o log de ajuda na função salvarAjudaLog na api",
        exception
      );
    }
  }

  return (
    <div id="super_layout">
      <Header />

      <main>
        <div className="page">
          <Path ajuda={true} setTelaAtual={setTelaAtual} />

          {todasAjudas.length === 0 ? (
            <Loading />
          ) : (
            <div className="questao-content">
              <div className="questao-content-1">
                <h1>{questao?.titulo}</h1>
              </div>

              <div className="margin-bottom">
                <h2>Sobre qual tópico você quer uma dica?</h2>
                <Typography variant="body2" component="span">
                  As ajudas estão distribuídas por assunto, sendo que algumas se
                  repetem em diferentes grupos.
                  <br />
                  Além disso, há ajudas mais genéricas e outras mais
                  direcionadas! A seleção da ajuda dependerá de você, ou seja,
                  da estratégia que você irá desenvolver. Há diferentes caminhos
                  possíveis!
                </Typography>
              </div>

              {todasAjudas.map((conceito) => (
                <div className="menu-drop" key={conceito.id}>
                  <button type="button" name="drop" key={conceito.id}>
                    {conceito.texto}
                  </button>
                  <div className="drop-content">
                    {conceito.ajudas.map((ajuda) => (
                      <button
                        name={ajuda.id}
                        key={ajuda.id + conceito.texto}
                        onClick={handleClick}
                      >
                        {ajuda.titulo}
                      </button>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          )}

          {useDica.id && !useDica.texto && (
            <div>
              <Modal code={useDica.id} setTelaAtual={setTelaAtual} />
            </div>
          )}

          {useDica.id && useDica.texto && (
            <div className="container-modal" onClick={handleFecharModal}>
              <div className="modal-content">
                <Markup content={useDica.texto} />
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
}
