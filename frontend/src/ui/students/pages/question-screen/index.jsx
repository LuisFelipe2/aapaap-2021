/* eslint-disable react-hooks/exhaustive-deps */
import { Header } from "../../../components/header/header";
import { Input } from "../../../components/Input_responder_questao/input";
import { Path } from "../../../components/path/index";

import "./estilo.css";
import { useEffect, useState } from "react";
import { Imagens } from "../../../components/Imagens_questoes_1_a_4";
import { Button, Typography } from "@mui/material";
import { Loading } from "../../../components/loading/loading";
import { api } from "../../../../requestApi/api";
import { Markup } from "interweave";

const TELAS = {
  question: 1,
  help: 2,
  procedimentos: 3,
  estrategia: 4,
};

export function ScreenQuestion({ tentativaId, setTelaAtual, tentativa }) {
  const [questionResponse, setQuestionResponse] = useState({
    titulo: "",
    mensagem: "",
    resposta: "",
  });
  const [respondido, setRespondido] = useState();
  const useApi = api();

  useEffect(() => {
    async function buscarQuestao(){
      const id = localStorage.getItem("questaoId");
      const questId = parseInt(id);
      const questoes = await useApi.buscarTodasQuestoes();
      const response = questoes.find((q) => {
        return q.id === questId;
      })
      setQuestionResponse(response);
      localStorage.setItem("questao", response.titulo)
    }

    buscarQuestao();
  }, []);

  useEffect(() => {
    if (tentativa !== undefined) {
      setRespondido(true);
    }
  }, [tentativa]);

  function handleClick(event, reposta) {
    const { name } = event.target;
    if (name === "btn-ajuda") {
      setTelaAtual(TELAS.help);
    }
    if (name === "btn-procedimento") {
      finalizarTentativa(reposta);
    }
    if (name === "btn-resolvido") {
      setTelaAtual(TELAS.procedimentos);
    }
  }

  async function finalizarTentativa(reposta) {
    await useApi.encerrarTentativa(tentativaId, reposta);
    setTelaAtual(TELAS.procedimentos);
  }

  return (
    <div id="super_layout">
      <Header />

      <main>
        <div className="page">
          <Path />

          {!!!tentativaId ? (<>
            <Loading />
            </>
          ) : (
            <div className="questao-content">
              <div className="questao-content-1">
                <h1>{questionResponse.titulo}</h1>
              </div>
              <Markup content={questionResponse.enunciado} />
              
              <div className="container-imagens">
                <Imagens questaoId={questionResponse.id} />
              </div>

              {!respondido ? (
                <Input onclick={handleClick} />
              ) : (
                <div>
                  <Typography>
                    Sua resposta foi: {tentativa.resposta}
                  </Typography>
                  <Button name="btn-resolvido" color="success" className="link" onClick={handleClick}>
                    Estratégia identificada
                  </Button>
                </div>
              )}

              <div className="container-imagens">
                <button
                  onClick={handleClick}
                  name="btn-ajuda"
                  className="bt-ajuda"
                >
                  Dicas de resolução?
                </button>
              </div>
            </div>
          )}
        </div>
      </main>
    </div>
  );
}
