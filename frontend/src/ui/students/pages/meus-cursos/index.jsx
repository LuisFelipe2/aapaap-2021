/* eslint-disable react-hooks/exhaustive-deps */
import { useHistory } from "react-router";
import { useEffect, useState } from "react";
import { Header } from "../../../components/header/header";
import "./estilo.css";
import { CardContent, Typography } from "@mui/material";
import { Loading } from "../../../components/loading/loading";
import { api } from "../../../../requestApi/api";

export function Curso({ setIdQuestao }) {
  const [allQuestions, setAllQuestions] = useState([]);

  const { push } = useHistory();
  const useApi = api();

  useEffect(() => {
    async function getAllQuestions() {
      localStorage.removeItem("estrategia");

      try{
        const retorno = await useApi.buscarTodasQuestoes();
        setAllQuestions(retorno);
      } catch(exception){
        console.log("Ocorreu uma falha na execução da função buscarTodasQuestoes da api: ", exception)
      }
    }

    getAllQuestions();
  }, []);

  function handleClick(id) {
    localStorage.setItem("questaoId", id);
    setIdQuestao(id);
    push("/questao");
  }

  return (
    <>
      <Header />
      <div className="caminho">
        <ul>
          <li onClick={() => push("/home")} className="link">
            Meus cursos
          </li>
          <li id="cinza">/</li>
          <li id="cinza">Situações-problema</li>
        </ul>
      </div>

      <main className="main--curso">
        
        {allQuestions.length === 0 ? (
          <Loading />
        ) : (
          <>
            <Typography
              className="titulo-buscar-questao"
              gutterBottom
              variant="h4"
              component="div"
            >
              Selecione uma das situações-problema abaixo para resolver:
            </Typography>
            {allQuestions.map((question) => (
              <CardContent
                key={question.id}
                className="efeito-hover"
                onClick={() => handleClick(question.id)}
              >
                <Typography gutterBottom variant="h5" component="div">
                  {question.titulo}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  Clique no card para visualizar esta situações-problema...
                </Typography>
              </CardContent>
            ))}
          </>
        )}
      </main>
    </>
  );
}
