import { CircularProgress, Slider, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Button } from "@mui/material";

import "./estilo.css";
import AlertDialog from "../../../components/modal-finalizar-questao";
import { api } from "../../../../requestApi/api";
import { Header } from "../../../components/header/header";
import { Markup } from "interweave";

const TELAS = {
  question: 1,
  help: 2,
  procedimentos: 3,
  estrategia: 4,
};

const TEXT_GRAU = {
  1: "DISCORDO TOTALMENTE",
  2: "DISCORDO",
  3: "NEUTRO",
  4: "CONCORDO",
  5: "CONCORDO TOTALMENTE",
};

//var Latex = require("react-latex");

export function ScreenEstrategia({ setTelaAtual, tentativaId }) {
  const [resultado, setResultado] = useState("");
  const [estrategia_customizada, setEstrategia_customizada] = useState("");
  const [resposta, setResposta] = useState({});
  const [grauAproximacao, setGrauAproximacao] = useState(3);
  const [loading, setLoading] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const { push } = useHistory();
  const useApi = api();

  useEffect(() => {
    async function getMyQuestions() {
      const result = localStorage.getItem("estrategia");
      setResultado(result);
      const idQuestao = localStorage.getItem("questaoId");
      const id = parseInt(idQuestao);
      const questoes = await useApi.buscarTodasQuestoes();
      const questaoAtual = questoes.find((q) => q.id === id);

      setResposta(questaoAtual);
      window.scrollTo(0, 0);
    }

    getMyQuestions();
  }, []);

  function handleChange(event) {
    const { name, value } = event.target;

    if (name === "estrategia_customizada") {
      setEstrategia_customizada(value);
    }
  }

  function handleClick() {
    salvarGrauDeSimilaridade();
  }
  async function salvarGrauDeSimilaridade() {
    try {
      await useApi.salvarGrauDeSimilaridade(grauAproximacao, tentativaId);
    } catch (error) {
      console.log(
        "Ocorreu um erro ao tentar salvar o grau de similaridade da questão, ",
        error
      );
    }
    push("/atividades");
  }

  function handleSubmit(event) {
    setLoading(true);
    salvarError();
    setTimeout(() => {
      setDisabled(true);
      setLoading(false);
    }, 1500);
  }

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  async function salvarError() {
    await useApi.salvarError(estrategia_customizada, tentativaId);
  }

  return (
    <div id="super_layout">
      <Header />

      <main>
        <div className="page">
          <div className="caminho">
            <ul>
              <li onClick={() => push("/home")} className="link">
                Meus cursos
              </li>
              <li id="cinza">/</li>
              <li onClick={() => push("/atividades")} className="link">
                Situações-problema
              </li>
              <li id="cinza">/</li>
              <li className="link" onClick={() => setTelaAtual(TELAS.question)}>
                {localStorage.getItem("questao")}
              </li>
              <li id="cinza">/</li>
              <li
                className="link"
                onClick={() => setTelaAtual(TELAS.procedimentos)}
              >
                Selecionar procedimento
              </li>
              <li id="cinza">/</li>
              <li id="cinza">Verificar estratégia</li>
            </ul>
          </div>
          <div className="questao-content">
            <h1>Você finalizou a situação-problema {resposta?.titulo}</h1>
            <p className="verde-bold">
              A resposta correta para essa situação-problema é{" "}
              {resposta?.resposta}
            </p>
            <div className="agradecimentos">
              <p>
                <Markup content={resultado} />
              </p>
            </div>

            <div className="inputs-final">
              <div className="flex-total">
                <Button
                  className="botao-escolher-novaQuestao"
                  variant="contained"
                  onClick={() => setOpen(true)}
                  color="success"
                >
                  Enviar avaliação e finalizar
                </Button>
              </div>
              <div>
                <Typography id="discrete-slider-small-steps" gutterBottom>
                  Avalie a seguinte afirmação: <br />A ESTRATÉGIA IDENTIFICADA
                  PELO APPAAP CORRESPONDE À ESTRATÉGIA QUE DESENVOLVI.
                </Typography>
                <Slider
                  defaultValue={3}
                  value={grauAproximacao}
                  onChange={(event) => setGrauAproximacao(event.target.value)}
                  name="slider"
                  aria-labelledby="discrete-slider-small-steps"
                  step={1}
                  marks
                  min={1}
                  max={5}
                  valueLabelDisplay="auto"
                  valueLabelFormat={TEXT_GRAU[grauAproximacao]}
                />
              </div>
            </div>
            <div className="feedback">
              <Typography variant="body1" component="div">
                <b>
                  Para um melhor retorno, tente descrever na caixa abaixo o que
                  você pensou para resolver essa situação-problema!
                </b>
              </Typography>
              <div>
                <textarea
                  onChange={handleChange}
                  value={estrategia_customizada}
                  name="estrategia_customizada"
                  style={{ resize: "none" }}
                />
                <Button
                  type="submit"
                  disabled={disabled}
                  color="success"
                  name="error"
                  onClick={handleSubmit}
                >
                  {loading ? <CircularProgress /> : "Enviar comentario"}
                </Button>

                <AlertDialog
                  handleClick={handleClick}
                  open={open}
                  handleClose={handleClose}
                  grau={TEXT_GRAU[grauAproximacao]}
                />
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
