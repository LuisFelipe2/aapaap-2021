/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  CardContent,
  Typography,
  Input,
  OutlinedInput,
  FormLabel,
  Select,
  MenuItem,
  Card,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";
import { Markup } from "interweave";
import { useEffect, useState } from "react";
import { api } from "../../../../requestApi/api";
import { ProfApi } from "../../../../requestApi/profApi";
import Editor from "../../../components/editorQUILL/Editor";
import { Header } from "../../../components/header/header";
import TransferList from "../../../components/Lista_de_transferencia";

import "./estilo.css";

export function Ajudas() {
  const [idToUpdate, setIdToUpdate] = useState(undefined);
  const [left, setLeft] = useState([]);
  const [right, setRight] = useState([]);
  const [questoes, setQuestoes] = useState([]);
  const [pesquisa, setPesquisa] = useState("");
  const [conceitos, setConceitos] = useState([]);
  const [ajudaPesquisada, setAjudaPesquisada] = useState([]);
  const [ajuda, setAjuda] = useState([]);
  const [hiddenEditor, setHiddenEditor] = useState(false)
  const [todasQuestoes, setTodasQuestoes] = useState([])
  const [ajudaInput, setAjudaInput] = useState({
    id: "",
    titulo: "",
    texto: null,
    idConceito: "",
  });
  const jogador = JSON.parse(localStorage.getItem("usuario"));
  const useApi = ProfApi();
  const normalApi = api();

  useEffect(() => {
    async function getQuestao() {
      const retorno = await useApi.buscarMinhasAjudas();
      setAjuda(retorno);
      setAjudaPesquisada(retorno);
    }
    async function getConceito() {
      const retorno = await normalApi.buscarAjudas(1);
      setConceitos(retorno);
    }
    async function getMyQuestions(){
      const todasQuestoes = await useApi.buscarMinhasQuestoes();
      setTodasQuestoes(todasQuestoes)
      const titulos = todasQuestoes.map((questao) => questao.titulo);
      setLeft(titulos);
    }
    getConceito();
    getQuestao();
    getMyQuestions();
  }, []);

  useEffect(() => {
    const questoes = todasQuestoes;

    if (!!idToUpdate) {
      const toRight = questoes.filter((questao) =>
        questoes.some((q) => parseInt(q) === parseInt(questao.id))
      );
      setRight(toRight.map((questao) => questao.titulo));

      const toLeft = questoes.filter((questao) =>
        questoes.every((q) => parseInt(q) !== parseInt(questao.id))
      );

      setLeft(toLeft.map((questao) => questao.titulo));
    } else {
      const titulos = questoes.map((questao) => questao.titulo);
      setLeft(titulos);
    }
  }, [questoes]);

  function handleClick(event) {
    const { name } = event.target;
    if (name === "buscar_ajuda") {
      pesquisarAjudas();
    }
  }

  function handleChange(event) {
    const { name, value } = event.target;
    if (name === "titulo") setAjudaInput({ ...ajudaInput, titulo: value });
    else if (name === "resposta")
      setAjudaInput({ ...ajudaInput, resposta: value });
    else if (name === "idConceito")
      setAjudaInput({ ...ajudaInput, idConceito: value });
    else if (name === "id") setAjudaInput({ ...ajudaInput, id: value });
    else if (name === "search_bar") setPesquisa(value);
  }

  function handleFocus(){
    setHiddenEditor(true)
  }
  function handleBlur(){
    setHiddenEditor(false)
  }

  const handleChangeEditor = (texto) => {
    setAjudaInput({ ...ajudaInput, texto });
  };

  function handleSubmit(event) {
    event.preventDefault();
    const { name } = event.target;
    if (name === "new") salvarAjuda();
    else if (name === "update") atualizarAjuda();
  }

  function handleSelectToUpdate(event) {
    const { name } = event.target;
    localStorage.setItem("ajudaId", name);
    const retorno = ajuda.find((ajuda) => ajuda.id === name);
    if (!!retorno) setarInformacoesAjudasSelecionada(retorno);
  }

  function handleDelete(event) {
    const { name } = event.target;
    var resultado = window.confirm("Deseja mesmo excluir o item selecionado?");
    if (resultado === true) {
      deleteById(name);
    }
  }

  async function salvarAjuda() {
    const listIdQuestao = todasQuestoes
      .filter((questao) => right.some((msg) => msg === questao.titulo))
      .map((questao) => questao.id);

    try {
      await useApi.criarAjuda(ajudaInput, listIdQuestao);
      window.location.reload();
    } catch (e) {
      alert("Ocorreu um erro inesperado ao tentar criar a ajuda");
    }
  }

  async function atualizarAjuda() {
    const listaDeQuestoes = todasQuestoes
    .filter((questao) => right.some((msg) => msg === questao.titulo))
    .map((questao) => questao.id)

    try {
      await useApi.atualizarAjuda(
        idToUpdate,
        ajudaInput,
        listaDeQuestoes
      );
      window.location.reload();
    } catch (exception) {
      alert("Ocorreu um erro inesperado ao tentar atualizar a ajuda");
    }
  }

  function pesquisarAjudas() {
    const retorno = ajuda.filter((ajuda) => {
      if (ajuda.titulo.toUpperCase().includes(pesquisa.toUpperCase())) {
        return true;
      }
      return false;
    });
    setAjudaPesquisada(retorno);
  }

  async function setarInformacoesAjudasSelecionada(ajudaParaAtualizar) {
    const questoesIds = ajudaParaAtualizar.questoesId;
    const titulo = ajudaParaAtualizar.titulo;
    const texto = ajudaParaAtualizar.texto;
    const id = ajudaParaAtualizar.id;
    setIdToUpdate(id);
    setQuestoes(questoesIds);
    setAjudaInput({
      ...ajudaInput,
      titulo,
      id: id.slice(0, 2),
      questaoId: questoesIds,
      texto,
    });
  }

  async function deleteById(id) {
    try {
      await useApi.deleteAjuda(id);
      window.location.reload();
    } catch (error) {
      alert("Ocorreu um erro inesperado ao tentar deletar a ajuda");
    }
  }

  return (
    <>
      <Header />
      <div className="padding">
        <Typography variant={"h1"}>
          Fábrica de ajudas - {jogador?.nome}
        </Typography>

        <div>
          <Typography variant={"h2"}>Criar Ajuda</Typography>
          <FormLabel>Selecione o tipo de ajuda: </FormLabel>
          <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            defaultValue="female"
            name="id"
            value={ajudaInput.id}
            onChange={handleChange}
          >
            <FormControlLabel
              value="QR"
              control={<Radio />}
              label="Questionamentos reflexivos"
            />
            <FormControlLabel
              value="PN"
              control={<Radio />}
              label="Perguntas e respostas norteadoras"
            />
            <FormControlLabel
              value="DR"
              control={<Radio />}
              label="Dicas de resolução"
            />
          </RadioGroup>

          <br />
          <FormLabel>Digite o título: </FormLabel>
          <Input
            value={ajudaInput.titulo}
            name="titulo"
            onChange={(e) => handleChange(e)}
            onFocus={handleFocus}
            onBlur={handleBlur}
          />
          <br />
          <FormLabel>Digite o texto: </FormLabel>
          {!hiddenEditor &&
            <Editor handleChange={handleChangeEditor} value={ajudaInput.texto} />
          }
          <br />
          <FormLabel>Selecione o conceito: </FormLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="idConceito"
            value={ajudaInput.idConceito}
            onChange={handleChange}
          >
            {conceitos.map((conceito) => (
              <MenuItem value={conceito.id}>{conceito.texto}</MenuItem>
            ))}
          </Select>
          <br />
          <FormLabel>Selecione as questões para tabela da direita: </FormLabel>
          <TransferList
            left={left}
            right={right}
            setLeft={setLeft}
            setRight={setRight}
          />

          <Button name="new" onClick={handleSubmit}>
            Criar Ajuda
          </Button>
          <Button name="update" onClick={handleSubmit}>
            Atualizar Ajuda
          </Button>
        </div>

        <div>
          <Typography variant={"h2"}>Buscar Ajuda</Typography>

          <OutlinedInput
            id="outlined-adornment-amount"
            onChange={handleChange}
            value={pesquisa}
            name="search_bar"
            labelWidth={60}
            onFocus={handleFocus}
            onBlur={handleBlur}
          />
          <Button variant="outlined" name="buscar_ajuda" onClick={handleClick}>
            Buscar
          </Button>

          <div>
            {ajudaPesquisada.map((ajuda) => (
              <Card>
                <CardContent>
                  <Typography variant="h3">{ajuda.titulo}</Typography>
                  <Markup content={ajuda.texto} />
                  <Button
                    name={ajuda.id}
                    onClick={handleSelectToUpdate}
                  >
                    SELECT
                  </Button>
                  <Button size="small" name={ajuda.id} onClick={handleDelete}>
                    DELETE
                  </Button>
                </CardContent>
              </Card>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
