/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  CardContent,
  Typography,
} from "@mui/material";
import { Markup } from "interweave";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { ProfApi } from "../../../../requestApi/profApi";
import { Header } from "../../../components/header/header";

import "./estilo.css";

export function SelecaoDeProcedimentos() {
  const [procedimento, setProcedimento] = useState([]);
  const jogador = JSON.parse(localStorage.getItem("usuario"));
  const useApi = ProfApi();
  const { push } = useHistory();

  useEffect(() => {
    async function getQuestao() {
      const ajudaId = localStorage.getItem("ajudaId");
      const questaoId = localStorage.getItem("questaoId");
      const turmaId = localStorage.getItem("turmaId");
      const retorno = await useApi.buscarMeusProcedimentosPorAjuda(ajudaId,questaoId,turmaId);
      setProcedimento(retorno)
    }
    getQuestao();
  }, []);

  return (
    <>
    <Header />
      <div className="padding">
        <Typography variant={"h1"}>Procedimentos - {jogador?.nome}</Typography>

        {procedimento.map((proc) => (
          <CardContent
            key={proc.id}
            className="efeito-hover"
          >
            <Typography gutterBottom variant="h5" component="div">
            <Markup content={proc.mensagem} />
            </Typography>
          </CardContent>
        ))}
        <div>
          <Typography variant={"h2"}>Criar Procedimento</Typography>

          <Button onClick={() => push(`/professor/procedimento/criar`)}>
            Criar procedimento
          </Button>
        </div>
      </div>
    </>
  );
}
