/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  CardContent,
  Input,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { ProfApi } from "../../../../requestApi/profApi";
import { Header } from "../../../components/header/header";
import "./estilo.css";

export function SelecaoDeTurma() {
  const [turmasInput, setTurmasInput] = useState([]);
  const jogador = JSON.parse(localStorage.getItem("usuario"));
  const useApi = ProfApi();
  const [turmas, setTurmas] = useState([]);

  const { push } = useHistory();

  useEffect(() => {
    async function getTurmas() {
      const retorno = await useApi.buscarMinhasTurmas();
      setTurmas(retorno);
    }
    getTurmas();
  }, []);

  function handleClick(id) {
    localStorage.setItem("turmaId", id);
    push("/professor/questao");
  }
  function handleChange(event) {
    const { value } = event.target;
    setTurmasInput({ ...turmasInput, nome: value });
  }
  function handleSubmit(event) {
    event.preventDefault();
    salvar();
  }

  async function salvar() {
    try {
      await useApi.criarTurma(turmasInput);
      window.location.reload();
    } catch (error) {
      alert("Ocorreu um erro inesperado ao tentar cadastrar a turma");
    }
  }

  function handleDelete(id){
    deletarTurma(id);
  }

  async function deletarTurma(id){
    try{
      if(window.confirm("Deseja mesmo excluir essa turma?")){
        await useApi.deleteTurma(id);
        window.location.reload();
      }
    }catch(erro){
      alert("Ocorreu um erro inesperado ao tentar excluir a turma");
    }
  }

  return (
    <>
      <Header />
      <div className="padding">
        <Typography variant={"h1"}>Minhas turmas - {jogador?.nome}</Typography>

        {turmas.map((turma) => (
          <CardContent
            key={turma.id}
            className="efeito-hover"
          >
            <Typography
              gutterBottom
              variant="h5"
              component="div"
              key={turma.id + "abc"}
            >
              {turma.nome}
            </Typography>
            <div style={{display: "flex", justifyContent: "space-between"}}>
            <Button name="navegar" onClick={() => handleClick(turma.id)}>Ir para turma</Button>
            <Button name="deletar" onClick={() => handleDelete(turma.id)}>Deletar turma</Button>
            </div>
          </CardContent>
        ))}

        <div>
          <Typography variant={"h2"}>Criar turma</Typography>
          <formLabelClasses>Digite o nome da turma: </formLabelClasses>
          <Input value={turmasInput.name} onChange={handleChange} />
          <Button onClick={handleSubmit} type="submit">
            Criar turma
          </Button>
        </div>
      </div>
    </>
  );
}
