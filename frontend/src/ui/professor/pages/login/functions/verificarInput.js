

export async function cadastrarUsuario(inputsText, api) {
  if (
    inputsText.email === "" ||
    inputsText.nome === "" ||
    inputsText.senha === "" ||
    inputsText.confirmarSenha === ""
  ) {
    return;
  }

  try {
    await api.criarUsuario(inputsText);
    return true;
  } catch (error) {
    console.log("Ocorreu um erro na criação do usuário")
  }
}

export async function gerarToken(inputsText, api) {
  if (inputsText.email === "" || inputsText.senha === "") return;
  
  try {
    const token = await api.gerarToken({
      login: inputsText.email,
      senha: inputsText.senha
    });
    return token;
  } catch (erro) {
    console.log("Ocorreu um erro ao tentar gerar token")
  }
}