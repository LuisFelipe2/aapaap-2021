import { TextField } from "@mui/material";

export function CampoEmail({value, changeValue}) {
  return (
    <TextField
      margin="normal"
      required
      fullWidth
      id="email"
      label="Email"
      name="email"
      autoComplete="email"
      value={value}
      onChange={changeValue}
    />
  );
}
