import { TextField } from "@mui/material";

export function CampoSenha({value, changeValue}) {
  return (
    <TextField
      margin="normal"
      inputProps={{
        type:"password",
        autoComplete: 'new-password'
     }}
      required
      fullWidth
      id="senha"
      label="Senha"
      name="senha"
      autoComplete="senha"
      value={value}
      onChange={changeValue}
    />
  );
}