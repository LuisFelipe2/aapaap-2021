/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  CardContent,
  Typography,
  OutlinedInput,
  FormLabel,
  Select,
  MenuItem,
  Card,
  CardActions,
  Input,
} from "@mui/material";
import { Markup } from "interweave";
import { useEffect, useState } from "react";
import { ProfApi } from "../../../../requestApi/profApi";
import Editor from "../../../components/editorQUILL/Editor";
import { Header } from "../../../components/header/header";
import TransferList from "../../../components/Lista_de_transferencia";

import "./estilo.css";

export function Estrategias() {
  const [idToUpdate, setIdToUpdate] = useState(0);
  const [left, setLeft] = useState([]);
  const [right, setRight] = useState([]);
  const [pesquisa, setPesquisa] = useState([]);
  const [procedimentos, setProcedimentos] = useState([]);
  const [estrategias, setEstrategia] = useState([]);
  const [estrategiaPesquisada, setEstrategiaPesquisada] = useState([]);
  const [hiddenEditor, setHiddenEditor] = useState(false);
  const [questoes, setQuestoes] = useState([]);
  const [estrategiaInput, setEstrategiaInput] = useState({
    texto: "",
    idQuestao: 0,
    grauComplexidade: 0,
  });
  const jogador = JSON.parse(localStorage.getItem("usuario"));
  const useApi = ProfApi();

  useEffect(() => {
    async function buscarMinhasEstrategias() {
      const retorno = await useApi.buscarMinhasEstrategias();
      setEstrategia(retorno);
      setEstrategiaPesquisada(retorno);
    }
    async function getMyQuestions(){
      const todasQuestoes = await useApi.buscarMinhasQuestoes();
      setQuestoes(todasQuestoes)
    }
    buscarMinhasEstrategias();
    getMyQuestions();
  }, []);

  useEffect(() => {
    if (!!idToUpdate) {
      const toRight = procedimentos.filter((procedimento) =>
        estrategias
          .find((e) => e.id === idToUpdate)
          .procedimentoIds.some((procUsed) => procUsed === procedimento.id)
      );
      setRight(toRight.map((proc) => proc.mensagem));

      const toLeft = procedimentos.filter((procedimento) =>
        estrategias
          .find((e) => e.id === idToUpdate)
          .procedimentoIds.every((procUsed) => procUsed !== procedimento.id)
      );
      setLeft(toLeft.map((proc) => proc.mensagem));
    } else {
      const mensagensProcedimento = procedimentos.map((proc) => proc.mensagem);
      setLeft(mensagensProcedimento);
    }
  }, [procedimentos]);

  function handleClick(e) {
    const { name } = e.target;
    if (name === "buscarProcedimentos") {
      buscarProcedimentosDaQuestao(estrategiaInput.idQuestao);
    }
    if (name === "buscar_estrategias") {
      pesquisarEstrategia();
    }
  }

  function handleChange(event) {
    const { name, value } = event.target;
    if (name === "questaoId")
      setEstrategiaInput({ ...estrategiaInput, idQuestao: value });
    else if (name === "grau_complexidade")
      setEstrategiaInput({ ...estrategiaInput, grauComplexidade: value });
    else if (name === "pesquisa") setPesquisa(value);
  }

  const handleChangeEditor = (texto) => {
    setEstrategiaInput({ ...estrategiaInput, texto });
  };

  function handleFocus() {
    setHiddenEditor(true);
  }

  function handleBlur() {
    setHiddenEditor(false);
  }

  function handleSubmit(event) {
    event.preventDefault();
    const { name } = event.target;
    if (name === "new") salvarEstrategia();
    else if (name === "update") atualizarEstrategia();
    else deleteEstrategia(name);
  }

  function handleAtualizarEstrategia(id) {
    const retorno = estrategias.find(
      (estrategia) => estrategia.id === parseInt(id)
    );
    if (!!retorno) setarInformacoesEstrategiaSelecionada(retorno);
  }

  function pesquisarEstrategia() {
    const retorno = estrategias.filter((estrategia) => {
      return estrategia.texto?.toUpperCase().includes(pesquisa.toUpperCase())
        ? true
        : false;
    });

    setEstrategiaPesquisada(retorno);
  }

  async function setarInformacoesEstrategiaSelecionada(
    estrategiaParaAtualizar
  ) {
    const idQuestao = parseInt(estrategiaParaAtualizar.questaoId);
    const texto = estrategiaParaAtualizar.texto;
    const id = estrategiaParaAtualizar.id;
    setIdToUpdate(id);
    setEstrategiaInput({ ...estrategiaInput, idQuestao, texto });
    await buscarProcedimentosDaQuestao(estrategiaInput.idQuestao);
  }

  async function buscarProcedimentosDaQuestao(idQuestao) {
    const retorno = await useApi.buscarProcedimentos(idQuestao);
    setProcedimentos(retorno);
    return retorno;
  }

  async function salvarEstrategia() {
    const listaDeProcedimentos = procedimentos
      .filter((a) => right.some((msg) => msg === a.mensagem))
      .map((a) => a.id)
      .filter((a) => !!a);

    try {
      await useApi.criarEstrategia(estrategiaInput, listaDeProcedimentos);
      window.location.reload();
    } catch (exception) {
      alert("ocorreu um erro ao tentar criar a estratégia");
    }
  }

  async function atualizarEstrategia() {
    const listaDeProcedimentos = procedimentos
      .filter((a) => right.some((msg) => msg === a.mensagem))
      .map((a) => a.id)
      .filter((a) => !!a);

    try {
      await useApi.atualizarEstrategia(
        idToUpdate,
        estrategiaInput,
        listaDeProcedimentos
      );
      window.location.reload();
    } catch (exception) {
      alert("Ocorreu um erro inesperado ao tentar atualizar a estratégia");
    }
  }

  async function deleteEstrategia(id) {
    try {
      if (window.confirm("Tem certeza que deseja deletar essa estratégia? ")) {
        await useApi.deleteEstrategia(id);
        window.location.reload();
      }
    } catch (exception) {
      alert("Ocorreu um erro inesperado ao tentar excluir a estratégia");
    }
  }

  return (
    <>
      <Header />
      <div className="padding">
        <Typography variant={"h1"}>
          Fábrica de Estratégia - {jogador?.nome}
        </Typography>

        <div>
          <Typography variant={"h2"}>Criar Estrategia</Typography>
          <FormLabel>Digite o texto: </FormLabel>
          {!hiddenEditor && (
            <Editor
              handleChange={handleChangeEditor}
              value={estrategiaInput.texto}
            />
          )}
          <FormLabel>Digite o grau de complexidade da estratégia: </FormLabel>
          <Input
            type="number"
            name="grau_complexidade"
            onChange={handleChange}
            value={estrategiaInput.grauComplexidade}
          />
          <br />
          <FormLabel>Digite a questão a que queremos</FormLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="questaoId"
            value={estrategiaInput.idQuestao}
            onChange={handleChange}
          >
            {questoes.map((questao) => (
              <MenuItem key={questao.id} value={questao.id}>
                {questao.titulo}
              </MenuItem>
            ))}
          </Select>
          <Button name="buscarProcedimentos" onClick={handleClick}>
            Buscar procedimentos da questão selecionada
          </Button>
          <br />
          <FormLabel>
            Selecione os procedimentos que compõe essa estrategia para tabela da
            direita:{" "}
          </FormLabel>
          <TransferList
            left={left}
            right={right}
            setLeft={setLeft}
            setRight={setRight}
          />

          <Button name="new" onClick={handleSubmit}>
            Criar Estrategia
          </Button>
          <Button name="update" onClick={handleSubmit}>
            Atualizar Estrategia
          </Button>
        </div>

        <div>
          <Typography variant={"h2"}>Buscar Estrategias</Typography>

          <OutlinedInput
            id="outlined-adornment-amount"
            name="pesquisa"
            value={pesquisa}
            onChange={handleChange}
            labelWidth={60}
            onFocus={handleFocus}
            onBlur={handleBlur}
          />
          <Button
            variant="outlined"
            name="buscar_estrategias"
            onClick={handleClick}
          >
            Buscar
          </Button>
          <div>
            {estrategiaPesquisada.map((estrategia) => (
              <Card key={estrategia.id}>
                <CardContent>
                  <Markup content={estrategia.texto} />
                  <CardActions>
                    <Button
                      onClick={() => handleAtualizarEstrategia(estrategia.id)}
                    >
                      ATUALIZAR
                    </Button>
                    <Button
                      size="small"
                      name={estrategia.id}
                      onClick={handleSubmit}
                    >
                      DELETE
                    </Button>
                  </CardActions>
                </CardContent>
              </Card>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
