/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  CardContent,
  Typography,
  OutlinedInput,
  FormLabel,
  Select,
  MenuItem,
  Card,
  CardActions,
} from "@mui/material";
import { Markup } from "interweave";
import { useEffect, useState } from "react";
import { ProfApi } from "../../../../requestApi/profApi";
import Editor from "../../../components/editorQUILL/Editor";
import { Header } from "../../../components/header/header";
import TransferList from "../../../components/Lista_de_transferencia";

import "./estilo.css";

export function Procedimentos() {
  const [idToUpdate, setIdToUpdate] = useState(0);
  const [left, setLeft] = useState([]);
  const [right, setRight] = useState([]);
  const [pesquisa, setPesquisa] = useState([]);
  const [ajudas, setAjudas] = useState([]);
  const [procedimento, setProcedimento] = useState([]);
  const [hiddenEditor, setHiddenEditor] = useState(false);
  const [procPesquisado, setProcPesquisado] = useState([]);
  const [questoes, setQuestoes] = useState([]);
  const [procedimentoInput, setProcedimentoInput] = useState({
    mensagem: null,
    questaoId: 0,
  });
  const jogador = JSON.parse(localStorage.getItem("usuario"));
  const useApi = ProfApi();

  useEffect(() => {
    async function buscarMeusProcedimento() {
      const retorno = await useApi.buscarTodosProcedimentos();
      setProcedimento(retorno);
      setProcPesquisado(retorno);
    }
    async function getMyQuestions(){
      const todasQuestoes = await useApi.buscarMinhasQuestoes();
      setQuestoes(todasQuestoes)
    }
    buscarMeusProcedimento();
    getMyQuestions();

  }, []);

  useEffect(() => {
    if (!!idToUpdate) {
      const toRight = ajudas.filter((ajuda) =>
        procedimento
          .find((e) => e.id === idToUpdate)
          .ajudasId.some((ajudaUsed) => ajudaUsed === ajuda.id)
      );
      setRight(toRight.map((ajuda) => ajuda.titulo));

      const toLeft = ajudas.filter((ajuda) =>
        procedimento
          .find((e) => e.id === idToUpdate)
          .ajudasId.every((ajudaUsed) => ajudaUsed !== ajuda.id)
      );
      setLeft(toLeft.map((ajuda) => ajuda.titulo));
    } else {
      const mensagensAjuda = ajudas.map((ajuda) => ajuda.titulo);
      setLeft(mensagensAjuda);
    }
  }, [ajudas]);

  function handleClick(e) {
    const { name } = e.target;
    if (name === "buscarAjudas") {
      buscarAjudasDaQuestao(procedimentoInput.questaoId);
    } else if (name === "buscar_procedimento") {
      pesquisarProcedimento();
    }
  }

  function handleFocus() {
    setHiddenEditor(true);
  }
  function handleBlur() {
    setHiddenEditor(false);
  }

  function handleChange(event) {
    const { name, value } = event.target;
    if (name === "mensagem")
      setProcedimentoInput({ ...procedimentoInput, mensagem: value });
    else if (name === "questaoId")
      setProcedimentoInput({ ...procedimentoInput, questaoId: value });
    else if (name === "pesquisa") setPesquisa(value);
  }

  function handleSubmit(event) {
    event.preventDefault();
    const { name } = event.target;
    if (name === "new") salvarProcedimento();
    else if (name === "update") atualizarProcedimento();
    else deleteProcedimento(name);
  }

  const handleChangeEditor = (mensagem) => {
    setProcedimentoInput({ ...procedimentoInput, mensagem });
  };

  async function salvarProcedimento() {
    const listaDeAjuda = ajudas
      .filter((a) => right.some((msg) => msg === a.titulo))
      .map((a) => a.id)
      .filter((a) => !!a);

    try {
      await useApi.criarProcedimento(procedimentoInput, listaDeAjuda);
      window.location.reload();
    } catch (exception) {
      alert("Ocorreu um erro inesperado ao tentar criar o procedimento");
    }
  }

  function pesquisarProcedimento() {
    const retorno = procedimento.filter((proc) => {
      return proc.mensagem.toUpperCase().includes(pesquisa.toUpperCase())
        ? true
        : false;
    });

    setProcPesquisado(retorno);
  }

  function handleAtualizarProcedimento(id) {
   // localStorage.setItem("procedimentoId", id);
    const retorno = procedimento.find(
      (procedimento) => procedimento.id === parseInt(id)
    );
    if (!!retorno) setarInformacoesProcedimentoSelecionado(retorno);
  }

  async function setarInformacoesProcedimentoSelecionado(
    procedimentoParaAtualizar
  ) {
    const questaoId = parseInt(procedimentoParaAtualizar.questoesId[0]);
    const mensagem = procedimentoParaAtualizar.mensagem;
    const id = procedimentoParaAtualizar.id;
    setIdToUpdate(id);
    setProcedimentoInput({ ...procedimentoInput, questaoId, mensagem });
    await buscarAjudasDaQuestao(procedimentoInput.questaoId);
  }

  async function buscarAjudasDaQuestao(idQuestao) {
    const retorno = await useApi.buscarAjudas(idQuestao);
    setAjudas(retorno);
    return retorno;
  }

  async function atualizarProcedimento() {
    const listaDeAjudas = ajudas
      .filter((a) => right.some((msg) => msg === a.titulo))
      .map((a) => a.id)
      .filter((a) => !!a);

    try {
      await useApi.atualizarProcedimento(
        idToUpdate,
        procedimentoInput,
        listaDeAjudas
      );
      window.location.reload();
    } catch (exception) {
      alert("Ocorreu um erro inesperado ao tentar atualizar o procedimento");
    }
  }

  async function deleteProcedimento(id) {
    try {
      if (
        window.confirm("Tem certeza que deseja excluir esse porcedimento? ")
      ) {
        await useApi.deleteProcedimento(id);
        window.location.reload();
      }
    } catch (exception) {
      alert("Ocorreu um erro inesperado ao tentar excluir o procedimento");
    }
  }

  return (
    <>
      <Header />
      <div className="padding">
        <Typography variant={"h1"}>
          Fábrica de Procedimentos - {jogador?.nome}
        </Typography>

        <div>
          <Typography variant={"h2"}>Criar Procedimento</Typography>
          <FormLabel>Digite o texto: </FormLabel>
          {!hiddenEditor && (
            <Editor
              handleChange={handleChangeEditor}
              value={procedimentoInput.mensagem}
            />
          )}
          <br />
          <FormLabel>Digite a questão a que queremos</FormLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="questaoId"
            value={procedimentoInput.idConceito}
            onChange={handleChange}
          >
            {questoes.map((questao) => (
              <MenuItem value={questao.id}>{questao.titulo}</MenuItem>
            ))}
          </Select>
          <Button name="buscarAjudas" onClick={handleClick}>
            Buscar Ajudas da questão selecionada
          </Button>
          <br />
          <FormLabel>
            Selecione as ajudas que refernciam esse procedimento para tabela da
            direita:{" "}
          </FormLabel>
          <TransferList
            left={left}
            right={right}
            setLeft={setLeft}
            setRight={setRight}
          />

          <Button name="new" onClick={handleSubmit}>
            Criar Procedimento
          </Button>
          <Button name="update" onClick={handleSubmit}>
            Atualizar Procedimento
          </Button>
        </div>

        <div>
          <Typography variant={"h2"}>Buscar Procedimentos</Typography>

          <OutlinedInput
            id="outlined-adornment-amount"
            name="pesquisa"
            value={pesquisa}
            onChange={handleChange}
            labelWidth={60}
            onFocus={handleFocus}
            onBlur={handleBlur}
          />
          <Button
            variant="outlined"
            name="buscar_procedimento"
            onClick={handleClick}
          >
            Buscar
          </Button>
          <div>
            {procPesquisado.map((procedimento) => (
              <Card>
                <CardContent>
                  <Markup content={procedimento.mensagem} />
                  <CardActions>
                    <Button
                      onClick={() =>
                        handleAtualizarProcedimento(procedimento.id)
                      }
                    >
                      Atualizar
                    </Button>
                    <Button
                      size="small"
                      name={procedimento.id}
                      onClick={handleSubmit}
                    >
                      DELETE
                    </Button>
                  </CardActions>
                </CardContent>
              </Card>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
