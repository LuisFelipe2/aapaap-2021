/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import { Button, CardContent, Input, MenuItem, Select, Typography } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { ProfApi } from "../../../../requestApi/profApi";
import { Header } from "../../../components/header/header";
import { RelatorioPrint } from "./pdf/RelatórioPrint";

export function Relatorio() {
  const [alunos, setAlunos] = useState([]);
  const [aluno, setAluno] = useState("");
  const [idTurma, setIdTurma] = useState(0);
  const [minhasTurma, setMinhasTurma] = useState([]);
  const [dados, setDados] = useState([])

  const componentRef = useRef()
  const useApi = ProfApi();

  useEffect(() => {
    async function buscarMeusAlunos() {
      const retornoApi = await useApi.buscarMeusAlunos();
      setAlunos(retornoApi);
    }

    async function buscarMinhasTurmas() {
      const retornoApi = await useApi.buscarMinhasTurmas();
      setMinhasTurma(retornoApi);
    }

    buscarMeusAlunos();
    buscarMinhasTurmas();
  }, []);


  // const handlePrint = useReactToPrint({
  //   content: () => componentRef.current,
  // })

  function handleChange(event) {
    const { name, value } = event.target;
    if (name === "aluno_email") setAluno(value);
    if (name === "idTurma") setIdTurma(value);
  }
  function handleClick(event) {
    const { name } = event.target;
    if (name === "relatorio_aluno") buscarPeloAluno(aluno);
    else if (name === "relatorio_turma") buscarPelaTurma(idTurma);
  }


  async function buscarPeloAluno(aluno) {
    const response = await useApi.buscarRelatorioPorEmail(aluno);
    setDados(response)
//    handlePrint()
//    RelatorioAluno(JSON.stringify(response));
  }

  async function buscarPelaTurma(idTurma) {
    const response = await useApi.buscarRelatorioPorTurma(idTurma);
    setDados(response)
  //  handlePrint()
  //  RelatorioAluno(JSON.stringify(dados));
  }

  return (
    <>
    <Header />
    <main>
      {dados.length === 0?<>
      <Typography variant="h2">Buscar relatório</Typography>
      <div>
        <Typography variant="body1">
          Digite o email do aluno e clique em baixar pdf
        </Typography>
        <Input name="aluno_email" value={aluno} onChange={handleChange} />
        <Button name="relatorio_aluno" onClick={handleClick}>Baixar pdf</Button>
      </div>
      <div>
        <Typography variant="body1">
          Selecione uma turma e clique em baixar pdf
        </Typography>
        <Select
          className="select_login"
          value={idTurma}
          onChange={handleChange}
          name="idTurma"
          label="Selecione sua turma/curso"
        >
          <MenuItem value={0}></MenuItem>
          {minhasTurma.map((turma) => (
            <MenuItem value={turma.id}>{turma.nome}</MenuItem>
          ))}
        </Select>
        <Button name="relatorio_turma" onClick={handleClick}>Baixar pdf</Button>
      </div>

      <Typography variant="h5">Todos os seus alunos:</Typography>
      <div>
        {alunos
          .filter((a) => {
            if (a.email.includes(aluno) || a.nome.includes(aluno)) return true;
            else return false;
          })
          .sort((a, b) => {
            if (a.nome === b.nome) return 0;
            else if (a.nome > b.nome) return 1;
            else if (a.nome < b.nome) return -1;
          })
          .sort((a, b) => {
            if (a.turma === b.turma) return 0;
            else if (a.turma > b.turma) return 1;
            else if (a.turma < b.turma) return -1;
          })
          .map((a) => (
            <CardContent key={a.id}>
              <Typography variante="body1">Nome: {a.nome}</Typography>
              <Typography variante="body2">Email: {a.email}</Typography>
              <Typography variante="body2">Turma: {a.turma}</Typography>
            </CardContent>
          ))}
      </div>
      </>
      :
      <RelatorioPrint dados={dados} setDados={setDados} ref={componentRef}/>
      }
    </main>
    </>
  );
}
