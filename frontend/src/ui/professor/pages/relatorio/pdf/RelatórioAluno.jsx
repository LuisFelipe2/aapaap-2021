import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";

export function RelatorioAluno(dados) {
  pdfMake.vsf = pdfFonts.pdfMake.vsf;

  const dadosObj = JSON.parse(dados)
  const dadosFormatados = dadosObj.map(dado =>{
    const {turma, nome, email, enunciado, questao, resposta, gabarito, estrategia, 
      grauDeSimilaridade, estrategiaCustomizada, procedimentos, logsAjuda} = dado;

    return JSON.stringify([
        {text: `Usuario: ${nome} - ${turma}`, fontSize: 14, bold: true, margin: [0, 20, 0, 8]},
        {text: `${email}`, fontSize: 10, bold: false, margin: [0, 10, 0, 8]},
        {text: `Questão: ${enunciado}`, fontSize: 12, bold: true, margin: [0, 10, 0, 8]},
        {text: `Enunciado: ${questao}`, fontSize: 10, bold: false, margin: [0, 10, 0, 8]},
        {text: `Gabarito: ${gabarito}`, fontSize: 10, bold: false, margin: [0, 10, 0, 8]},
        {text: `Tentativa: ${resposta}`, fontSize: 12, bold: true, margin: [0, 10, 0, 8]},
        {
          text: `Estrategia: ${estrategia} (Grau de similaridade: ${grauDeSimilaridade? grauDeSimilaridade: "não informado"})`, fontSize: 10, bold: false, margin: [0, 20, 0, 8]
        },
        {text: 'Procedimentos Selecionados: ', style: 'header', margin: [0, 10, 0, 8]},
        {
          ul: procedimentos, fontSize: 10, bold: false
        },
        {text: 'Log de ajudas: ', style: 'header', margin: [0, 10, 0, 8]},
        {
          ul: logsAjuda.map(log => `[${log.hora}] - ${log.ajuda}`), fontSize: 10, bold: false
        },
        {text: `Feedback do aluno: ${estrategiaCustomizada}`, fontSize: 10, bold: false, margin: [0, 20, 0, 8]},
        {text: `------------------------------------------------`, fontSize: 10, bold: false, margin: [0, 20, 0, 8]}
      ]);
  });

  const reportTitle = [];
  const details = [
      dadosFormatados.map(dado => JSON.parse(dado))
  ];
  const rodape = [];

  const docDefinitions = {
    pageSize: "A4",
    pageMargins: [15, 50, 15, 40],

    header: [reportTitle],
    content: [details],
    footer: [rodape],
  };

  pdfMake.createPdf(docDefinitions).download();
}
