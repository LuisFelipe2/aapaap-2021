/* eslint-disable react-hooks/exhaustive-deps */
import { Markup } from "interweave";
import { useEffect } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";


import "./estilo.css"

export function RelatorioPrint({dados, setDados}) {

  const {push} = useHistory();

  useEffect(()=>{
    window.print();
    setDados([])
    push("/professor/relatorio")
  }, []);
  
  return (
    <div>
      {dados.map((dado) => {
        const {
          turma,
          nome,
          email,
          enunciado,
          questao,
          resposta,
          gabarito,
          estrategia,
          grauDeSimilaridade,
          estrategiaCustomizada,
          procedimentos,
          logsAjuda,
        } = dado;

        return (
          <>
            <h3>
              Usuario: {nome} - [{turma}]{" "}
            </h3>
            <span>{email}</span>
            <div>
              <strong>Questão: </strong>
              {enunciado}
            </div>
            <div>
              <strong>Enunciado: </strong>
              <Markup content={questao} />
            </div>
            <div>
              <strong>Gabarito: </strong>
              {gabarito}
            </div>
            <br />
            <p><strong>Resposta: </strong>{resposta}</p>
            <p><>
            <strong>Estrategia: </strong><Markup content={estrategia}/> (Grau de similaridade:{" "}
              {grauDeSimilaridade ? grauDeSimilaridade : "não informado"})
              </>
            </p>
            <p><strong>Procedimentos Selecionados: </strong></p>
            <ul>
              {procedimentos.map((procedimento) => (
                <li><Markup content={procedimento}/></li>
              ))}
            </ul>
            <p><strong>Log de ajudas: </strong></p>
            <ul>
              {logsAjuda.map((log) => (
                <li>
                  <p>[{log.hora.slice(0, 16)}] - {log.ajuda}</p>
                </li>
              ))}
            </ul>
            <p>Feedback do aluno: {estrategiaCustomizada}</p>
            <hr />
          </>
        );
      })}
      </div>
  );
}
