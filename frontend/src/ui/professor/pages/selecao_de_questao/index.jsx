/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  CardContent,
  Typography,
  Input,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { ProfApi } from "../../../../requestApi/profApi";
import Editor from "../../../components/editorQUILL/Editor";
import { Header } from "../../../components/header/header";

import "./estilo.css";

export function SelecaoDeQuestao() {
  const [questao, setQuestao] = useState([]);
  const [hiddenEditor, setHiddenEditor] = useState(false)
  const [questaoInput, setQuestaoInput] = useState({
    titulo: "",
    texto: "",
    resposta: "",
    idTurma: localStorage.getItem("turmaId"),
  });
  const useApi = ProfApi();
  const { push } = useHistory();

  useEffect(() => {
    async function getQuestao() {
      const turmaId = localStorage.getItem("turmaId");
      const retorno = await useApi.buscarMinhasQuestoesPorTurma(turmaId);
      setQuestao(retorno);
    }
    getQuestao();
  }, []);

  function handleClick(id) {
    localStorage.setItem("questaoId", id);
    push("/professor/ajuda");
  }
  function handleDelete(id) {
    deletarQuestao(id);
  }

  function handleChange(event) {
    const { name, value } = event.target;
    if (name === "titulo") setQuestaoInput({ ...questaoInput, titulo: value });
    else if (name === "resposta")
      setQuestaoInput({ ...questaoInput, resposta: value });
  }

  const handleChangeEditor = (texto) => {
    setQuestaoInput({ ...questaoInput, texto });
  }

  function handleFocus(){
    setHiddenEditor(true)
  }
  function handleBlur(){
    setHiddenEditor(false)
  }

  function handleSubmit(event) {
    event.preventDefault();
    salvarQuestao();
  }

  async function deletarQuestao(id) {
    try {
      if (window.confirm("Deseja mesmo excluir essa questão?")) {
        await useApi.deleteQuestao(id);
        window.location.reload();
      }
    } catch (erro) {
      alert("Ocorreu um erro inesperado ao tentar excluir a questão");
    }
  }
  async function salvarQuestao() {
    try {
      await useApi.criarQuestao(questaoInput);
      window.location.reload();
    } catch (error) {
      alert("Ocorreu um erro inesperado ao tentar cadastrar a questão");
    }
  }

  return (
    <>
      <Header />
      <div className="padding">
        <Typography variant={"h1"}>Minhas questões</Typography>

        {questao.map((questao) => (
          <CardContent className="efeito-hover">
            <Typography gutterBottom variant="h5" component="div">
              {questao.titulo}
            </Typography>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button name="navegar" onClick={() => handleClick(questao.id)}>
                Ir para questão
              </Button>
              <Button name="deletar" onClick={() => handleDelete(questao.id)}>
                Deletar questão
              </Button>
            </div>
          </CardContent>
        ))}

        <div>
          <Typography variant={"h2"}>Criar Questao</Typography>
          <formLabelClasses>Digite o título: </formLabelClasses>
          <Input
            value={questaoInput.titulo}
            name="titulo"
            onChange={handleChange}
            onBlur={handleBlur}
            onFocus={handleFocus}
          />
          <br />
          <formLabelClasses>Digite o Enunciado: </formLabelClasses>
          {!hiddenEditor &&
            <Editor handleChange={handleChangeEditor} value={questaoInput.texto} />
          }
          <formLabelClasses>Digite a resposta da questão: </formLabelClasses>
          <Input
            value={questaoInput.resposta}
            name="resposta"
            onChange={handleChange}
            onBlur={handleBlur}
            onFocus={handleFocus}
          />
          <Button onClick={handleSubmit}>Criar questao</Button>
        </div>
      </div>
    </>
  );
}
