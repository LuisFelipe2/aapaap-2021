import { Button, Typography } from "@mui/material";
import { useHistory } from "react-router-dom";
import { Header } from "../../../components/header/header";

export function Home() {
  const usuario = JSON.parse(localStorage.getItem("usuario"));
  const {push} = useHistory();
  return (
    <>
      <Header />
      <Typography variant={"h2"}>Olá, {usuario?.nome}</Typography>
      <Button onClick={() => push("/professor/turma")}>Minhas Turmas</Button>
      <Button onClick={() => push("/professor/ajuda/criar")}>Minhas Ajudas</Button>
      <Button onClick={() => push("/professor/procedimento/criar")}>Meus Procedimentos</Button>
      <Button onClick={() => push("/professor/estrategia/criar")}>Minhas Estratégias</Button>
      <Button onClick={() => push("/professor/relatorio")}>Ver Relatorios</Button>
    </>
  );
}
