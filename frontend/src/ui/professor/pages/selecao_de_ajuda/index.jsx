/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { api } from "../../../../requestApi/api";
import { Header } from "../../../components/header/header";

import "./estilo.css";

export function SelecaoDeAjudas() {
  const [ajuda, setAjuda] = useState([]);
  const jogador = JSON.parse(localStorage.getItem("usuario"));
  const useOldApi = api();

  const { push } = useHistory();

  useEffect(() => {
    async function getQuestao() {
      const retorno = await useOldApi.buscarAjudas(
        localStorage.getItem("questaoId")
      );
      setAjuda(retorno);
    }
    getQuestao();
  }, []);

  function handleClick(id) {
    localStorage.setItem("ajudaId", id);
    push("/professor/procedimento");
  }

  return (
    <>
      <Header />
      <div className="padding">
        <Typography variant={"h1"}>Selecione a ajuda da questão - {jogador?.nome}</Typography>

        {ajuda.map((conceito) => (
          <div className="menu-drop" key={conceito.id}>
            <button type="button" name="drop" key={conceito.id}>
              {conceito.texto}
            </button>
            <div className="drop-content">
              {conceito.ajudas.map((ajuda) => (
                <button
                  name={ajuda.id}
                  key={ajuda.id + conceito.texto}
                  onClick={() => handleClick(ajuda.id)}
                >
                  {ajuda.titulo}
                </button>
              ))}
            </div>
          </div>
        ))}

        <div>
          <Typography variant={"h2"}>Criar Ajuda</Typography>
          <Button onClick={() => push(`/professor/ajuda/criar`)}>Ir Para Criação de Ajuda</Button>
        </div>
      </div>
    </>
  );
}
