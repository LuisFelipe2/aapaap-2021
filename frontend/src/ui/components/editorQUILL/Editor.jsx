/* eslint-disable no-unused-expressions */
/* eslint-disable no-labels */
import React from "react";
import ReactQuill, { Quill } from "react-quill";

// #1 import quill-image-uploader
import ImageUploader from "quill-image-uploader";

import katex from "katex";
import "katex/dist/katex.min.css";
import { useDebouncedCallback } from "use-debounce/lib";
window.katex = katex;

// #2 register module
Quill.register("modules/imageUploader", ImageUploader);

export function Editor({ handleChange, value }) {

  const debounced = useDebouncedCallback((val) => { handleChange(val) })

  const modules = {
    toolbar:  [
      ["bold", "italic", "underline", "strike"],
      ["clean"],
      [{ script: "super" }, { script: "sub" }],
      [{ indent: "-1" }, { indent: "+1" }],
      [{ list: "ordered" }, { list: "bullet" }],
      ["formula"],
      ["image"]
    ]
  }

  const formats = [
    "header",
    "bold",
    "strike",
    "formula",
    "indent",
    "list",
    "script",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "imageBlot", // #5 Optinal if using custom formats
  ];
  // barraBubble: [
  //   ["bold"],
  //   ["italic"],
  //   ["underline"],
  //   ["strike"],
  //   ["formula"],
  //   [{ script: "super" }],
  //   [{ script: "sub" }],
  // ],
  // # 4 Add module and upload function

  // imageUploader: {
  //   upload: (file) => {
  //     return new Promise((resolve, reject) => {
  //       const formData = new FormData();
  //       formData.append("image", file);

  //       fetch(
  //         "https://api.imgbb.com/1/upload?key=d36eb6591370ae7f9089d85875e56b22",
  //         {
  //           method: "POST",
  //           body: formData,
  //         }
  //       )
  //         .then((response) => response.json())
  //         .then((result) => {
  //           console.log(result);
  //           resolve(result.data.url);
  //         })
  //         .catch((error) => {
  //           reject("Upload failed");
  //           console.error("Error:", error);
  //         });
  //     });
  //   },
  // },

  return (
    <ReactQuill
      theme="snow"
      id={'quill'}
      modules={modules}
      formats={formats}
      value={value || ""}
      disable
      onChange={(e) => debounced(e)}
    >
      <div className="my-editing-area" />
    </ReactQuill>
  );
}

export default Editor;
