/* eslint-disable react-hooks/exhaustive-deps */
import "./estilo.css";

import img from "../../../assets/imagens/imagens/logo.png";
import { Avatar, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { api } from "../../../requestApi/api";
import { Box } from "@mui/system";
import Popper from "@mui/material/Popper";
import { useGlobalUser } from "../../../context/user.context";

export function Header() {
  const [, setGlobalUser] = useGlobalUser();
  const [userName, setUserName] = useState("");
  const useApi = api();

  useEffect(() => {
    async function buscarUserName() {
      const userNameRespose = await useApi.buscarInfoUser();
      setUserName(userNameRespose.nome);
      localStorage.setItem("usuario", JSON.stringify(userNameRespose));
    }

    if (
      localStorage.getItem("usuario") === undefined ||
      localStorage.getItem("usuario") === null
    )
      buscarUserName();
    else {
      const usuario = JSON.parse(localStorage.getItem("usuario"));
      setUserName(usuario.nome);
    }
  }, []);

  const [anchorEl, setAnchorEl] = useState(null);

  const handleMouseEnter = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <div className="header" id="header">
      <header>
        <div className="header-container">
          <div className="logo">
            <img src={img} alt="Logo appAAP" />
          </div>
          <div className="imagem_user" onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseEnter}>
            <Typography
              className="p"
              variant="subtitle2"
              gutterBottom
              component="div"
            >
              {userName}
            </Typography>
            <Avatar>{userName.charAt(0).toUpperCase()}</Avatar>
            <Popper id={id} open={open} anchorEl={anchorEl}>
              <Box sx={{ border: 1, p: 1, bgcolor: "white" }}>
                <button onClick={() => setGlobalUser("")}>
                  Logout
                </button>
              </Box>
            </Popper>
          </div>
        </div>
      </header>
    </div>
  );
}
