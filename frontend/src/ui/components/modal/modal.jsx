
import "./estilo.css"

import DR1 from "./cards-ajudas/DR1.jpg"
import DR2 from "./cards-ajudas/DR2.jpg"
import DR3 from "./cards-ajudas/DR3.jpg"
import DR4 from "./cards-ajudas/DR4.jpg"
import DR5 from "./cards-ajudas/DR5.jpg"
import DR6 from "./cards-ajudas/DR6.jpg"
import DR7 from "./cards-ajudas/DR7.jpg"
import DR8 from "./cards-ajudas/DR8.jpg"
import DR9 from "./cards-ajudas/DR9.jpg"
import DR10 from "./cards-ajudas/DR10.jpg"
import DR11 from "./cards-ajudas/DR11.jpg"

import QR1 from "./cards-ajudas/QR1.jpg"
import QR2 from "./cards-ajudas/QR2.jpg"
import QR3 from "./cards-ajudas/QR3.jpg"
import QR4 from "./cards-ajudas/QR4.jpg"
import QR5 from "./cards-ajudas/QR5.jpg"
import QR6 from "./cards-ajudas/QR6.jpg"
import QR7 from "./cards-ajudas/QR7.jpg"
import QR8 from "./cards-ajudas/QR8.jpg"
import QR9 from "./cards-ajudas/QR9.jpg"


import PN1 from "./cards-ajudas/PN1.jpg"
import PN2 from "./cards-ajudas/PN2.jpg"
import PN3 from "./cards-ajudas/PN3.jpg"
import PN4 from "./cards-ajudas/PN4.jpg"
import PN5 from "./cards-ajudas/PN5.jpg"
import PN6 from "./cards-ajudas/PN6.jpg"
import PN7 from "./cards-ajudas/PN7.jpg"
import PN8 from "./cards-ajudas/PN8.jpg"
import PN9 from "./cards-ajudas/PN9.jpg"
import PN10 from "./cards-ajudas/PN10.jpg"
import PN11 from "./cards-ajudas/PN11.jpg"
import PN12 from "./cards-ajudas/PN12.jpg"
import PN13 from "./cards-ajudas/PN13.jpg"
import PN14 from "./cards-ajudas/PN14.jpg"
import PN15 from "./cards-ajudas/PN15.jpg"
import PN16 from "./cards-ajudas/PN16.jpg"
import PN17 from "./cards-ajudas/PN17.jpg"

const TELAS ={
  question: 1,
  help: 2,
  procedimentos: 3,
  estrategia: 4
}

export function Modal({code, setTelaAtual}){

  function handleClick(){
    setTelaAtual(TELAS.question)
  }

  return(
    <>
      <div className="container-modal" onClick={handleClick}>
      <div className="modal-content">

      { code==="DR1" && <img alt="Imagem com conteúdo da ajuda" src={DR1} />}
      { code==="DR1" && <img alt="Imagem com conteúdo da ajuda" src={DR1} />}
      { code==="DR1" && <img alt="Imagem com conteúdo da ajuda" src={DR1} />}
      { code==="DR2" && <img alt="Imagem com conteúdo da ajuda" src={DR2} />}
      { code==="DR3" && <img alt="Imagem com conteúdo da ajuda" src={DR3} />}
      { code==="DR4" && <img alt="Imagem com conteúdo da ajuda" src={DR4} />}
      { code==="DR5" && <img alt="Imagem com conteúdo da ajuda" src={DR5} />}
      { code==="DR6" && <img alt="Imagem com conteúdo da ajuda" src={DR6} />}
      { code==="DR7" && <img alt="Imagem com conteúdo da ajuda" src={DR7} />}
      { code==="DR8" && <img alt="Imagem com conteúdo da ajuda" src={DR8} />}
      { code==="DR9" && <img alt="Imagem com conteúdo da ajuda" src={DR9} />}
      { code==="DR10" && <img alt="Imagem com conteúdo da ajuda"  src={DR10} />}
      { code==="DR11" && <img alt="Imagem com conteúdo da ajuda"  src={DR11} />}

      { code==="QR1" && <img alt="Imagem com conteúdo da ajuda" src={QR1} />}
      { code==="QR2" && <img alt="Imagem com conteúdo da ajuda" src={QR2} />}
      { code==="QR3" && <img alt="Imagem com conteúdo da ajuda" src={QR3} />}
      { code==="QR4" && <img alt="Imagem com conteúdo da ajuda" src={QR4} />}
      { code==="QR5" && <img alt="Imagem com conteúdo da ajuda" src={QR5} />}
      { code==="QR6" && <img alt="Imagem com conteúdo da ajuda" src={QR6} />}
      { code==="QR7" && <img alt="Imagem com conteúdo da ajuda" src={QR7} />}
      { code==="QR8" && <img alt="Imagem com conteúdo da ajuda" src={QR8} />}
      { code==="QR9" && <img alt="Imagem com conteúdo da ajuda" src={QR9} />}

      { code==="PN1" && <img alt="Imagem com conteúdo da ajuda" src={PN1} />}
      { code==="PN2" && <img alt="Imagem com conteúdo da ajuda" src={PN2} />}
      { code==="PN3" && <img alt="Imagem com conteúdo da ajuda" src={PN3} />}
      { code==="PN4" && <img alt="Imagem com conteúdo da ajuda" src={PN4} />}
      { code==="PN5" && <img alt="Imagem com conteúdo da ajuda" src={PN5} />}
      { code==="PN6" && <img alt="Imagem com conteúdo da ajuda" src={PN6} />}
      { code==="PN7" && <img alt="Imagem com conteúdo da ajuda" src={PN7} />}
      { code==="PN8" && <img alt="Imagem com conteúdo da ajuda" src={PN8} />}
      { code==="PN9" && <img alt="Imagem com conteúdo da ajuda" src={PN9} />}
      { code==="PN10" && <img alt="Imagem com conteúdo da ajuda" src={PN10} />}
      { code==="PN11" && <img alt="Imagem com conteúdo da ajuda" src={PN11} />}
      { code==="PN12" && <img alt="Imagem com conteúdo da ajuda" src={PN12} />}
      { code==="PN13" && <img alt="Imagem com conteúdo da ajuda" src={PN13} />}
      { code==="PN14" && <img alt="Imagem com conteúdo da ajuda" src={PN14} />}
      { code==="PN15" && <img alt="Imagem com conteúdo da ajuda" src={PN15} />}
      { code==="PN16" && <img alt="Imagem com conteúdo da ajuda" src={PN16} />}
      { code==="PN17" && <img alt="Imagem com conteúdo da ajuda" src={PN17} />}

      </div>
      </div>
    </>
  )

}