import { useHistory } from "react-router";
import "./estilo.css";

/*TODO -> 
  Esse componente tem por função indicar o caminho percorrido pela avegação do usuário, permitindo voltar
  em páginas anteriores de maneira intuitiva. Porém ele não está sendo usado em todas as páginas que
  possuem essa navbar. Ou exclua esse componente e replica o código nos pontos em que estava sendo utilizado
  ou  abstraia esse componente para ser utilizado em todos os casos que necessitar de barra de navegação.
*/

export function Path({ ajuda = false, setTelaAtual }) {
  const { push } = useHistory();
  function handleClick() {
    setTelaAtual(1);
  }
  return (
    <div className="caminho">
      <ul>
        <li onClick={() => push("/home")} className="link">
          Meus cursos
        </li>
        <li id="cinza">/</li>
        <li onClick={() => push("/atividades")} className="link">
          Situações-problema
        </li>
        <li id="cinza">/</li>

        {!ajuda && (
          <li id="cinza">
            {localStorage.getItem("questao")}
          </li>
        )}

        {ajuda && (
          <>
            <li className="link" onClick={handleClick}>
              {localStorage.getItem("questao")}
            </li>
            <li id="cinza">/</li>
            <li id="cinza">Dicas de resolução</li>
          </>
        )}
      </ul>
    </div>
  );
}
