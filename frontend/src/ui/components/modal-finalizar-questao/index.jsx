
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

export default function AlertDialog({handleClick, grau, open, handleClose}) {
  

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Finalizar questão?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          Avalie a seguinte afirmação: <br />A ESTRATÉGIA IDENTIFICADA
                  PELO APPAAP CORRESPONDE À ESTRATÉGIA QUE DESENVOLVI.<br /><br />
                  Sua resposta: {grau}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClick} color="primary">
            Confirmo
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Voltar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}