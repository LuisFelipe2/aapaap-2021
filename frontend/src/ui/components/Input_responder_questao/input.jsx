import { useState } from "react";
import "./estilo.css"

export function Input({onclick}){

  const [valueInput, setValueInput] = useState("");

  function handleClick(event){
    onclick(event, valueInput)
  }
  function handleChange(event){
    const {value} = event.target
    setValueInput(value)
  }

    function handleSubmit(e){
      e.preventDefault()
      handleClick(e)
    }

  return(
    <form className="Input" name="btn-procedimento" onSubmit={handleSubmit}>
      <input onChange={handleChange} value={valueInput} type="text" id="input" className="Input-text" placeholder="Digite sua resposta aqui" />
      <button type="submit" name="btn-procedimento" htmlFor="input" className="Input-label link">Enviar resposta</button>
    </form>
  );

}

