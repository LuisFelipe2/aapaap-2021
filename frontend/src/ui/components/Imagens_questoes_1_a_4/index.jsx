
import imagem from "../../../assets/imagens/imagens/imagem-arco-vilalangaro.jpeg"
import imagem2 from "../../../assets/imagens/imagens/medidas-arco-vilalangaro.jpeg"
import imagem3 from "../../../assets/imagens/imagens/ESTACIONAMENTO - FIGURA.png"
import imagem4 from "../../../assets/imagens/imagens/PALCO EM CONSTRUÇÃO - FIGURA.png"

export function Imagens({questaoId}){

  return(
    <>
      {questaoId===1 &&<>
        <img src={imagem} alt="Imagem da questão" />
        <img src={imagem2} alt="Imagem da questão" />
        </>
      }
      {questaoId===3 &&
        <img src={imagem3} alt="Imagem da questão" />
      }
      {questaoId===2 &&
        <img src={imagem4} alt="Imagem da questão" />
      }

    </>
  )
}