import createGlobalState from 'react-create-global-state'

const stringifyUser = localStorage.getItem('token')

const user = stringifyUser || ""

const [useGlobalUser, UserGlobalProvider] = createGlobalState(user)

export { useGlobalUser, UserGlobalProvider }