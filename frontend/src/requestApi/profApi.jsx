/* eslint-disable react-hooks/exhaustive-deps */
import { useMemo } from "react";
import { useGlobalUser } from "../context/user.context";
import { useHttp } from "./use-http/use-http";

export function ProfApi() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [user] = useGlobalUser();

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const http = useHttp("https://appaap.herokuapp.com", {
    Authorization: user? `Bearer ${user}`: "",
  })

  function criarUsuario(usuarioObj) {
    const response = http.post("/publico/usuario", usuarioObj);
    return response;
  }
  function gerarToken(loginObj) {
    const response = http.post("/publico/usuario/auth", loginObj);
    return response;
  }
  function sendEmail(email) {
    const response = http.post("/publico/usuario/sendEmail", {email});
    return response;
  }
  function resetarSenha(resetPasswordObj) {
    const response = http.post("/publico/usuario/resetPassword", resetPasswordObj);
    return response;
  }
  function buscarTurmas() {
    return http.get("/publico/turma");
  }
  function login(usuarioObj) {
    const response = http.post("/publico/auth/login", usuarioObj);
    return response;
  }
  function me() {
    return http.get("/publico/usuario");
  }
  function buscarInfoUser() {
    return http.get("/publico/usuario");
  }
  function tokenEhValido() {
    const response = http.post("/publico/usuario/token", user);
    return response;
  }

  ///PROFESSOR
  function criarTurma(turma){
    const response = http.post("/privado/professor/turma", turma);
    return response;
  }
  function buscarMinhasTurmas(){
    const response = http.get("/privado/professor/turma");
    return response;
  }
  function criarQuestao(questao){
    const response = http.post("/privado/professor/questao", questao);
    return response;
  }
  function buscarMinhasQuestoes(){
    const response = http.get("/privado/professor/questao");
    return response;
  }
  function buscarMinhasQuestoesPorTurma(turmaId){
    const response = http.get(`/privado/professor/questao/${turmaId}`);
    return response;
  }
  function criarAjuda(ajuda, questoes){
    const response = http.post("/privado/professor/ajuda", {...ajuda, "questoes": questoes});
    return response;
  }
  function criarProcedimento(procedimento, listaDeAjuda){
    const response = http.post("/privado/professor/procedimento", {...procedimento, "ajudasId": listaDeAjuda});
    return response;
  }
  function criarEstrategia(estrategia, listaDeProcedimentos){
    const response = http.post("/privado/professor/estrategia", {...estrategia, "procedimentosId": listaDeProcedimentos});
    return response;
  }
  function buscarQuestoesPorAjuda(idAjuda){
    const response = http.get(`/privado/professor/ajuda/${idAjuda}`);
    return response
  }
  function buscarTodosProcedimentos(){
    const response = http.get("/privado/professor/procedimento");
    return response;
  }
  function buscarMeusProcedimento(){
    const response = http.get("/privado/professor/procedimento/me");
    return response;
  }
  function buscarMeusProcedimentosPorAjuda(ajudaId, procedimentoId, turmaId){
    const response = http.get(`/privado/professor/procedimento/${ajudaId}/${procedimentoId}/${turmaId}`);
    return response;
  }
  function buscarAjudas(questaoId){
    const response = http.get(`/privado/professor/ajuda/${questaoId}`);
    return response;
  }
  function buscarMinhasAjudas(){
    const response = http.get(`/privado/professor/ajuda`);
    return response;
  }
  function buscarProcedimentos(questaoId){
    const response = http.get(`/privado/professor/procedimento/${questaoId}`);
    return response;
  }
  function buscarMinhasEstrategias(){
    const response = http.get("/privado/professor/estrategia/me");
    return response;
  }
  function atualizarAjuda(id, ajuda, listaDeQuestoes){
    const response = http.post(`/privado/professor/ajuda/${id}`, {...ajuda, "questoes": listaDeQuestoes});
    return response;
  }
  function buscarMeusAlunos(){
    const response = http.get("/privado/professor/aluno");
    return response
  }
  function atualizarEstrategia(id, estrategia, listaDeProcedimentos){
    const response = http.post(`/privado/professor/estrategia/${id}`, {...estrategia, "procedimentosId": listaDeProcedimentos});
    return response;
  }
  function deleteEstrategia(id){
    const response = http.deletar(`/privado/professor/estrategia/${id}`);
    return response;
  }
  function atualizarProcedimento(id, procedimento, listaDeAjudas){
    const response = http.post(`/privado/professor/procedimento/${id}`, {...procedimento, "ajudasId": listaDeAjudas});
    return response;
  }
  function deleteProcedimento(id){
    const response = http.deletar(`/privado/professor/procedimento/${id}`);
    return response;
  }
  function deleteAjuda(id){
    const response = http.deletar(`/privado/professor/ajuda/${id}`);
    return response;
  }
  function deleteTurma(id){
    const response = http.deletar(`/privado/professor/turma/${id}`);
    return response;
  }
  function deleteQuestao(id){
    const response = http.deletar(`/privado/professor/questao/${id}`);
    return response;
  }
  function buscarRelatorioPorEmail(email){
    const response = http.get(`/privado/professor/relatorio/aluno/${email}`);
    return response;
  }
  function buscarRelatorioPorTurma(idTurma){
    const response = http.get(`/privado/professor/relatorio/turma/${idTurma}`);
    return response;
  }
  
  // eslint-disable-next-line react-hooks/rules-of-hooks
  return useMemo(
    () => ({
      login,
      criarUsuario,
      gerarToken,
      resetarSenha,
      sendEmail,
      me,
      buscarInfoUser,
      buscarTurmas,
      tokenEhValido,
      criarTurma,
      criarQuestao,
      criarAjuda,
      criarProcedimento,
      criarEstrategia,
      buscarQuestoesPorAjuda,
      buscarTodosProcedimentos,
      buscarMinhasTurmas,
      buscarMinhasQuestoes,
      buscarMinhasQuestoesPorTurma,
      buscarMeusProcedimentosPorAjuda,
      buscarAjudas,
      buscarMinhasAjudas,
      buscarProcedimentos,
      buscarMeusProcedimento,
      buscarMinhasEstrategias,
      atualizarEstrategia,
      deleteEstrategia,
      atualizarProcedimento,
      atualizarAjuda,
      deleteProcedimento,
      deleteAjuda,
      deleteTurma,
      deleteQuestao,
      buscarMeusAlunos,
      buscarRelatorioPorEmail,
      buscarRelatorioPorTurma
    }),
    [user]
  );
}
