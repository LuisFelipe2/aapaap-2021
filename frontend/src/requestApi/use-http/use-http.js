import { useAxios } from "../use-axios/use-axios"

export function useHttp(baseURL, headers) {
  const instanceAxios = useAxios(baseURL, headers)

  async function get(url, body) {
    const response = await instanceAxios.get(url, body)
    return response.data
  }

  async function deletar(url, body) {
    const response = await instanceAxios.delete(url, body)
    return response.data
  }

  async function post(url, body) {
    
      const response = await instanceAxios.post(url, body)
      return response.data
  }

  return {
    get,
    post,
    deletar,
  }
}