/* eslint-disable react-hooks/exhaustive-deps */
import { useMemo } from "react";
import { useGlobalUser } from "../context/user.context";
import { useHttp } from "./use-http/use-http";

export function api() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [user] = useGlobalUser();

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const http = useHttp("https://appaap.herokuapp.com", {
    Authorization: user? `Bearer ${user}`: "",
  })

  function criarUsuario(usuarioObj) {
    const response = http.post("/publico/usuario", usuarioObj);
    return response;
  }
  function gerarToken(loginObj) {
    const response = http.post("/publico/usuario/auth", loginObj);
    return response;
  }
  function sendEmail(email) {
    const response = http.post("/publico/usuario/sendEmail", {email});
    return response;
  }
  function resetarSenha({token, senha}) {
    const response = http.post("/publico/usuario/resetPassword", {token, senha});
    return response;
  }
  function buscarTurmas() {
    return http.get("/publico/turma");
  }
  function login(usuarioObj) {
    const response = http.post("/publico/auth/login", usuarioObj);
    return response;
  }
  function me() {
    return http.get("/publico/usuario");
  }
  function buscarInfoUser() {
    return http.get("/publico/usuario");
  }
  //Question
  function buscarTodasQuestoes() {
    const response = http.get("/privado/questao");
    return response;
  }
  function buscarQuestao(idQuestao) {
    const response = http.get(`/privado/questao/${idQuestao}`);
    return response;
  }
  //AJUDA
  function buscarAjudas(idquestao) {
    const response = http.get(`/privado/ajuda/${idquestao}`);
    return response;
  }
  function salvarAjudaLog(idTentativa, idAjuda) {
    http.post(`/privado/ajuda`, { idTentativa, idAjuda });
  }
  //TENTATIVA
  function buscarTentativa(idQuestao) {
    return http.get(`/privado/tentativa/${idQuestao}`);
  }
  function iniciarTentativa(idQuestao) {
    return http.post(`/privado/tentativa/${idQuestao}`);
  }
  function encerrarTentativa(idTentativa, resposta) {
    return http.post(`/privado/tentativa`, { idTentativa, resposta });
  }
  function buscarProcedimentos(idTentativa) {
    const response = http.post(
      `/privado/relatorio/procedimentos/${idTentativa}`
    );
    return response;
  }
  function buscarEstrategia(idTentativa, procedimentos) {
    const response = http.post(
      `/privado/relatorio/estrategia/${idTentativa}`,
      procedimentos
    );
    return response;
  }
  function salvarGrauDeSimilaridade(grab, idTentativa) {
    return http.post(`/privado/realtors/${idTentativa}/${grab}`);
  }
  function salvarError(estrategiaCustomizada, idTentativa) {
    const response = http.post("/privado/erro", {
      estrategiaCustomizada,
      idTentativa,
    });
    return response;
  }
  function tokenEhValido() {
    const response = http.post("/publico/usuario/token", user);
    return response;
  }

  // eslint-disable-next-line react-hooks/rules-of-hooks
  return useMemo(
    () => ({
      login,
      criarUsuario,
      gerarToken,
      resetarSenha,
      sendEmail,
      me,
      buscarInfoUser,
      buscarTurmas,
      buscarProcedimentos,
      buscarQuestao,
      buscarAjudas,
      buscarEstrategia,
      tokenEhValido,
      salvarAjudaLog,
      buscarTodasQuestoes,
      iniciarTentativa,
      encerrarTentativa,
      buscarTentativa,
      salvarGrauDeSimilaridade,
      salvarError,
    }),[user]
  );
}
