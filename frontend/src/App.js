/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react';
import { useGlobalUser } from './context/user.context';
import { api } from './requestApi/api';
import './App.css';
import { Routes } from './rotas/Routes';


function App() {

  const [globalUser,setGlobalUser] = useGlobalUser();
  const useApi = api();

  useEffect(()=>{

    async function buscarInfoUser(){
      try{
        const user = await useApi.buscarInfoUser();
        localStorage.setItem("usuario", JSON.stringify(user))
      }catch(exception){
        console.log("Usuário não está logado")
      }
    }

    const usuario = localStorage.getItem("usuario")
    if(!!!usuario) buscarInfoUser();

  }, [])

  useEffect(()=>{

    async function tokenEhValido() {
      const permmit = await useApi.tokenEhValido();
      if(permmit)
        localStorage.setItem("token", globalUser);
      else{
        localStorage.clear()
        setGlobalUser("")
      }
    }

    if(globalUser!=="")
      tokenEhValido()
    else{
      localStorage.clear()
    }

  }, [globalUser])

  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
