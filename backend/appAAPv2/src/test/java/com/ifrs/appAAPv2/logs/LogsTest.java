package com.ifrs.appAAPv2.logs;

import com.ifrs.appAAPv2.base.Base;
import com.ifrs.appAAPv2.repositorio.RelatorioRepository;
import com.ifrs.appAAPv2.representation.request.RelatorioRequest;
import com.ifrs.appAAPv2.serviço.AjudaService;
import com.ifrs.appAAPv2.serviço.EstrategiaService;
import com.ifrs.appAAPv2.serviço.RelatorioService;
import com.ifrs.appAAPv2.serviço.ProcedimentoService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class LogsTest {
    @InjectMocks
    RelatorioService relatorioService;

    @Mock
    RelatorioRepository relatorioRepository;

    @Mock
    ProcedimentoService procedimentosService;

    @Mock
    EstrategiaService estrategiaService;

    @Mock
    AjudaService ajudaService;


    @Test
    public void deveRetornarEstrategiaRealizadaPeloAluno(){

//        //Define
//        RelatorioRequest relatorioRequest = Base.criarRelatorioRequest();
//        List<Integer> responseProcedimento = Base.ListDeIdsProcedimentos();
//        List<Estrategia> responseEstrategia = Base.ListDeIdsEstrategias();
//        List
//
//        Mockito.when(ajudaService.buscarTodasAjudasPorId(relatorioRequest.getAjudas())).thenReturn(responseProcedimento);
//        Mockito.when(procedimentosService.buscarTodosProcedimentosByIdAjudas(relatorioRequest.getAjudas())).thenReturn(responseProcedimento);
//        Mockito.when(estrategiaService.buscarTodasEstrategiasByIdProcedimentos(Mockito.anyList())).thenReturn(responseEstrategia);
////        Mockito.doNothing().when(logsRepository);
//
//        Integer estrategiaFinal = relatorioService.buscarEstrategia(relatorioRequest);
//
//        Assert.assertEquals(3, estrategiaFinal.intValue());
    }

}
