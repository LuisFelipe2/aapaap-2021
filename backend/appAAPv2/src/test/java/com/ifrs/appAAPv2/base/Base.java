package com.ifrs.appAAPv2.base;

//import com.ifrs.appAAPv2.domain.User;
import com.ifrs.appAAPv2.representation.request.RelatorioRequest;
import com.ifrs.appAAPv2.representation.request.UserRequest;

import java.util.ArrayList;
import java.util.List;

public class Base {

    public static RelatorioRequest criarRelatorioRequest(){
        RelatorioRequest relatorioRequest = new RelatorioRequest();
        relatorioRequest.setQuestaoId(1);
        relatorioRequest.setAjudas(ListDeIdsAjudas());
        return relatorioRequest;
    }

    public static UserRequest criarUserRequest() {
        UserRequest usuario = new UserRequest();
        usuario.setNome("Admin");
        usuario.setIdTurma(1);
        usuario.setEmail("admin@mail.com");
//        usuario.setSenha("Admin123");
        return usuario;
    }
//    public static User criarUsuario() {
//        User usuario = new User();
//        usuario.setId(1);
//        usuario.setNome("Admin");
//        usuario.setCurso("Desenvolvimento");
//        usuario.setEmail("admin@mail.com");
//        usuario.setSenha("Admin123");
//
//        return usuario;
//    }




    public static List<String> ListDeIdsAjudas(){
        List<String> responseAjudas = new ArrayList<>();
        responseAjudas.add("2");
        responseAjudas.add("3");
        responseAjudas.add("4");
        responseAjudas.add("18");
        responseAjudas.add("3");
        responseAjudas.add("16");

        return responseAjudas;
    }
    public static List<Integer> ListDeIdsProcedimentos(){
        List<Integer> responseProcedimento = new ArrayList<>();
        responseProcedimento.add(2);
        responseProcedimento.add(3);
        responseProcedimento.add(4);
        responseProcedimento.add(10);
        responseProcedimento.add(3);
        responseProcedimento.add(13);
        responseProcedimento.add(10);
        responseProcedimento.add(23);
        responseProcedimento.add(4);
        responseProcedimento.add(16);

        return responseProcedimento;
    }
    public static List<Integer> ListDeIdsEstrategias(){
        List<Integer> responseEstrategia = new ArrayList<>();
        responseEstrategia.add(2);
        responseEstrategia.add(3);

        return responseEstrategia;
    }

}
