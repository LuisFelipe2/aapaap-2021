package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Relatorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RelatorioRepository extends JpaRepository<Relatorio, Integer> {

    @Query(value = "select * from relatorio where id_tentativa=?1", nativeQuery = true)
    Optional<Relatorio> findByIdTentativa(Integer idTentativa);
}
