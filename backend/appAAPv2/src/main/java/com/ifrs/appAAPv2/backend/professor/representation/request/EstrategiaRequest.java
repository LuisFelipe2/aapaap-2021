package com.ifrs.appAAPv2.backend.professor.representation.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.domain.Questao;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstrategiaRequest {
	String texto;
	List<Integer> procedimentosId;
	Integer idQuestao;
	Integer grauComplexidade;
}
