package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.backend.professor.repository.dto.AlunoDto;
import com.ifrs.appAAPv2.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlunoProfRepository extends JpaRepository<Usuario, Integer> {

	@Query(value = "select u.*, t.nome as turma from usuario u " +
		"join rel_usuario_turma rut on rut.id_usuario = u.id " +
		"join turma t on t.id = rut.id_turma " +
		"where t.nome in ?1", nativeQuery = true)
	List<AlunoDto> findAllMyStudents(List<String> turmas);

	@Query(value = "select u.* from usuario u " +
		"left join rel_usuario_turma rut on rut.id_usuario = u.id " +
		"left join turma t on rut.id_turma = t.id " +
		"where t.id = ?1", nativeQuery = true)
	List<Usuario> buscarAlunosPorTurma(Integer idTurma);
}
