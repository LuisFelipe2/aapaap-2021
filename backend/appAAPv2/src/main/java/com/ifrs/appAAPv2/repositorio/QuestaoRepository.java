package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Questao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestaoRepository extends JpaRepository<Questao, Integer> {

	@Query(value = "select * from questao q " +
		"join rel_questao_turma rqt " +
		"on rqt.id_questao = q.id " +
		"join turma t " +
		"on t.id = rqt.id_turma " +
		"where t.id in ?1", nativeQuery = true)
	List<Questao> findAllMyQuestionByTurma(List<Integer> minhasTurmas);

	@Query(value = "select * from questao q " +
		"limit 4", nativeQuery = true)
	List<Questao> findFirstFour();
}
