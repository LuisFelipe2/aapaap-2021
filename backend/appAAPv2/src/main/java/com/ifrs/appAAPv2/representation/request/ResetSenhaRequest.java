package com.ifrs.appAAPv2.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResetSenhaRequest {
	private String email;
	private String token;
	private String senha;
}
