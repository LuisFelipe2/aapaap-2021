package com.ifrs.appAAPv2.backend.professor.controller;

import com.ifrs.appAAPv2.backend.professor.representation.request.AjudaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.AjudaResponse;
import com.ifrs.appAAPv2.backend.professor.service.AjudaProfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.rmi.UnexpectedException;
import java.util.Set;

@RestController
@RequestMapping("/privado/professor/ajuda")
public class AjudaProfController {
	@Autowired
	AjudaProfService ajudaProfService;

	@GetMapping
	public Set<AjudaResponse> buscarMinhasAjudas() throws Exception {
		return ajudaProfService.buscarMinhasAjudas();
	}

	@PostMapping
	public void criarAjuda(@RequestBody AjudaRequest ajudaRequest) throws Exception {
		ajudaProfService.criarAjuda(ajudaRequest);
	}

	@PostMapping("/{ajudaId}")
	public void atualizarAjuda(@RequestBody AjudaRequest ajudaRequest, @PathVariable String ajudaId) throws Exception {
		ajudaProfService.atualizarAjuda(ajudaRequest, ajudaId);
	}

	@GetMapping("/{questaoId}")
	public Set<AjudaResponse> buscarAjudasPorQuestao(@PathVariable Integer questaoId) throws UnexpectedException {
		return ajudaProfService.buscarAjudasPorQuestao(questaoId);
	}

	@DeleteMapping("/{id}")
	public void deleteAjuda(@PathVariable String id) throws Exception {
		ajudaProfService.deletarAjuda(id);
	}
}
