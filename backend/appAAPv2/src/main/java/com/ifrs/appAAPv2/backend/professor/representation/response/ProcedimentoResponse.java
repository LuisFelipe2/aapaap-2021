package com.ifrs.appAAPv2.backend.professor.representation.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ifrs.appAAPv2.domain.Ajuda;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProcedimentoResponse {
	Integer id;
	String mensagem;
	Boolean isValid;
	List<Integer> questoesId;
	List<String> ajudasId;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ProcedimentoResponse response = (ProcedimentoResponse) o;
		return Objects.equals(id, response.id) && Objects.equals(mensagem, response.mensagem) && Objects.equals(questoesId, response.questoesId) && Objects.equals(ajudasId, response.ajudasId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, mensagem, questoesId, ajudasId);
	}
}
