package com.ifrs.appAAPv2.backend.professor.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AlunoResponse {
	Integer id;
	String nome;
	String email;
	String turma;
}
