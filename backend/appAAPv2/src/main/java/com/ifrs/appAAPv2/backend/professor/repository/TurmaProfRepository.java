package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.domain.Turma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TurmaProfRepository extends JpaRepository<Turma, Integer> {

	@Query(value = "select t.* from turma t " +
		"join rel_usuario_turma r " +
		"on r.id_turma = t.id " +
		"where r.id_usuario = ?1", nativeQuery = true)
	List<Turma> findAllByUser(Integer idUser);
}
