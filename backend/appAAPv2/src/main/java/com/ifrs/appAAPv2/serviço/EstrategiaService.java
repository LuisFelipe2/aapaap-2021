package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.Estrategia;
import com.ifrs.appAAPv2.repositorio.EstrategiaRepository;
import com.ifrs.appAAPv2.representation.dto.RelProcedimentoEstrategiaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstrategiaService {

    @Autowired
    EstrategiaRepository estrategiaRepository;

    @Autowired
    TentativaService tentativaService;

    public List<Estrategia> buscarTodasEstrategiasByIdProcedimentos(List<Integer> responseProcedimento, Integer idQuestao) {

        List<Integer> procedimentosUsados = new ArrayList<>();
        List<RelProcedimentoEstrategiaDTO> rel = estrategiaRepository.buscarVezesQueProcedimentosSeRepetemEmRelacionamento(responseProcedimento, idQuestao);
        if(rel.size() > 0){
            procedimentosUsados.addAll(rel.stream()
                .map(RelProcedimentoEstrategiaDTO::getId)
                .collect(Collectors.toList()));

            return estrategiaRepository.buscarEstrategiasPorIdProcedimento(procedimentosUsados, idQuestao);
        }else{
            List<RelProcedimentoEstrategiaDTO> rel2 = estrategiaRepository.verificarSeEstrategiaRepeteEmTodos(responseProcedimento, idQuestao, responseProcedimento.size());
            if(rel2.size()>0){
                return estrategiaRepository.findAllById(rel2.stream()
                    .map(RelProcedimentoEstrategiaDTO::getId)
                    .collect(Collectors.toList()));
            }
        }
        if(procedimentosUsados.size()==0){
            List<RelProcedimentoEstrategiaDTO> rel3 = estrategiaRepository.buscarQuantitativo();
            procedimentosUsados.addAll(buscarPorQuantitativo(rel3));
        }
        return estrategiaRepository.findAllById(procedimentosUsados);
    }

    public List<Integer> buscarPorQuantitativo(List<RelProcedimentoEstrategiaDTO> rel){

        List<Integer> integers = new ArrayList<>();
        int max = 0;
        for (int x = 0; x<rel.size(); x++){
            if(rel.get(x).getCnt()>max ){
                max = rel.get(x).getCnt();
                List<Integer> nova = new ArrayList<>();
                nova.add(rel.get(x).getId());
                integers = nova;
            }
            else if(rel.get(x).getCnt()==max){
                integers.add(rel.get(x).getId());
            }
        }
        return integers;
    }
}
