package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.repositorio.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurmaService {

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    TurmaRepository turmaRepository;
    public Turma buscarTumaPorId(Integer id) throws Exception {
        return turmaRepository.findById(id).orElseThrow(() -> new Exception("Turma não encontrada"));
    }

    public List<Turma> buscarTodasAsTurmas() {
        List<Turma> turmas = turmaRepository.findAll();
        List<Usuario> usuarios = usuarioService.buscarTodos();

        return turmas.stream()
            .filter(turma -> usuarios.stream().anyMatch(usuario -> usuario.getTurma().contains(turma)))
            .collect(Collectors.toList());
    }
}
