package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.backend.professor.repository.dto.LogAjudaDto;
import com.ifrs.appAAPv2.backend.professor.repository.dto.ProcedimentoDto;
import com.ifrs.appAAPv2.backend.professor.repository.dto.RelatorioDto;
import com.ifrs.appAAPv2.domain.Relatorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RelatorioProfRepository extends JpaRepository<Relatorio, Integer> {

	@Query(value = "select " +
		"t2.nome as turma, " +
		"u.nome as nome, u.email as email, " +
		"q.titulo as enunciado, q.texto as questao, q.resposta as gabarito, " +
		"t.resposta as resposta, t.id as id, " +
		"e2.texto as estrategia, " +
		"e.estrategia_customizada as estrategiaCustomizada, " +
		"r.grau_de_similaridade as grauDeSimilaridade " +
		"from relatorio r left join tentativa t " +
		"on t.id = r.id_tentativa left join questao q " +
		"on q.id = t.id_questao left join usuario u " +
		"on u.id =t.id_usuario left join rel_usuario_turma rut " +
		"on rut.id_usuario = u.id left join turma t2 " +
		"on t2.id = rut.id_turma left join error e " +
		"on e.id_relatorio = r.id left join rel_estrategia_relatorio rer " +
		"on rer.id_relatorio = r.id left join estrategia e2 " +
		"on e2.id = rer.id_estrategia " +
		"where u.email = ?1", nativeQuery = true)
	List<RelatorioDto> getRelatoryByUserEmail(String email);


	@Query(value = "select " +
		"t.id as id, " +
		"p.mensagem as procedimento " +
		"from relatorio r join tentativa t " +
		"on t.id = r.id_tentativa join usuario u " +
		"on u.id = t.id_usuario join rel_procedimentos_relatorio rpr " +
		"on rpr.id_relatorio = r.id join procedimento p " +
		"on p.id = rpr.id_procedimento " +
		"where u.email = ?1", nativeQuery = true)
	List<ProcedimentoDto> getProcedimentoByUserEmail(String email);

	@Query(value = "select " +
		"la.id_tentativa as id, " +
		"a.titulo as ajuda, " +
		"la.hora as hora " +
		"from relatorio r join tentativa t " +
		"on t.id = r.id_tentativa join usuario u " +
		"on u.id = t.id_usuario join log_ajuda la " +
		"on la.id_tentativa = t.id join ajuda a " +
		"on a.id = la.id_ajuda " +
		"where u.email = ?1", nativeQuery = true)
	List<LogAjudaDto> getLogAjudaByUserEmail(String email);
}
