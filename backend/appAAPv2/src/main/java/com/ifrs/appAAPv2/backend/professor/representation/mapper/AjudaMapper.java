package com.ifrs.appAAPv2.backend.professor.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.representation.request.AjudaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.AjudaResponse;
import com.ifrs.appAAPv2.domain.Ajuda;
import com.ifrs.appAAPv2.domain.Questao;

import java.util.List;
import java.util.stream.Collectors;

public class AjudaMapper {
	public Ajuda toDomain(AjudaRequest ajudaRequest, List<Questao> questao) {
		Ajuda ajuda = new Ajuda();
		ajuda.setTexto(ajudaRequest.getTexto());
		ajuda.setId(ajudaRequest.getId());
		ajuda.setTitulo(ajudaRequest.getTitulo());
		ajuda.setQuestoes(questao);
		return ajuda;
	}

	public AjudaResponse toResponse(Ajuda ajuda) {
		AjudaResponse ajudaResponse = new AjudaResponse();
		ajudaResponse.setId(ajuda.getId());
		ajudaResponse.setTitulo(ajuda.getTitulo());
		ajudaResponse.setTexto(ajuda.getTexto());
		ajudaResponse.setQuestoesId(ajuda.getQuestoes().stream()
			.map(Questao::getId)
			.collect(Collectors.toList()));
		return ajudaResponse;
	}
}
