package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Ajuda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AjudaRepository extends JpaRepository<Ajuda, String> {

    @Query(value = "select ajuda.* from ajuda join rel_questao_ajuda \n" +
            "on ajuda.id = rel_questao_ajuda.id_ajuda join questao\n" +
            "on questao.id = rel_questao_ajuda.id_questao\n" +
            "where questao.id = ?1", nativeQuery = true)
    List<Ajuda> findAllByQuestaoId(Integer id);

    @Query(value = "select ajuda.*, conceito.id as id_conceito, conceito.texto from ajuda join rel_questao_ajuda \n" +
            "on ajuda.id = rel_questao_ajuda.id_ajuda join questao\n" +
            "on questao.id = rel_questao_ajuda.id_questao join rel_conceito_ajuda\n" +
            "on ajuda.id = rel_conceito_ajuda.id_ajuda join conceito\n" +
            "on conceito.id = rel_conceito_ajuda.id_conceito\n" +
            "where questao.id = ?1 and id_conceito = 1", nativeQuery = true)
    List<Ajuda> findAllByQuestaoIdConceitoUm(Integer id);

    @Query(value = "select ajuda.*, conceito.id as id_conceito, conceito.texto from ajuda join rel_questao_ajuda \n" +
            "on ajuda.id = rel_questao_ajuda.id_ajuda join questao\n" +
            "on questao.id = rel_questao_ajuda.id_questao join rel_conceito_ajuda\n" +
            "on ajuda.id = rel_conceito_ajuda.id_ajuda join conceito\n" +
            "on conceito.id = rel_conceito_ajuda.id_conceito\n" +
            "where questao.id = ?1 and id_conceito = 2", nativeQuery = true)
    List<Ajuda> findAllByQuestaoIdConceitoDois(Integer id);

    @Query(value = "select ajuda.*, conceito.id as id_conceito, conceito.texto from ajuda join rel_questao_ajuda \n" +
            "on ajuda.id = rel_questao_ajuda.id_ajuda join questao\n" +
            "on questao.id = rel_questao_ajuda.id_questao join rel_conceito_ajuda\n" +
            "on ajuda.id = rel_conceito_ajuda.id_ajuda join conceito\n" +
            "on conceito.id = rel_conceito_ajuda.id_conceito\n" +
            "where questao.id = ?1 and id_conceito = 3", nativeQuery = true)
    List<Ajuda> findAllByQuestaoIdConceitoTres(Integer id);

    @Query(value = "select ajuda.*, conceito.id as id_conceito, conceito.texto from ajuda join rel_questao_ajuda \n" +
            "on ajuda.id = rel_questao_ajuda.id_ajuda join questao\n" +
            "on questao.id = rel_questao_ajuda.id_questao join rel_conceito_ajuda\n" +
            "on ajuda.id = rel_conceito_ajuda.id_ajuda join conceito\n" +
            "on conceito.id = rel_conceito_ajuda.id_conceito\n" +
            "where questao.id = ?1 and id_conceito = 4", nativeQuery = true)
    List<Ajuda> findAllByQuestaoIdConceitoQuatro(Integer id);
}
