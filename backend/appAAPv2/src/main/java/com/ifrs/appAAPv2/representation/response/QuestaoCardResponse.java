package com.ifrs.appAAPv2.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestaoCardResponse {
    Integer id;
    String enunciado;
    String titulo;
    String resposta;
}
