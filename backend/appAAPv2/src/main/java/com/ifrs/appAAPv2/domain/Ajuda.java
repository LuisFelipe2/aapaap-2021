package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "ajuda")
public class Ajuda {

    @Id
    String id;
    String texto;
    String titulo;

//    @JsonIgnore
//    @ManyToMany
//    @JoinTable(name="rel_procedimento_ajuda", joinColumns=
//            {@JoinColumn(name="id_ajuda")}, inverseJoinColumns=
//            {@JoinColumn(name="id_procedimento")})
//    List<Procedimento> procedimentos;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="rel_questao_ajuda", joinColumns=
            {@JoinColumn(name="id_ajuda")}, inverseJoinColumns=
            {@JoinColumn(name="id_questao")})
    List<Questao> questoes;

}
