package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.PasswordResetToken;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.repositorio.PasswordTokenRepository;
import com.ifrs.appAAPv2.repositorio.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    PasswordTokenRepository passwordTokenRepository;
    @Autowired
    UserDetailImpl userDetail;

    public Optional<Usuario> buscarUsuarioPorEmail(String usuarioEmail) throws Exception {
        return usuarioRepository.findByEmail(usuarioEmail);
    }

    public void AtualizarUsuario(Usuario usuario){
    	usuarioRepository.save(usuario);
    }

	public void createPasswordResetTokenForUser(Usuario user, String token) {
		PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordTokenRepository.save(myToken);
	}

	public void resetarSenha(Usuario usuario, String senha) {
    	String senhaCriptografada = userDetail.critografarSenha(senha);
    	usuario.setSenha(senhaCriptografada);
    	usuarioRepository.save(usuario);
	}

	public List<Usuario> buscarTodos() {
    	return usuarioRepository.findAll();
	}
}
