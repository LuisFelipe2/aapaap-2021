package com.ifrs.appAAPv2.representation.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TentativaRequest {
    Integer idQuestao;
    Integer idTentativa;
    String resposta;
}
