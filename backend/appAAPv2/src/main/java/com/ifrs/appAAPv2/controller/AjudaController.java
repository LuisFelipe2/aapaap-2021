package com.ifrs.appAAPv2.controller;


import com.ifrs.appAAPv2.domain.Ajuda;
import com.ifrs.appAAPv2.domain.Conceito;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.representation.request.LogRequest;
import com.ifrs.appAAPv2.representation.response.QuestaoCardResponse;
import com.ifrs.appAAPv2.serviço.AjudaService;
import com.ifrs.appAAPv2.serviço.QuestaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.rmi.UnexpectedException;
import java.util.List;

@RestController
@RequestMapping("/privado/ajuda")
public class AjudaController {

    @Autowired
    AjudaService ajudaService;

    @GetMapping("/{id}")
    public List<Conceito> buscarAjudasPorQuestao(@PathVariable Integer id) throws UnexpectedException {
        return ajudaService.buscarTodasAjudasPorIdQuestao(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void salvarLogAjuda(@RequestBody LogRequest logRequest) throws Exception {
        ajudaService.salvarLogAjuda(logRequest);
    }


}
