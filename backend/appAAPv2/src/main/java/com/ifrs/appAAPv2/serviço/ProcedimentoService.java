package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.Ajuda;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.repositorio.ProcedimentoRepository;
import com.ifrs.appAAPv2.representation.dto.RelProcedimentoEstrategiaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProcedimentoService {

    @Autowired
    ProcedimentoRepository procedimentoRepository;

    public List<Procedimento> buscarTodosProcedimentosByIdAjudas(List<Ajuda> ajudas, Questao idQuestao) {
        return procedimentoRepository.buscarProcedimentosPorAjuda(ajudas, idQuestao.getId());
    }

    public List<Procedimento> buscarTodosProcedimentosPorId(List<Integer> procedimentosUsados) {
        return procedimentoRepository.findAllById(procedimentosUsados);
    }

    public List<RelProcedimentoEstrategiaDTO> buscarProcedimentosPadroes(Integer idQuestao) {
        return procedimentoRepository.buscarProcedimentosPadroes(idQuestao);
    }
}
