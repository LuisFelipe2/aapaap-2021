package com.ifrs.appAAPv2.backend.professor.validate;

import static java.util.Objects.isNull;

public class TurmaValidate {
	public static void validarNomeNulo(String nome) throws Exception {
		if(isNull(nome)){
			throw new Exception("O nome da turma não pode ser nulo");
		}
	}

}
