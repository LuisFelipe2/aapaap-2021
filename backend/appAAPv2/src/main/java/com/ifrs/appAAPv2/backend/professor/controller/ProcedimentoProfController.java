package com.ifrs.appAAPv2.backend.professor.controller;

import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.ProcedimentoIndexRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.ProcedimentoRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.ProcedimentoResponse;
import com.ifrs.appAAPv2.backend.professor.representation.response.QuestaoResponse;
import com.ifrs.appAAPv2.backend.professor.service.ProcedimentoProfService;
import com.ifrs.appAAPv2.domain.Procedimento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/privado/professor/procedimento")
public class ProcedimentoProfController {

	@Autowired
	ProcedimentoProfService procedimentoProfService;

	@PostMapping
	public void criarProcedimento(@RequestBody ProcedimentoRequest procedimentoRequest) throws Exception {
		procedimentoProfService.criarProcedimento(procedimentoRequest);
	}

	@GetMapping
	public Set<ProcedimentoResponse> buscarTodosOsProcedimentos() throws Exception {
		return procedimentoProfService.buscarMeusProcedimentos();
	}

	@GetMapping("/{questaoId}")
	public Set<ProcedimentoResponse> buscarMeusProcedimentos(@PathVariable Integer questaoId) throws Exception {
		return procedimentoProfService.buscarMeusProcedimentosPorQuestao(questaoId);
	}

	@PostMapping("/{procedimentoId}")
	public void atualizarProcedimento(@RequestBody ProcedimentoRequest procedimentoRequest, @PathVariable Integer procedimentoId) throws Exception {
		procedimentoProfService.atualizarProcedimento(procedimentoRequest, procedimentoId);
	}

	@GetMapping("/{ajudaId}/{procedimentoId}/{turmaId}")
	public Set<ProcedimentoResponse> buscarMeusProcedimentosPorAjuda(@PathVariable String ajudaId, @PathVariable Integer procedimentoId, @PathVariable Integer turmaId) throws Exception {
		return procedimentoProfService.buscarMeusProcedimentosPorAjuda(ajudaId, procedimentoId, turmaId);
	}

	@DeleteMapping("/{id}")
	public void deletarProcedimento(@PathVariable Integer id){
		procedimentoProfService.deletarProcedimento(id);
	}

}
