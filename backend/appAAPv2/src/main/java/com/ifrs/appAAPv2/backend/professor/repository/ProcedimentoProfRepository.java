package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.domain.Questao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProcedimentoProfRepository extends JpaRepository<Procedimento, Integer> {

	@Query(value = "select p.*, a.* from procedimento p\n" +
		"join rel_procedimento_ajuda rpa \n" +
		"ON rpa.id_procedimento = p.id\n" +
		"join ajuda a \n" +
		"on rpa.id_ajuda = a.id \n" +
		"join rel_questao_ajuda rqa \n" +
		"on rqa.id_ajuda = a.id \n" +
		"join questao q \n" +
		"on q.id =rqa.id_questao\n" +
		"join rel_questao_turma rqt \n" +
		"on rqt.id_questao = q.id \n" +
		"join turma t\n" +
		"on t.id = rqt.id_turma \n" +
		"join rel_usuario_turma rut \n" +
		"on rut.id_turma = t.id \n" +
		"join usuario u \n" +
		"on u.id = rut.id_usuario \n" +
		"where u.id = ?1 \n" +
		"and t.id = ?2 \n" +
		"and q.id = ?3 \n" +
		"and a.id = ?4", nativeQuery = true)
	List<Procedimento> buscarMeusProcedimentosPorAjuda(Integer usuarioId, Integer turmaId, Integer questaoId, String ajudaId);

	@Query(value = "select p.* from procedimento p\n" +
		"join rel_questao_procedimento rqp\n" +
		"ON rqp.id_procedimento = p.id\n" +
		"where rqp.id_questao = ?1", nativeQuery = true)
	List<Procedimento> findAllByQuestaoId(Integer questaoId);

	@Query(value = "select p.* from procedimento p\n" +
		"join rel_procedimento_ajuda rpa \n" +
		"ON rpa.id_procedimento = p.id\n" +
		"join ajuda a \n" +
		"on rpa.id_ajuda = a.id \n" +
		"join rel_questao_ajuda rqa \n" +
		"on rqa.id_ajuda = a.id \n" +
		"join questao q \n" +
		"on q.id = rqa.id_questao\n" +
		"join rel_questao_turma rqt \n" +
		"on rqt.id_questao = q.id \n" +
		"join turma t\n" +
		"on t.id = rqt.id_turma \n" +
		"join rel_usuario_turma rut \n" +
		"on rut.id_turma = t.id \n" +
		"join usuario u \n" +
		"on u.id = rut.id_usuario \n" +
		"where u.id = ?1", nativeQuery = true)
	List<Procedimento> buscarMeusProcedimentos(Integer usuarioId);
}
