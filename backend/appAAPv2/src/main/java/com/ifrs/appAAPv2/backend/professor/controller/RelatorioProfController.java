package com.ifrs.appAAPv2.backend.professor.controller;

import com.ifrs.appAAPv2.backend.professor.representation.response.RelatorioResponse;
import com.ifrs.appAAPv2.backend.professor.service.RelatorioProfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/privado/professor/relatorio")
public class RelatorioProfController {

	@Autowired
	RelatorioProfService relatorioProfService;

	@GetMapping("/aluno/{email}")
	public List<RelatorioResponse> buscarMeusAlunos(@PathVariable String email) throws Exception {
		return relatorioProfService.buscarRelatorioPorEmail(email);
	}

	@GetMapping("/turma/{idTurma}")
	public List<RelatorioResponse> buscarPorTurma(@PathVariable Integer idTurma) throws Exception {
		return relatorioProfService.buscarRelatorioPorTurma(idTurma);
	}
}

