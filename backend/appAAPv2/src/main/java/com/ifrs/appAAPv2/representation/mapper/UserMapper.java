package com.ifrs.appAAPv2.representation.mapper;


import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.representation.request.UserRequest;
import com.ifrs.appAAPv2.representation.response.AuthResponse;
import org.springframework.security.core.userdetails.UserDetails;

public class UserMapper {

    public Usuario toDomain(UserRequest userRequest){
        Usuario usuario = new Usuario();
//        usuario.setSenha(userRequest.getSenha());
        usuario.setEmail(userRequest.getEmail());
        usuario.setNome(userRequest.getNome());
        return usuario;
    }

}
