package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Estrategia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String texto;
    Integer grauComplexidade;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="rel_procedimento_estrategia", joinColumns=
            {@JoinColumn(name="id_estrategia")}, inverseJoinColumns=
            {@JoinColumn(name="id_procedimento")})
    List<Procedimento> procedimentos;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="id_questao")
    Questao idQuestao;
}
