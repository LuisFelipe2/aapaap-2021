package com.ifrs.appAAPv2.backend.professor.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RelatorioResponse {
	String turma;
	String nome;
	String email;
	String enunciado;
	String questao;
	String resposta;
	String gabarito;
	String estrategia;
	List<String> procedimentos;
	List<LogAjudaResponse> logsAjuda;
	String grauDeSimilaridade;
	String estrategiaCustomizada;
}
