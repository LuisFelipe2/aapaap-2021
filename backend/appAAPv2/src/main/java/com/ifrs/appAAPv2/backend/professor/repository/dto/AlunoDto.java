package com.ifrs.appAAPv2.backend.professor.repository.dto;

import com.ifrs.appAAPv2.domain.Turma;

public interface AlunoDto {
	public Integer getId();
	public String getNome();
	public String getEmail();
	public String getTurma();
}
