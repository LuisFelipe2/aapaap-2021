package com.ifrs.appAAPv2.backend.professor.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuestaoResponse {
	Integer id;
	String titulo;
	String enunciado;
	String resposta;
}
