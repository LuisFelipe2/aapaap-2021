package com.ifrs.appAAPv2.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "resetar_senha")
@NoArgsConstructor
public class PasswordResetToken {
	private static final int EXPIRATION_MINUTES = 60 * 24;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "reset_token")
	private String token;

	@OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false, name = "id_user")
	private Usuario user;

	@Column(name = "expiracao")
	private LocalDateTime expiryDate;

	public PasswordResetToken(String token, Usuario user) {
		this.token = token;
		this.user = user;
		this.expiryDate = LocalDateTime.now().plusMinutes(EXPIRATION_MINUTES);
	}
}
