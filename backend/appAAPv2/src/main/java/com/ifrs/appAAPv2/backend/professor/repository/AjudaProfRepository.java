package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.domain.Ajuda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AjudaProfRepository extends JpaRepository<Ajuda, String> {
	@Query(value = "select ajuda.* from ajuda join rel_questao_ajuda \n" +
		"on ajuda.id = rel_questao_ajuda.id_ajuda join questao\n" +
		"on questao.id = rel_questao_ajuda.id_questao\n" +
		"where questao.id = ?1", nativeQuery = true)
	List<Ajuda> findByQuestaoId(Integer questaoId);

	@Query(value = "select ajuda.* from ajuda join rel_questao_ajuda\n" +
		"on ajuda.id = rel_questao_ajuda.id_ajuda join questao q\n" +
		"on q.id = rel_questao_ajuda.id_questao\n" +
		"join rel_questao_turma rqt\n" +
		"on rqt.id_questao = q.id\n" +
		"join turma t\n" +
		"on t.id = rqt.id_turma \n" +
		"join rel_usuario_turma r\n" +
		"on r.id_turma = t.id\n" +
		"where r.id_usuario = ?1", nativeQuery = true)
	List<Ajuda> findAllByUsarioId(Integer id);

	@Query(value = "select count(*) from ajuda\n" +
		"where ajuda.id like ?1",nativeQuery = true)
	long contAjudasPorTipo(String id);
}
