package com.ifrs.appAAPv2.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Conceito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String texto;

    @ManyToMany
    @JoinTable(name="rel_conceito_ajuda", joinColumns=
                    {@JoinColumn(name="id_conceito")}, inverseJoinColumns=
                    {@JoinColumn(name="id_ajuda")})
    List<Ajuda> ajudas;
}
