package com.ifrs.appAAPv2.backend.professor.service;

import com.ifrs.appAAPv2.backend.professor.repository.RelatorioProfRepository;
import com.ifrs.appAAPv2.backend.professor.repository.dto.LogAjudaDto;
import com.ifrs.appAAPv2.backend.professor.repository.dto.ProcedimentoDto;
import com.ifrs.appAAPv2.backend.professor.repository.dto.RelatorioDto;
import com.ifrs.appAAPv2.backend.professor.representation.mapper.RelatorioMapper;
import com.ifrs.appAAPv2.backend.professor.representation.response.AlunoResponse;
import com.ifrs.appAAPv2.backend.professor.representation.response.RelatorioResponse;
import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.serviço.TurmaService;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RelatorioProfService {

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	RelatorioProfRepository relatorioService;

	@Autowired
	TurmaService turmaService;

	@Autowired
	AlunoProfService alunoProfService;

	RelatorioMapper relatorioMapper = new RelatorioMapper();

	public  List<RelatorioResponse> buscarRelatorioPorEmail(String email) throws Exception {

		Usuario usuario = usuarioService.buscarUsuarioPorEmail(email)
			.orElseThrow(()->new Exception("Ocorreu um erro ao tentar buscar o usuário"));

		List<RelatorioDto> relatorioDtos = relatorioService.getRelatoryByUserEmail(email);
		List<ProcedimentoDto> procedimentoDtos = relatorioService.getProcedimentoByUserEmail(email);
		List<LogAjudaDto> logAjudaDtos = relatorioService.getLogAjudaByUserEmail(email);
		return relatorioDtos.stream()
			.map(relatorioDto -> relatorioMapper.toResponse(relatorioDto, procedimentoDtos, logAjudaDtos))
			.collect(Collectors.toList());
	}

	public List<RelatorioResponse> buscarRelatorioPorTurma(Integer idTurma) throws Exception {
		Turma turma = turmaService.buscarTumaPorId(idTurma);

		List<AlunoResponse> alunoResponses = alunoProfService.buscarMeusAlunos();

		List<String> alunosEmail = alunoResponses.stream()
			.filter(aluno -> aluno.getTurma().equals(turma.getNome()))
			.map(AlunoResponse::getEmail)
			.collect(Collectors.toList());

		List<RelatorioResponse> relatorioResponses = new ArrayList<>();
		for(String email : alunosEmail){
			relatorioResponses.addAll(buscarRelatorioPorEmail(email));
		}
		return relatorioResponses;
	}
}
