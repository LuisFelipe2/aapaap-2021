package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Estrategia;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.representation.dto.RelProcedimentoEstrategiaDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface EstrategiaRepository extends JpaRepository<Estrategia, Integer> {
    @Query(value = "select estrategia.* from procedimento\n" +
            "join rel_procedimento_estrategia on procedimento.id = rel_procedimento_estrategia.id_procedimento\n" +
            "join estrategia on estrategia.id = rel_procedimento_estrategia.id_estrategia \n" +
            "WHERE procedimento.id in ?1 and estrategia.id_questao = ?2", nativeQuery = true)
    List<Estrategia> buscarEstrategiasPorIdProcedimento(List<Integer> idProcedimentos, Integer id);

    @Query(value = "select rel_procedimento_estrategia.id_procedimento as id, count(*) as cnt from procedimento\n" +
            "join rel_procedimento_estrategia on procedimento.id = rel_procedimento_estrategia.id_procedimento\n" +
            "join estrategia on estrategia.id = rel_procedimento_estrategia.id_estrategia\n" +
            "WHERE procedimento.id in ?1 and estrategia.id_questao = ?2\n" +
            "group by id_procedimento\n" +
            "having count(*)=1", nativeQuery = true)
    List<RelProcedimentoEstrategiaDTO> buscarVezesQueProcedimentosSeRepetemEmRelacionamento(List<Integer> idProcedimentos, Integer id);


    @Query(value = "select estrategia.id as id, count(*) as cnt from estrategia \n" +
            "join rel_procedimento_estrategia rpe on estrategia.id = rpe.id_estrategia \n" +
            "join procedimento on procedimento.id = rpe.id_procedimento \n" +
            "WHERE procedimento.id in ?1 and estrategia.id_questao = ?2\n" +
            "group by estrategia.id\n" +
            "having count(*)=?3 ", nativeQuery = true)
    List<RelProcedimentoEstrategiaDTO> verificarSeEstrategiaRepeteEmTodos(List<Integer> idProcedimentos, Integer id, Integer total);

    @Query(value = "select estrategia.id as id, count(*) as cnt from estrategia \n" +
            "join rel_procedimento_estrategia rpe on estrategia.id = rpe.id_estrategia \n" +
            "join procedimento on procedimento.id = rpe.id_procedimento \n" +
            "WHERE procedimento.id in ?1 and estrategia.id_questao = ?2\n" +
            "group by estrategia.id\n", nativeQuery = true)
    List<RelProcedimentoEstrategiaDTO> buscarQuantitativo();
}
