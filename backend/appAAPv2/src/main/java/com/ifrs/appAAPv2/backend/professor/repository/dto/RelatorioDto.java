package com.ifrs.appAAPv2.backend.professor.repository.dto;

import java.util.List;

public interface RelatorioDto {

	public Integer getId();
	public String getTurma();
	public String getNome();
	public String getEmail();
	public String getEnunciado();
	public String getQuestao();
	public String getResposta();
	public String getGabarito();
	public String getEstrategia();
	public String getGrauDeSimilaridade();
	public String getEstrategiaCustomizada();

}
