package com.ifrs.appAAPv2.backend.professor.service;

import com.ifrs.appAAPv2.backend.professor.repository.TurmaProfRepository;
import com.ifrs.appAAPv2.backend.professor.representation.mapper.TurmaMapper;
import com.ifrs.appAAPv2.backend.professor.representation.request.TurmaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.TurmaResponse;
import com.ifrs.appAAPv2.backend.professor.validate.TurmaValidate;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.serviço.UserDetailImpl;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class TurmaProfService {

	@Autowired
	TurmaProfRepository turmaProfRepository;

	TurmaMapper turmaMapper = new TurmaMapper();

	@Autowired
	UserDetailImpl userDetails;

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	AlunoProfService alunoProfService;

	@Autowired
	QuestaoProfService questaoProfService;

	public void CriarTurma(TurmaRequest turmaRequest) throws Exception {
		TurmaValidate.validarNomeNulo(turmaRequest.getNome());

		Usuario usuario = getUser();

		Turma turma = turmaMapper.toDomain(turmaRequest);

		Turma turma2 = turmaProfRepository.save(turma);
		usuario.getTurma().add(turma2);
		usuarioService.AtualizarUsuario(usuario);
	}

	public List<TurmaResponse> buscarMinhasTurmas() throws Exception {
		Usuario usuario = getUser();
		List<Turma> turmas = turmaProfRepository.findAllByUser(usuario.getId());

		return turmas.stream()
			.map(turma -> turmaMapper.toResponse(turma))
			.collect(Collectors.toList());
	}

	private Usuario getUser() throws Exception {
		String emailUsuario = userDetails.me();
		return usuarioService
			.buscarUsuarioPorEmail(emailUsuario)
			.orElseThrow(() -> new Exception("Usuário não encontrado"));
	}

	public void deletarTurmaPorId(Integer id) throws Exception {
		Turma turma = turmaProfRepository.findById(id)
			.orElseThrow(() -> new Exception("Turma não encontrada"));
		Usuario eu = getUser();

		if(!eu.getTurma().contains(turma))
			throw new Exception("Você não tem permissão para excluir essa turma");

		try{
			turmaProfRepository.delete(turma);
		}catch (Exception e) {
			List<Usuario> alunos = alunoProfService.buscarAlunosPorTurma(turma.getId());
			List<Questao> questoes = questaoProfService.buscarQuesoesPorTurma(eu.getId(), turma.getId());

			alunos.forEach(aluno -> aluno.getTurma().remove(turma));
			questoes.forEach(questao -> questao.getTurma().remove(turma));

			alunoProfService.atualizarTodos(alunos);
			questaoProfService.atualizarTodos(questoes);
		}
	}
}
