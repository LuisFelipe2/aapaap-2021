package com.ifrs.appAAPv2.backend.professor.validate;

import com.ifrs.appAAPv2.backend.professor.representation.request.ProcedimentoIndexRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.ProcedimentoRequest;

import static java.util.Objects.isNull;

public class ValidateProcedimentos {
	public static void validarParametrosNull(ProcedimentoRequest procedimentoRequest) throws Exception {
		if(isNull(procedimentoRequest.getAjudasId()) ||
			isNull(procedimentoRequest.getMensagem()) ||
			isNull(procedimentoRequest.getQuestaoId())){
			throw new Exception("nenhum parâmetro deve ser nulo");
		}
	}

	public static void validarParametrosNullIndex(ProcedimentoIndexRequest procedimentoRequest) throws Exception {
		if(isNull(procedimentoRequest.getAjudasId()) ||
			isNull(procedimentoRequest.getProcedimentoId())){
			throw new Exception("nenhum parâmetro deve ser nulo");
		}
	}
}
