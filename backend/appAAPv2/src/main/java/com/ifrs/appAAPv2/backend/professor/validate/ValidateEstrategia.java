package com.ifrs.appAAPv2.backend.professor.validate;

import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaIndexRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaRequest;

import static java.util.Objects.isNull;

public class ValidateEstrategia {
	public static void validarParametrosNull(EstrategiaRequest estrategiaRequest) throws Exception {
		if(isNull(estrategiaRequest.getIdQuestao()) ||
			isNull(estrategiaRequest.getTexto()) ||
			isNull(estrategiaRequest.getProcedimentosId())){
			throw new Exception("nenhum parâmetro deve ser nulo");
		}
	}

	public static void validarParametrosNullIndex(EstrategiaIndexRequest estrategiaIndexRequest) throws Exception {
		if(isNull(estrategiaIndexRequest.getIdQuestao()) ||
			isNull(estrategiaIndexRequest.getIdEstrategia()) ||
			isNull(estrategiaIndexRequest.getProcedimentosId())){
			throw new Exception("nenhum parâmetro deve ser nulo");
		}
	}
}
