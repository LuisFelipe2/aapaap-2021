package com.ifrs.appAAPv2.backend.professor.representation.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ifrs.appAAPv2.domain.Ajuda;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcedimentoRequest {
	String mensagem;
	List<String> ajudasId;
	Integer questaoId;
}
