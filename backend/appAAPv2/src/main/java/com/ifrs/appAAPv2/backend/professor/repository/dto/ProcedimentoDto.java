package com.ifrs.appAAPv2.backend.professor.repository.dto;

import java.util.List;

public interface ProcedimentoDto {

	public String getProcedimento();
	public Integer getId();
}
