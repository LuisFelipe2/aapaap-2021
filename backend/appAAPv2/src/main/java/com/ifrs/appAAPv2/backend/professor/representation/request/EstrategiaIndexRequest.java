package com.ifrs.appAAPv2.backend.professor.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstrategiaIndexRequest {
	Integer idEstrategia;
	List<Integer> procedimentosId;
	Integer idQuestao;
}
