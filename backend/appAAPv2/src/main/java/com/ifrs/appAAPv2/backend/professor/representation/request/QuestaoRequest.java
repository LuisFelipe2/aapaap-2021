package com.ifrs.appAAPv2.backend.professor.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestaoRequest {
	String titulo;
	String texto;
	String resposta;
	Integer idTurma;
}
