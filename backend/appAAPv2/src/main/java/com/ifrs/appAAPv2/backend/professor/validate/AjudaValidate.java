package com.ifrs.appAAPv2.backend.professor.validate;

import com.ifrs.appAAPv2.backend.professor.representation.request.AjudaIndexRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.AjudaRequest;

import static java.util.Objects.isNull;
import static org.springframework.util.CollectionUtils.isEmpty;

public class AjudaValidate {
	public static void validarParametrosNulo(AjudaRequest ajudaRequest) throws Exception {
		if(isEmpty(ajudaRequest.getQuestoes()) ||
			isNull(ajudaRequest.getTexto()) ||
			isNull(ajudaRequest.getTitulo())){
			throw new Exception("nenhum parâmetro deve ser nulo");
		}
	}

	public static void validarParametrosNuloIndex(AjudaIndexRequest ajudaRequest) throws Exception {
		if(isNull(ajudaRequest.getIdQuestao()) ||
			isNull(ajudaRequest.getIdAjuda())){
			throw new Exception("nenhum parâmetro deve ser nulo");
		}
	}
}
