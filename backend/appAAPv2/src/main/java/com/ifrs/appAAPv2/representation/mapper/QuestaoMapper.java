package com.ifrs.appAAPv2.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.representation.request.QuestaoRequest;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.representation.response.QuestaoCardResponse;

public class QuestaoMapper {
    
    public QuestaoCardResponse toResponseList(Questao questao){
        return new QuestaoCardResponse(questao.getId(), 
                questao.getTexto(), questao.getTitulo(), questao.getResposta());
    }

}
