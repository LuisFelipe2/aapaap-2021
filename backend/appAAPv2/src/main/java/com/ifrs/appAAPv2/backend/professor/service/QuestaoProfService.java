package com.ifrs.appAAPv2.backend.professor.service;

import com.ifrs.appAAPv2.backend.professor.repository.TurmaProfRepository;
import com.ifrs.appAAPv2.backend.professor.representation.mapper.QuestaoMapper;
import com.ifrs.appAAPv2.backend.professor.representation.request.QuestaoRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.QuestaoResponse;
import com.ifrs.appAAPv2.backend.professor.validate.QuestaoValidate;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.backend.professor.repository.QuestaoProfRepository;
import com.ifrs.appAAPv2.serviço.UserDetailImpl;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestaoProfService {

	@Autowired
	QuestaoProfRepository questaoRepository;

	@Autowired
	TurmaProfRepository turmaProfRepository;

	@Autowired
	UserDetailImpl userDetails;

	@Autowired
	UsuarioService usuarioService;

	QuestaoMapper questaoMapper = new QuestaoMapper();

	public void criarQuestao(QuestaoRequest questaoRequest) throws Exception {
		QuestaoValidate.validarParametrosNulo(questaoRequest);

		Usuario usuario = getUser();
		Turma turma = turmaProfRepository.findById(questaoRequest.getIdTurma())
			.orElseThrow(() -> new Exception("Id de turma não válido"));

		if(!usuario.getTurma().contains(turma)){
			throw new Exception("O usuário não é professor da turma que tentou cadastrar a questão");
		}

		Questao questao = questaoMapper.toDomain(questaoRequest, turma);
		questaoRepository.save(questao);
	}

	public List<QuestaoResponse> buscarMinhasQuestoes() throws Exception {
		Usuario usuario = getUser();
		List<Questao> questoes = questaoRepository.buscarMinhasQuestoes(usuario.getId());
		return questoes.stream()
			.map(questao -> questaoMapper.toResponse(questao))
			.collect(Collectors.toList());
	}

	public List<QuestaoResponse> buscarMinhasQuestoesPorTurma(Integer idTurma) throws Exception {
		Usuario usuario = getUser();
		List<Questao> questoes = buscarQuesoesPorTurma(usuario.getId(), idTurma);
		return questoes.stream()
			.map(questao -> questaoMapper.toResponse(questao))
			.collect(Collectors.toList());
	}

	public List<Questao> buscarQuesoesPorTurma(Integer idUsuario, Integer idTurma) {
		return questaoRepository.buscarQuestoesPorTurma(idUsuario, idTurma);
	}
	private Usuario getUser() throws Exception {
		String emailUsuario = userDetails.me();
		return usuarioService
			.buscarUsuarioPorEmail(emailUsuario)
			.orElseThrow(() -> new Exception("Usuário não encontrado"));
	}

	public Questao buscarQuestaoPorId(Integer questaoId) throws Exception {
		return questaoRepository.findById(questaoId).orElseThrow(() -> new Exception("Não foi encontrada a questão pelo id"));
	}

	public void deletarQuestaoPorId(Integer id) throws Exception {
		Questao questao = questaoRepository.findById(id)
			.orElseThrow(() -> new Exception("Questão não encontrada"));

		Usuario eu = getUser();
		List<Questao> minhasQuestoes = questaoRepository.buscarMinhasQuestoes(eu.getId());

		if(!minhasQuestoes.contains(questao))
			throw new Exception("Você não tem permissão para excluir essa turma");

		try{
			questaoRepository.delete(questao);
		}catch (Exception e) {
			questao.setTurma(new ArrayList<>());
			questaoRepository.save(questao);
		}
	}

	public void atualizarTodos(List<Questao> questoes) {
		questaoRepository.saveAll(questoes);
	}
}
