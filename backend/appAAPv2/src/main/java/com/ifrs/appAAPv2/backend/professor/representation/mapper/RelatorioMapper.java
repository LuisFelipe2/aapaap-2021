package com.ifrs.appAAPv2.backend.professor.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.repository.dto.LogAjudaDto;
import com.ifrs.appAAPv2.backend.professor.repository.dto.ProcedimentoDto;
import com.ifrs.appAAPv2.backend.professor.repository.dto.RelatorioDto;
import com.ifrs.appAAPv2.backend.professor.representation.response.LogAjudaResponse;
import com.ifrs.appAAPv2.backend.professor.representation.response.RelatorioResponse;

import java.util.ArrayList;
import java.util.List;

public class RelatorioMapper {
	public RelatorioResponse toResponse(RelatorioDto relatorioDto,
	                                    List<ProcedimentoDto> procedimentoSelecionadoDto,
	                                    List<LogAjudaDto> logAjudaDtos){
		RelatorioResponse relatorioResponse = new RelatorioResponse();
		relatorioResponse.setEmail(relatorioDto.getEmail());
		relatorioResponse.setEnunciado(relatorioDto.getEnunciado());
		relatorioResponse.setResposta(relatorioDto.getResposta());
		relatorioResponse.setEstrategia(relatorioDto.getEstrategia());
		relatorioResponse.setGabarito(relatorioDto.getGabarito());
		relatorioResponse.setEstrategiaCustomizada(relatorioDto.getEstrategiaCustomizada());
		relatorioResponse.setNome(relatorioDto.getNome());
		relatorioResponse.setGrauDeSimilaridade(relatorioDto.getGrauDeSimilaridade());
		relatorioResponse.setQuestao(relatorioDto.getQuestao());
		relatorioResponse.setTurma(relatorioDto.getTurma());
		relatorioResponse.setEstrategiaCustomizada(relatorioDto.getEstrategiaCustomizada());

		relatorioResponse.setProcedimentos(new ArrayList<>());
		for (ProcedimentoDto procedimentoDto : procedimentoSelecionadoDto){
			if(relatorioDto.getId().equals(procedimentoDto.getId())) {
				relatorioResponse.getProcedimentos().add(procedimentoDto.getProcedimento());
			}
		}

		relatorioResponse.setLogsAjuda(new ArrayList<>());
		for (LogAjudaDto logAjudaDto : logAjudaDtos){
			Integer procedimentoTentativaId = logAjudaDto.getId();
			Integer relatorioTentativaId = relatorioDto.getId();
			if(relatorioDto.getId().equals(logAjudaDto.getId())){
				LogAjudaResponse logAjudaResponse = new LogAjudaResponse(logAjudaDto.getAjuda(), logAjudaDto.getHora());
				relatorioResponse.getLogsAjuda().add(logAjudaResponse);
			}
		}

		return relatorioResponse;
	}
}
