package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.Error;
import com.ifrs.appAAPv2.domain.Relatorio;
import com.ifrs.appAAPv2.domain.Tentativa;
import com.ifrs.appAAPv2.repositorio.ErrorRepository;
import com.ifrs.appAAPv2.representation.request.ErroRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ErroService {

    @Autowired
    RelatorioService relatorioService;
    @Autowired
    TentativaService tentativaService;

    @Autowired
    ErrorRepository errorRepository;

    public Error salvarErro(ErroRequest erroRequest) throws Exception {
        Optional<Relatorio> relatorio = relatorioService.buscarRelatorioPorIdTentativa(erroRequest.getIdTentativa());
        Error error  = new Error();
        error.setIdRelatorio(relatorio.get().getId());
        error.setEstrategiaCustomizada(erroRequest.getEstrategiaCustomizada());

        return errorRepository.save(error);
    }
}
