package com.ifrs.appAAPv2.controller;


import com.ifrs.appAAPv2.domain.Estrategia;
import com.ifrs.appAAPv2.domain.Tentativa;
import com.ifrs.appAAPv2.representation.request.TentativaRequest;
import com.ifrs.appAAPv2.serviço.RelatorioService;
import com.ifrs.appAAPv2.serviço.TentativaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/privado/tentativa")
public class TentativaController {

    @Autowired
    TentativaService tentativaService;

    @GetMapping("/{idQuestao}")
    @ResponseStatus(HttpStatus.CREATED)
    public Tentativa BuscarTentativa(@PathVariable Integer idQuestao) throws Exception {
        Tentativa r = tentativaService.buscarTentativa(idQuestao);
        return r;
    }

    @PostMapping("/{idQuestao}")
    @ResponseStatus(HttpStatus.CREATED)
    public Integer SalvarTentativa(@PathVariable Integer idQuestao) throws Exception {
        Integer r = tentativaService.salvarTentativa(idQuestao);
        return r;
    }
    @PostMapping
    public Tentativa responder(@RequestBody TentativaRequest tentativaRequest) throws Exception {
        return tentativaService.responder(tentativaRequest);
    }

}
