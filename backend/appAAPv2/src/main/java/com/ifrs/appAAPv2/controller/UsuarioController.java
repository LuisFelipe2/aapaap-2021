package com.ifrs.appAAPv2.controller;


import com.ifrs.appAAPv2.JwtService.JwtService;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.representation.request.AuthRequest;
import com.ifrs.appAAPv2.representation.request.ResetSenhaRequest;
import com.ifrs.appAAPv2.representation.request.SenderEmailRequest;
import com.ifrs.appAAPv2.representation.request.UserRequest;
import com.ifrs.appAAPv2.representation.response.AuthResponse;
import com.ifrs.appAAPv2.serviço.PasswordTokenService;
import com.ifrs.appAAPv2.serviço.UserDetailImpl;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/publico/usuario")
@RequiredArgsConstructor
public class UsuarioController {

    private final UserDetailImpl userDetail;
    private final JwtService jwtService;

    @Autowired
    UsuarioService usuarioService;
    @Autowired
    PasswordTokenService passwordTokenService;

    @Value("${APP_URL}")
    String appUrl;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario saveUser(@RequestBody @Valid UserRequest userRequest) throws Exception {
        return userDetail.salvarUsuario(userRequest);
    }

    @PostMapping("/auth")
    public AuthResponse logar(@RequestBody @Valid AuthRequest authRequest){
        Usuario usuario = Usuario.builder().email(authRequest.getLogin()).build();
        userDetail.autenticar(authRequest);
        String token = jwtService.gerarToken(usuario);
        return new AuthResponse(usuario.getEmail(), token);
    }

    @PostMapping("/token")
    public boolean verificarToken(@RequestBody String token){
        return jwtService.tokenValido(token);
    }

    @GetMapping
    public Usuario buscarInformacoesUser() throws Exception {
        String email = userDetail.me();
        return usuarioService.buscarUsuarioPorEmail(email).orElseThrow(()-> new UsernameNotFoundException("Usuário não está logado"));
    }


    @PostMapping("/resetPassword")
    public void ResetarSenha(@RequestBody ResetSenhaRequest resetRequest){
        passwordTokenService.resetarSenha(resetRequest.getToken(), resetRequest.getSenha());
    }


    @PostMapping("/sendEmail")
    public void enviarEmailParaResetarPassword(@RequestBody SenderEmailRequest resetRequest) throws Exception {
        Usuario user = usuarioService.buscarUsuarioPorEmail(resetRequest.getEmail()).orElseThrow(()->new Exception("Usuário não encontrado"));
        String token = UUID.randomUUID().toString();
        usuarioService.createPasswordResetTokenForUser(user, token);

        constructResetTokenEmail(appUrl, token, user);

    }

    @Autowired
    JavaMailSender mailSender;

    private void constructResetTokenEmail(
        String contextPath, String token, Usuario user)
        throws MessagingException {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        String url = contextPath + "/resetSenha?token=" + token;
        helper.setTo(user.getEmail());
        String subject = "Solicitação de alteração de palavra-chave";
        String bodyMessage = "<p>clique nesse <a href=\""+ url + "\">link</a> ("+ url +") para alterar senha</p>";

        helper.setSubject(subject);
        helper.setText(bodyMessage, true);
        mailSender.send(message);
    }

}
