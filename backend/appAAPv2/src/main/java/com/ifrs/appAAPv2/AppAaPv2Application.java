package com.ifrs.appAAPv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppAaPv2Application {

	public static void main(String[] args) {
		SpringApplication.run(AppAaPv2Application.class, args);
	}

}
