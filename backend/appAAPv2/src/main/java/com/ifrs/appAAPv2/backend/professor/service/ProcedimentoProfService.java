package com.ifrs.appAAPv2.backend.professor.service;

import com.ifrs.appAAPv2.backend.professor.repository.AjudaProfRepository;
import com.ifrs.appAAPv2.backend.professor.repository.ProcedimentoProfRepository;
import com.ifrs.appAAPv2.backend.professor.representation.mapper.ProcedimentoMapper;
import com.ifrs.appAAPv2.backend.professor.representation.request.ProcedimentoIndexRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.ProcedimentoRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.ProcedimentoResponse;
import com.ifrs.appAAPv2.backend.professor.validate.ValidateProcedimentos;
import com.ifrs.appAAPv2.domain.Ajuda;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.serviço.UserDetailImpl;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProcedimentoProfService {

	@Autowired
	AjudaProfRepository ajudaProfRepository;
	@Autowired
	QuestaoProfService questaoProfService;

	@Autowired
	ProcedimentoProfRepository procedimentoProfRepository;

	@Autowired
	UserDetailImpl userDetails;

	@Autowired
	UsuarioService usuarioService;

	ProcedimentoMapper procedimentoMapper = new ProcedimentoMapper();

	public void criarProcedimento(ProcedimentoRequest procedimentoRequest) throws Exception {
		ValidateProcedimentos.validarParametrosNull(procedimentoRequest);

		Questao questao = questaoProfService.buscarQuestaoPorId(procedimentoRequest.getQuestaoId());

		List<Ajuda> ajudas = ajudaProfRepository.findAllById(procedimentoRequest.getAjudasId());
		if(ajudas.size() != procedimentoRequest.getAjudasId().size()){
			throw new Exception("Uma ou mais ajudas não foram encontradas pelos id selecionado");
		}

		Procedimento procedimento = procedimentoMapper.toDomain(procedimentoRequest, ajudas, questao);
		procedimentoProfRepository.save(procedimento);
	}

	public void indexarProcedimentoNaAjuda(ProcedimentoIndexRequest procedimentoRequest) throws Exception {
		ValidateProcedimentos.validarParametrosNullIndex(procedimentoRequest);

		List<Ajuda> ajudas = ajudaProfRepository.findAllById(procedimentoRequest.getAjudasId());
		if(ajudas.size() == procedimentoRequest.getAjudasId().size()){
			throw new Exception("Uma ou mais ajudas não foram encontradas pelos id selecionado");
		}

		Procedimento procedimento = procedimentoProfRepository.findById(procedimentoRequest.getProcedimentoId())
			.orElseThrow(() -> new Exception("Não foi possível encontrar a ajuda com o id informado"));

		procedimento.getAjudas().addAll(ajudas);
		procedimentoProfRepository.save(procedimento);
	}

	public List<ProcedimentoResponse> buscarTodosProcedimentos() {
		List<Procedimento> procedimentos = procedimentoProfRepository.findAll();
		return procedimentos.stream()
			.map(procedimento -> procedimentoMapper.toResponse(procedimento))
			.collect(Collectors.toList());
	}

	public Set<ProcedimentoResponse> buscarMeusProcedimentosPorAjuda(String ajudaId, Integer questaoId, Integer turmaId) throws Exception {
		Usuario usuario = getUser();
		List<Procedimento> procedimentos = procedimentoProfRepository.buscarMeusProcedimentosPorAjuda(usuario.getId(), turmaId, questaoId, ajudaId);
		return procedimentos.stream()
			.map(questao -> procedimentoMapper.toResponse(questao))
			.collect(Collectors.toSet());
	}

	private Usuario getUser() throws Exception {
		String emailUsuario = userDetails.me();
		return usuarioService
			.buscarUsuarioPorEmail(emailUsuario)
			.orElseThrow(() -> new Exception("Usuário não encontrado"));
	}

	public Set<ProcedimentoResponse> buscarMeusProcedimentosPorQuestao(Integer questaoId) {
		List<Procedimento> procedimentos = procedimentoProfRepository.findAllByQuestaoId(questaoId);
		return procedimentos.stream()
			.map(procedimento -> procedimentoMapper.toResponse(procedimento))
			.collect(Collectors.toSet());
	}

	public Set<ProcedimentoResponse> buscarMeusProcedimentos() throws Exception {
		Usuario eu = getUser();
		List<Procedimento> procedimentos = procedimentoProfRepository.buscarMeusProcedimentos(eu.getId());
		Set<ProcedimentoResponse> procedimentoResponseSet = new HashSet<>();
		procedimentos.stream()
			.map(procedimento -> procedimentoMapper.toResponse(procedimento))
			.forEach(procedimentoResponseSet::add);

		return procedimentoResponseSet;
	}

	public void deletarProcedimento(Integer id) {
		Procedimento procedimento = new Procedimento();
		procedimento.setId(id);
		procedimentoProfRepository.save(procedimento);
		procedimentoProfRepository.deleteById(id);
	}

	public void atualizarProcedimento(ProcedimentoRequest procedimentoRequest, Integer procedimentoId) throws Exception {
		ValidateProcedimentos.validarParametrosNull(procedimentoRequest);

		Questao questao = questaoProfService.buscarQuestaoPorId(procedimentoRequest.getQuestaoId());

		List<Ajuda> ajudas = ajudaProfRepository.findAllById(procedimentoRequest.getAjudasId());
		if(ajudas.size()>0 && ajudas.size() != procedimentoRequest.getAjudasId().size()){
			throw new Exception("Uma ou mais ajudas não foram encontradas pelos id selecionado");
		}
		Procedimento procedimento = procedimentoProfRepository.findById(procedimentoId)
			.orElseThrow(() -> new Exception("Não existe procedimento com o ID informado"));

		procedimento.setId(procedimentoId);
		procedimento.setMensagem(procedimentoRequest.getMensagem());

		///Popular ajudas da questões
		if(ajudas.size()==0){
			List<Questao> novasQuestao = procedimento.getQuestoesId().stream()
				.filter(q -> !q.getId().equals(questao.getId())).collect(Collectors.toList());
			procedimento.setQuestoesId(novasQuestao);
		}else{
			List<Questao> novasQuestao = procedimento.getQuestoesId().stream()
				.filter(q -> q.getId().equals(questao.getId())).collect(Collectors.toList());
			if(novasQuestao.size() == 0){
				procedimento.setQuestoesId(novasQuestao);
				procedimento.getQuestoesId().add(questao);
			}else{
				procedimento.setQuestoesId(novasQuestao);
			}
		}

		procedimento.getAjudas().addAll(ajudas);

		procedimentoProfRepository.save(procedimento);
	}
}
