package com.ifrs.appAAPv2.backend.professor.repository.dto;

public interface LogAjudaDto {
	public Integer getId();
	public String getAjuda();
	public String getHora();
}
