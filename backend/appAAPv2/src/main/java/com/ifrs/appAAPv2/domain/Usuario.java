package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @NotEmpty(message = "O campo nome é obrigatório")
    String nome;

    @Email(message = "O email deve ser válido")
    @NotEmpty(message = "O campo email é obrigatório")
    String email;

    @NotEmpty(message = "O campo senha é obrigatório")
    String senha;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="rel_usuario_turma", joinColumns=
            {@JoinColumn(name="id_usuario")}, inverseJoinColumns=
            {@JoinColumn(name="id_turma")})
    List<Turma> turma;

}
