package com.ifrs.appAAPv2.backend.professor.service;

import com.ifrs.appAAPv2.backend.professor.repository.AjudaProfRepository;
import com.ifrs.appAAPv2.backend.professor.repository.ConceitoProfRepository;
import com.ifrs.appAAPv2.backend.professor.representation.mapper.AjudaMapper;
import com.ifrs.appAAPv2.backend.professor.representation.request.AjudaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.AjudaResponse;
import com.ifrs.appAAPv2.backend.professor.validate.AjudaValidate;
import com.ifrs.appAAPv2.domain.Ajuda;
import com.ifrs.appAAPv2.domain.Conceito;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.repositorio.QuestaoRepository;
import com.ifrs.appAAPv2.serviço.UserDetailImpl;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AjudaProfService {

	@Autowired
	QuestaoRepository questaoRepository;

	@Autowired
	AjudaProfRepository ajudaProfRepository;

	@Autowired
	UserDetailImpl userDetails;

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	ConceitoProfRepository conceitoProfRepository;

	AjudaMapper ajudaMapper = new AjudaMapper();

	public void criarAjuda(AjudaRequest ajudaRequest) throws Exception {
		AjudaValidate.validarParametrosNulo(ajudaRequest);

		List<Questao> questao = questaoRepository.findAllById(ajudaRequest.getQuestoes());
		Ajuda ajuda = ajudaMapper.toDomain(ajudaRequest, questao);

		Conceito conceitoEscolhido = conceitoProfRepository.findById(ajudaRequest.getIdConceito())
			.orElseThrow(() -> new Exception("Conceito não encontrado"));
		conceitoEscolhido.getAjudas().add(ajuda);

		long qtd_ajudas = ajudaProfRepository.contAjudasPorTipo(ajudaRequest.getId() + "%");

		if(qtd_ajudas==0) return;
		long numeroId = qtd_ajudas + 1;

		String ajudaId = ajudaRequest.getId() + numeroId;
		ajuda.setId(ajudaId);

		ajudaProfRepository.save(ajuda);
		conceitoProfRepository.save(conceitoEscolhido);
	}

//	public void indexarAjudaNaQuestao(AjudaIndexRequest ajudaRequest) throws Exception {
//		AjudaValidate.validarParametrosNuloIndex(ajudaRequest);
//
//		Usuario usuario = getUser();
//
//		Questao questao = questaoRepository.findById(ajudaRequest.getIdQuestao())
//			.orElseThrow(() -> new Exception("Id de turma não válido"));
//
//		boolean NaoTemATurmaDaQuestao = usuario.getTurma().stream().filter(turma ->
//			questao.getTurma().contains(turma)).collect(Collectors.toList()).isEmpty();
//
//		if(NaoTemATurmaDaQuestao){
//			throw new Exception("Não possui a turma da questão para poder cadastrar ajuda");
//		}
//
//		Ajuda ajuda = ajudaProfRepository.findById(ajudaRequest.getIdAjuda())
//			.orElseThrow(() -> new Exception("Não foi possível encontrar a ajuda com o id informado"));
//
//		ajuda.getQuestoes().add(questao);
//		ajudaProfRepository.save(ajuda);
//	}

	public Set<AjudaResponse> buscarAjudasPorQuestao(Integer questaoId) {
		List<Ajuda> ajudas = ajudaProfRepository.findByQuestaoId(questaoId);
		return  ajudas.stream()
			.map(ajuda -> ajudaMapper.toResponse(ajuda))
			.collect(Collectors.toSet());
	}

	public void atualizarAjuda(AjudaRequest ajudaRequest, String ajudaId) throws Exception {
		AjudaValidate.validarParametrosNulo(ajudaRequest);

		List<Questao> questao = questaoRepository.findAllById(ajudaRequest.getQuestoes());
		Ajuda ajuda = ajudaMapper.toDomain(ajudaRequest, questao);

		Conceito conceitoEscolhido = conceitoProfRepository.findById(ajudaRequest.getIdConceito())
			.orElseThrow(() -> new Exception("Conceito não encontrado"));
		conceitoEscolhido.getAjudas().add(ajuda);

		ajuda.setId(ajudaId);

		ajudaProfRepository.save(ajuda);
		conceitoProfRepository.save(conceitoEscolhido);
	}

	public void deletarAjuda(String id) throws Exception {
		ajudaProfRepository.findById(id)
			.orElseThrow(() -> new Exception("ajuda não encontrada pelo id: " + id));
		Ajuda ajudaVazia = new Ajuda();
		ajudaVazia.setId(id);
		ajudaProfRepository.save(ajudaVazia);
	}

	public Set<AjudaResponse> buscarMinhasAjudas() throws Exception {
		Usuario usuario = getUser();
		List<Ajuda> ajudas = ajudaProfRepository.findAllByUsarioId(usuario.getId());
		Set<AjudaResponse> retornoSet = new HashSet<>();
		ajudas.stream()
			.map(ajuda -> ajudaMapper.toResponse(ajuda))
			.forEach(retornoSet::add);
		return retornoSet;
	}

	private Usuario getUser() throws Exception {
		String emailUsuario = userDetails.me();
		return usuarioService
			.buscarUsuarioPorEmail(emailUsuario)
			.orElseThrow(() -> new Exception("Usuário não encontrado"));
	}
}
