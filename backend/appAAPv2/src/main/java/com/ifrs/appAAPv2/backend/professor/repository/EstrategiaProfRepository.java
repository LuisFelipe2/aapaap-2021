package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.domain.Estrategia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstrategiaProfRepository extends JpaRepository<Estrategia, Integer> {

	@Query(value = "select e.* from estrategia e " +
		"join rel_procedimento_estrategia rpe " +
		"on rpe.id_estrategia = e.id " +
		"join procedimento p " +
		"on p.id = rpe.id_procedimento " +
		"join rel_procedimento_ajuda rpa " +
		"ON rpa.id_procedimento = p.id " +
		"join ajuda a " +
		"on rpa.id_ajuda = a.id " +
		"join rel_questao_ajuda rqa " +
		"on rqa.id_ajuda = a.id " +
		"join questao q " +
		"on q.id = rqa.id_questao " +
		"join rel_questao_turma rqt " +
		"on rqt.id_questao = q.id " +
		"join turma t " +
		"on t.id = rqt.id_turma " +
		"join rel_usuario_turma rut " +
		"on rut.id_turma = t.id " +
		"join usuario u " +
		"on u.id = rut.id_usuario " +
		"where u.email = ?1", nativeQuery = true)
	List<Estrategia> findAllMyStrategys(String email);
}
