package com.ifrs.appAAPv2.backend.professor.validate;

import com.ifrs.appAAPv2.backend.professor.representation.request.QuestaoRequest;

import static java.util.Objects.isNull;

public class QuestaoValidate {

	public static void validarParametrosNulo(QuestaoRequest questaoRequest) throws Exception {
		if(isNull(questaoRequest.getIdTurma()) ||
			isNull(questaoRequest.getResposta()) ||
			isNull(questaoRequest.getTexto()) ||
			isNull(questaoRequest.getTitulo())) {
			throw new Exception("nenhum parâmetro deve ser nulo");
		}
	}
}
