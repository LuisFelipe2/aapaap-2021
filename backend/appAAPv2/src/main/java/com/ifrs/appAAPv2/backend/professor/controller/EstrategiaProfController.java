package com.ifrs.appAAPv2.backend.professor.controller;

import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaIndexRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.EstrategiaResponse;
import com.ifrs.appAAPv2.backend.professor.service.EstrategiaProfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/privado/professor/estrategia")
public class EstrategiaProfController {

	@Autowired
	EstrategiaProfService estrategiaProfService;

	@PostMapping
	public void criarEstrategia(@RequestBody EstrategiaRequest estrategiaRequest) throws Exception {
		estrategiaProfService.criarEstrategia(estrategiaRequest);
	}

	@PostMapping("/{estrategiaId}")
	public void atualizarEstrategia(@RequestBody EstrategiaRequest estrategiaRequest, @PathVariable Integer estrategiaId) throws Exception {
		estrategiaProfService.atualizarEstrategia(estrategiaRequest, estrategiaId);
	}

	@GetMapping("/me")
	public Set<EstrategiaResponse> buscarMinhasEstrategias() throws Exception {
		return estrategiaProfService.buscarMinhaEstrategias();
	}

	@DeleteMapping("/{id}")
	public void deleteEstrategia(@PathVariable Integer id){
		estrategiaProfService.deletarEstrategia(id);
	}

}
