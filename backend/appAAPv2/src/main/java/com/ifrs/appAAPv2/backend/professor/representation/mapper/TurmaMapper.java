package com.ifrs.appAAPv2.backend.professor.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.representation.request.TurmaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.TurmaResponse;
import com.ifrs.appAAPv2.domain.Turma;

public class TurmaMapper {
	public Turma toDomain(TurmaRequest turmaRequest) {
		Turma turma = new Turma();
		turma.setNome(turmaRequest.getNome());
		return turma;
	}

	public TurmaResponse toResponse(Turma turma) {
		TurmaResponse response = new TurmaResponse();
		response.setNome(turma.getNome());
		response.setId(turma.getId());
		return response;
	}
}
