package com.ifrs.appAAPv2.backend.professor.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AjudaIndexRequest {
	String idAjuda;
	Integer idQuestao;
}
