package com.ifrs.appAAPv2.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRequest {

    @NotEmpty(message = "O campo nome é obrigatório")
    String nome;

    @Email(message = "O email deve ser válido")
    @NotEmpty(message = "O campo email é obrigatório")
    String email;

    @NotEmpty(message = "O campo senha é obrigatório")
    String senha;

    Integer idTurma;
}
