package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.domain.Questao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestaoProfRepository extends JpaRepository<Questao, Integer> {
	@Query(value = "select q.* from questao q " +
		"join rel_questao_turma rqt " +
		"on rqt.id_questao = q.id " +
		"join turma t " +
		"on t.id = rqt.id_turma " +
		"join rel_usuario_turma r " +
		"on r.id_turma = t.id " +
		"where r.id_usuario = ?1", nativeQuery = true)
	List<Questao> buscarMinhasQuestoes(Integer id);

	@Query(value = "select q.* from questao q " +
		"join rel_questao_turma rqt " +
		"on rqt.id_questao = q.id " +
		"join turma t " +
		"on t.id = rqt.id_turma " +
		"join rel_usuario_turma r " +
		"on r.id_turma = t.id " +
		"where r.id_usuario = ?1 " +
		"and t.id = ?2", nativeQuery = true)
	List<Questao> buscarQuestoesPorTurma(Integer idUsuario,Integer idTurma);
}
