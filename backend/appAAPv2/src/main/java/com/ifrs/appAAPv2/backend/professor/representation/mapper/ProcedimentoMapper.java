package com.ifrs.appAAPv2.backend.professor.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.representation.request.ProcedimentoRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.ProcedimentoResponse;
import com.ifrs.appAAPv2.domain.Ajuda;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.domain.Questao;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ProcedimentoMapper {

	public Procedimento toDomain(ProcedimentoRequest procedimentoRequest, List<Ajuda> ajudas, Questao questao) {
		Procedimento procedimento = new Procedimento();
		procedimento.setMensagem(procedimentoRequest.getMensagem());
		procedimento.setAjudas(ajudas);
		procedimento.setQuestoesId(Collections.singletonList(questao));
		return procedimento;
	}

	public ProcedimentoResponse toResponse(Procedimento procedimento) {
		ProcedimentoResponse response = new ProcedimentoResponse();
		response.setMensagem(procedimento.getMensagem());
		response.setId(procedimento.getId());
		response.setIsValid(procedimento.getValido());
		response.setAjudasId(procedimento.getAjudas().stream()
			.map(Ajuda::getId)
			.collect(Collectors.toList()));
		response.setQuestoesId(procedimento.getQuestoesId().stream()
			.map(Questao::getId)
			.collect(Collectors.toList()));
		return response;
	}
}