package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode
public class Questao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String titulo;
    String texto;
    String resposta;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "rel_questao_turma", joinColumns =
            {@JoinColumn(name="id_questao")}, inverseJoinColumns=
            {@JoinColumn(name="id_turma")})
    List<Turma> turma;

}
