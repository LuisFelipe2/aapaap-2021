package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Error;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ErrorRepository extends JpaRepository<Error, Integer> {
}
