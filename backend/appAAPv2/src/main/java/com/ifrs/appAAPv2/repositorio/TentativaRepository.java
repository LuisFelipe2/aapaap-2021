package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Tentativa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TentativaRepository extends JpaRepository<Tentativa, Integer> {
    @Query(value = "select * from tentativa where id_usuario=?2 and id_questao=?1 and resposta=null", nativeQuery = true)
    Optional<Tentativa> findByIdQuestionAndIdUser(Integer id_questio, Integer id_user);

    @Query(value = "select * from tentativa where id_usuario=?2 and id_questao=?1", nativeQuery = true)
    Optional<Tentativa> existsPorQuestaoIdUserId(Integer id_questio, Integer id_user);
}
