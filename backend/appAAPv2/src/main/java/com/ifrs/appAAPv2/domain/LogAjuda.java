package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class LogAjuda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer Id;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="id_ajuda")
    Ajuda idAjuda;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="id_tentativa")
    Tentativa idTentativa;

    LocalDateTime hora;
}
