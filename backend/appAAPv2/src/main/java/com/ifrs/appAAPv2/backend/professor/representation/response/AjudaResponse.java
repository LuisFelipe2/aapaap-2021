package com.ifrs.appAAPv2.backend.professor.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AjudaResponse {
	String id;
	String texto;
	String titulo;
	List<Integer> questoesId;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AjudaResponse that = (AjudaResponse) o;
		return Objects.equals(id, that.id) && Objects.equals(texto, that.texto) && Objects.equals(titulo, that.titulo) && Objects.equals(questoesId, that.questoesId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, texto, titulo, questoesId);
	}
}
