package com.ifrs.appAAPv2.representation.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogRequest {
    String idAjuda;
    Integer idTentativa;
}
