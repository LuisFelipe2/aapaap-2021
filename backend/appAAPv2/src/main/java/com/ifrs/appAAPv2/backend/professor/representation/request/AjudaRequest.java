package com.ifrs.appAAPv2.backend.professor.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AjudaRequest {
	String id;
	String texto;
	String titulo;
	List<Integer> questoes;
	Integer idConceito;
}
