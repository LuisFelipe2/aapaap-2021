package com.ifrs.appAAPv2.backend.professor.repository;

import com.ifrs.appAAPv2.domain.Conceito;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConceitoProfRepository extends JpaRepository<Conceito, Integer> {
}
