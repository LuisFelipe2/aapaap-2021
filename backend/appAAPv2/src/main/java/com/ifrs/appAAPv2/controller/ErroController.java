package com.ifrs.appAAPv2.controller;

import com.ifrs.appAAPv2.domain.Error;
import com.ifrs.appAAPv2.representation.request.ErroRequest;
import com.ifrs.appAAPv2.serviço.ErroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/privado/erro")
public class ErroController {

    @Autowired
    ErroService erroService;

    @PostMapping
    public Error salvarErro(@RequestBody ErroRequest erroRequest) throws Exception {
        return erroService.salvarErro(erroRequest);
    }
}
