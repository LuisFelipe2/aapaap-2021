package com.ifrs.appAAPv2.backend.professor.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EstrategiaResponse {
	String texto;
	Integer id;
	Integer questaoId;
	List<Integer> procedimentoIds;
	Integer grauComplexidade;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EstrategiaResponse response = (EstrategiaResponse) o;
		return Objects.equals(texto, response.texto) && Objects.equals(id, response.id) && Objects.equals(questaoId, response.questaoId) && Objects.equals(procedimentoIds, response.procedimentoIds);
	}

	@Override
	public int hashCode() {
		return Objects.hash(texto, id, questaoId, procedimentoIds);
	}
}
