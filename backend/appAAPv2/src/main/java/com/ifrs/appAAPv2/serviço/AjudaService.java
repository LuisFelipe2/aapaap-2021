package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.*;
import com.ifrs.appAAPv2.repositorio.AjudaLogRepository;
import com.ifrs.appAAPv2.repositorio.AjudaRepository;
import com.ifrs.appAAPv2.repositorio.ConceitoRepository;
import com.ifrs.appAAPv2.representation.request.LogRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AjudaService {
    @Autowired
    AjudaRepository ajudaRepository;

    @Autowired
    TentativaService tentativaService;
    @Autowired
    UserDetailImpl userDetail;
    @Autowired
    UsuarioService usuarioService;
    @Autowired
    AjudaLogRepository ajudaLogRepository;
    @Autowired
    ConceitoRepository conceitoRepository;


    public List<Ajuda> buscarTodasAjudasPorId(List<String> ajudas) {
        return ajudaRepository.findAllById(ajudas);
    }

    public List<Conceito> buscarTodasAjudasPorIdQuestao(Integer id) {
        List<Ajuda> ajudasConceitoUm = ajudaRepository.findAllByQuestaoIdConceitoUm(id);
        List<Ajuda> ajudasConceitoDois = ajudaRepository.findAllByQuestaoIdConceitoDois(id);
        List<Ajuda> ajudasConceitoTres = ajudaRepository.findAllByQuestaoIdConceitoTres(id);
        List<Ajuda> ajudasConceitoQuatro = ajudaRepository.findAllByQuestaoIdConceitoQuatro(id);

        List<Conceito> conceitos = new ArrayList<>();
        conceitos.add(new Conceito(1, "Expressões e fórmulas associadas a funções", ajudasConceitoUm));
        conceitos.add(new Conceito(2, "Gráfico de uma função", ajudasConceitoDois));
        conceitos.add(new Conceito(3, "Relações trigonométricas e conceitos geométricos", ajudasConceitoTres));
        conceitos.add(new Conceito(4, "Regra de três, proporcionalidade, frações e sistemas lineares", ajudasConceitoQuatro));

        return conceitos;
    }
    public void salvarLogAjuda(LogRequest logRequest) throws Exception {
        Optional<Tentativa> tentativa = tentativaService.buscarPorId(logRequest.getIdTentativa());
        Ajuda ajuda = ajudaRepository.getById(logRequest.getIdAjuda());
        if(tentativa.isPresent()){
            LogAjuda logAjuda = new LogAjuda();
            logAjuda.setIdAjuda(ajuda);
            logAjuda.setIdTentativa(tentativa.get());
            logAjuda.setHora(LocalDateTime.now());

            ajudaLogRepository.save(logAjuda);
        }
    }

    public List<LogAjuda> buscarLogAjudas(Integer idTentativa) {
        return ajudaLogRepository.findAllByIdTentativa(idTentativa);
    }
}
