package com.ifrs.appAAPv2.backend.professor.representation.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TurmaResponse {
	Integer id;
	String nome;
	List<String> emails;
}
