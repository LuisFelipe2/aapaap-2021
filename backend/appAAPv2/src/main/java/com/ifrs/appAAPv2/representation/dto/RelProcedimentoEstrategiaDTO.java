package com.ifrs.appAAPv2.representation.dto;


public interface RelProcedimentoEstrategiaDTO {
    Integer getId();
    Integer getCnt();
}