package com.ifrs.appAAPv2.backend.professor.controller;

import com.ifrs.appAAPv2.backend.professor.representation.response.AlunoResponse;
import com.ifrs.appAAPv2.backend.professor.service.AlunoProfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/privado/professor/aluno")
public class AlunoProfController {

	@Autowired
	AlunoProfService alunoProfService;

	@GetMapping
	public List<AlunoResponse> buscarMeusAlunos() throws Exception {
		return alunoProfService.buscarMeusAlunos();
	}
}
