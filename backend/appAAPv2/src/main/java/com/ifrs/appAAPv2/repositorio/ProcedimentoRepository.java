package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Ajuda;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.representation.dto.RelProcedimentoEstrategiaDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface ProcedimentoRepository extends JpaRepository<Procedimento, Integer> {
    @Query(value = "select procedimento.* from procedimento \n" +
            "join rel_procedimento_ajuda on procedimento.id = rel_procedimento_ajuda.id_procedimento \n" +
            "join ajuda on ajuda.id = rel_procedimento_ajuda.id_ajuda \n" +
            "join rel_questao_ajuda on rel_questao_ajuda.id_ajuda = ajuda.id\n" +
            "join questao on questao.id = rel_questao_ajuda.id_questao\n" +
            "join rel_questao_procedimento rqp on rqp.id_procedimento = procedimento.id\n"+
            "where ajuda.id in ?1 and questao.id=?2 and rqp.id_questao=?2", nativeQuery = true)
    List<Procedimento> buscarProcedimentosPorAjuda(List<Ajuda> ajudas, Integer idQuestao);

    @Query(value = "select rel_procedimento_estrategia.id_procedimento as id, count(*) as cnt from procedimento\n" +
            "join rel_procedimento_estrategia on procedimento.id = rel_procedimento_estrategia.id_procedimento\n" +
            "join estrategia on estrategia.id = rel_procedimento_estrategia.id_estrategia \n" +
            "where estrategia.id_questao = ?1\n" +
            "group by id_procedimento\n" +
            "order by id", nativeQuery = true)
    List<RelProcedimentoEstrategiaDTO> buscarProcedimentosPadroes(Integer idQuestao);
}
