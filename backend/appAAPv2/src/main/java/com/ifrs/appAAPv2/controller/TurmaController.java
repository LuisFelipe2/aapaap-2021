package com.ifrs.appAAPv2.controller;

import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.serviço.TurmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/publico/turma")
public class TurmaController {
    @Autowired
    TurmaService turmaService;
    @GetMapping
    public List<Turma> buscarTurmas(){
        return turmaService.buscarTodasAsTurmas();
    }
}
