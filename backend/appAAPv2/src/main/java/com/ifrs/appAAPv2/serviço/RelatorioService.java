package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.*;
import com.ifrs.appAAPv2.repositorio.RelatorioRepository;
import com.ifrs.appAAPv2.representation.dto.RelProcedimentoEstrategiaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class RelatorioService {

    @Autowired
    RelatorioRepository relatorioRepository;

    @Autowired
    AjudaService ajudaService;
    @Autowired
    TentativaService tentativaService;

    @Autowired
    ProcedimentoService procedimentosService;

    @Autowired
    EstrategiaService estrategiaService;

    public List<Estrategia> buscarEstrategia(List<Integer> procedimentosUsados, Integer idTentativa) throws Exception {

        Optional<Relatorio> relatorioExistente = relatorioRepository.findByIdTentativa(idTentativa);

        if(relatorioExistente.isPresent()){
            return relatorioExistente.get().getEstrategia();
        }

        Tentativa tentativa = tentativaService.buscarPorId(idTentativa).orElseThrow(() -> new Exception("Você não respondeu essa questão"));
        List<Procedimento> procedimentos = procedimentosService.buscarTodosProcedimentosPorId(procedimentosUsados).stream().distinct().collect(Collectors.toList());
        List<Estrategia> estrategiaResponse = estrategiaService.buscarTodasEstrategiasByIdProcedimentos(procedimentosUsados, tentativa.getIdQuestao().getId());

        Optional<Estrategia> estrategiaFinal = estrategiaResponse.stream()
                .sorted((a, b) -> {
                    if(nonNull(a.getGrauComplexidade()) && nonNull(b.getGrauComplexidade())){
                        return a.getGrauComplexidade().compareTo(b.getGrauComplexidade());
                    }else{
                        if(isNull(a.getGrauComplexidade())) return -1;
                        else return 1;
                    }
                }).findFirst();

        if(!estrategiaFinal.isPresent()){
            return null;
        }

        List<Estrategia> estrategias = new ArrayList<>();
        estrategias.add(estrategiaFinal.get());
        if(estrategiaResponse.stream().filter(estrategia -> estrategia.getId() == 5).count() != 0 && estrategiaFinal.get().getId()!=5){
            estrategias.addAll(estrategiaResponse.stream().filter(estrategia -> estrategia.getId()==5).collect(Collectors.toList()));
        }

        Relatorio relatorio = new Relatorio();
        relatorio.setEstrategia(estrategias);
        relatorio.setTentativa(tentativa);
        relatorio.setProcedimento(procedimentos);
        relatorioRepository.save(relatorio);

        return estrategias;
    }

    public List<Procedimento> buscarProcedimentos(Integer idTentativa) throws Exception {

        Optional<Relatorio> relatorioExistente = relatorioRepository.findByIdTentativa(idTentativa);

        if(relatorioExistente.isPresent()){
            return null;
        }

        Tentativa tentativa = tentativaService.buscarPorId(idTentativa).orElseThrow(() -> new Exception("Tentativa não existe"));
        List<LogAjuda> ajudas = ajudaService.buscarLogAjudas(tentativa.getId());

        if (ajudas.size() != 0) {
            List<LogAjuda> logAjudasReais = new ArrayList<>();
            for (int x = 0; x < ajudas.size(); x++) {

                if (x == ajudas.size() -1) {
                    logAjudasReais.add(ajudas.get(x));
                    break;
                }

                boolean ehDepois = ajudas.get(x).getHora().plusSeconds(5).isBefore(ajudas.get(x + 1).getHora());

                if (ehDepois) {
                    logAjudasReais.add(ajudas.get(x));
                }
            }
//        List<Ajuda> ajudas = ajudaService.buscarTodasAjudasPorId(relatorioRequest.getAjudas());
            List<Procedimento> responseProcedimento = procedimentosService.buscarTodosProcedimentosByIdAjudas(logAjudasReais.stream().map(LogAjuda::getIdAjuda).collect(Collectors.toList()), tentativa.getIdQuestao());

            List<Procedimento> procedimentosUsados = responseProcedimento.stream().filter(procedimento -> {
                long vezesQueProcedimentoRepete = responseProcedimento.stream().filter(procedimento1 -> procedimento1.getId().equals(procedimento.getId())).count();
                return vezesQueProcedimentoRepete > 1;
            }).distinct().collect(Collectors.toList());

            if (procedimentosUsados.size() == 0)
                return responseProcedimento;

            return procedimentosUsados;
        } else {
            List<RelProcedimentoEstrategiaDTO> rel = procedimentosService.buscarProcedimentosPadroes(tentativa.getIdQuestao().getId());

            List<Integer> procedimentosUnicos;
            int x = 0;
            do{
                int finalX = x;
                procedimentosUnicos = rel.stream()
                    .filter(r -> r.getCnt() == finalX)
                    .map(RelProcedimentoEstrategiaDTO::getId).collect(Collectors.toList());
                x++;
            }while(procedimentosUnicos.size() == 0);

            return procedimentosService.buscarTodosProcedimentosPorId(procedimentosUnicos);
        }
    }

//    public void criarRelatorio(Integer id_questio, Integer id_user) {
//        Optional<Tentativa> tentativa = tentativaService.buscarTentativaPorQuestaoIdUserId(id_questio, id_user);
//        if(tentativa.isPresent()) {
//            Relatorio relatorioNovo = new Relatorio();
//            relatorioNovo.setTentativa(tentativa.get());
//            relatorioNovo.setEstrategia();
//            relatorioRepository.save(relatorioNovo);
//        }
//    }


    public Optional<Relatorio> buscarRelatorioPorIdTentativa(Integer idTentativa) {
        return relatorioRepository.findByIdTentativa(idTentativa);
    }

    public void atualizarGrau(Integer idTentativa, Integer grau) throws Exception {
        Relatorio relatorio = buscarRelatorioPorIdTentativa(idTentativa).orElseThrow(()-> new Exception("Exceção inesperada"));
        relatorio.setGrauDeSimilaridade(grau);
        relatorioRepository.save(relatorio);
    }
}
