package com.ifrs.appAAPv2.controller;

import com.ifrs.appAAPv2.domain.Estrategia;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.serviço.RelatorioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/privado/relatorio")
public class RelatorioController {

    @Autowired
    RelatorioService relatorioService;

    @PostMapping("/estrategia/{idTentativa}")
    public List<Estrategia> SalvarRelatorio(@PathVariable Integer idTentativa, @RequestBody List<Integer> procedimentos) throws Exception {
        return relatorioService.buscarEstrategia(procedimentos, idTentativa);
    }

    @PostMapping("/procedimentos/{idTentativa}")
    public List<Procedimento> BuscarProcedimentosPorAjuda(@PathVariable Integer idTentativa) throws Exception {
        return relatorioService.buscarProcedimentos(idTentativa);
    }

    @PostMapping("/{idTentativa}/{grau}")
    public void SalvarGrauDeSimilaridade(@PathVariable Integer idTentativa, @PathVariable Integer grau) throws Exception {
        relatorioService.atualizarGrau(idTentativa,grau);
    }
}
