package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.PasswordResetToken;
import com.ifrs.appAAPv2.repositorio.PasswordTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class PasswordTokenService {

	@Autowired
	PasswordTokenRepository passwordTokenRepository;
	@Autowired
	UsuarioService usuarioService;

	public void resetarSenha(String token, String senha) {
		Optional<PasswordResetToken> resetPassword = passwordTokenRepository.findByToken(token);
		if (resetPassword.isPresent()){
			if(resetPassword.get().getExpiryDate().isAfter(LocalDateTime.now())){
				usuarioService.resetarSenha(resetPassword.get().getUser(), senha);
			}
		}
	}

}
