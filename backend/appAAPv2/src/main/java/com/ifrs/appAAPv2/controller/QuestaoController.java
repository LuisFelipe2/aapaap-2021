package com.ifrs.appAAPv2.controller;


import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.representation.response.QuestaoCardResponse;
import com.ifrs.appAAPv2.serviço.QuestaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.rmi.UnexpectedException;
import java.util.List;

@RestController
@RequestMapping("/privado/questao")
public class QuestaoController {

    @Autowired
    QuestaoService questaoService;

    @GetMapping
    public List<QuestaoCardResponse> buscarCardsDeQuestoes() throws Exception {
        return questaoService.buscarCardsDeQuestoes();
    }

    @GetMapping("/{idQuestao}")
    public Questao buscarQuestao(@PathVariable Integer idQuestao) throws UnexpectedException {
        return questaoService.buscarQuestao(idQuestao);
    }
}
