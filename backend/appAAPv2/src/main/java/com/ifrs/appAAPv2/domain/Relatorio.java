package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Relatorio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    Integer grauDeSimilaridade;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "id_tentativa")
    Tentativa tentativa;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="rel_estrategia_relatorio", joinColumns=
            {@JoinColumn(name="id_relatorio")}, inverseJoinColumns=
            {@JoinColumn(name="id_estrategia")})
    List<Estrategia> estrategia;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="rel_procedimentos_relatorio", joinColumns=
            {@JoinColumn(name="id_relatorio")}, inverseJoinColumns=
            {@JoinColumn(name="id_procedimento")})
    List<Procedimento> procedimento;

}
