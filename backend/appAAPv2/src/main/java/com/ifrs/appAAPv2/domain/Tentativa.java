package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Tentativa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String resposta;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="id_questao")
    Questao idQuestao;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="id_usuario")
    Usuario idUsuario;

}
