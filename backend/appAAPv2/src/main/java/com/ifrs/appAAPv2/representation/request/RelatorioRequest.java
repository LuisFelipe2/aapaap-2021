package com.ifrs.appAAPv2.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RelatorioRequest {

    Integer questaoId;
    List<String> ajudas;
}
