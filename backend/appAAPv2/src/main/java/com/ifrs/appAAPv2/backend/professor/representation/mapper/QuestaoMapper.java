package com.ifrs.appAAPv2.backend.professor.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.representation.request.QuestaoRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.QuestaoResponse;
import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Turma;

import javax.management.Query;
import java.util.Collections;

public class QuestaoMapper {
	public Questao toDomain(QuestaoRequest questaoRequest, Turma turma) {
		Questao questao = new Questao();
		questao.setResposta(questaoRequest.getResposta());
		questao.setTexto(questaoRequest.getTexto());
		questao.setTitulo(questaoRequest.getTitulo());
		if(questao.getTurma() != null)
			questao.getTurma().add(turma);
		else
			questao.setTurma(Collections.singletonList(turma));

		return questao;
	}

	public QuestaoResponse toResponse(Questao questao) {
		QuestaoResponse response = new QuestaoResponse();
		response.setEnunciado(questao.getTexto());
		response.setResposta(questao.getResposta());
		response.setId(questao.getId());
		response.setTitulo(questao.getTitulo());
		return response;
	}
}
