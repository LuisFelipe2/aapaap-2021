package com.ifrs.appAAPv2.backend.professor.service;

import com.ifrs.appAAPv2.backend.professor.repository.AlunoProfRepository;
import com.ifrs.appAAPv2.backend.professor.repository.dto.AlunoDto;
import com.ifrs.appAAPv2.backend.professor.representation.mapper.UsuarioMapper;
import com.ifrs.appAAPv2.backend.professor.representation.response.AlunoResponse;
import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.serviço.UserDetailImpl;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AlunoProfService {

	@Autowired
	AlunoProfRepository alunoProfRepository;

	@Autowired
	UserDetailImpl userDetails;

	@Autowired
	UsuarioService usuarioService;

	private final UsuarioMapper usuarioMapper = new UsuarioMapper();

	public List<AlunoResponse> buscarMeusAlunos() throws Exception {
		Usuario eu = getUser();
		List<String> minhasTurmas = eu.getTurma().stream()
			.map(Turma::getNome)
			.collect(Collectors.toList());

		List<AlunoDto> usuarios = alunoProfRepository.findAllMyStudents(minhasTurmas);

		return usuarios.stream()
			.filter(usuario -> !usuario.getEmail().equals(eu.getEmail()))
			.map(usuarioMapper::toResponse)
			.collect(Collectors.toList());
	}

	public List<Usuario> buscarAlunosPorTurma(Integer idTurma){
		return alunoProfRepository.buscarAlunosPorTurma(idTurma);
	}


	private Usuario getUser() throws Exception {
		String emailUsuario = userDetails.me();
		return usuarioService
			.buscarUsuarioPorEmail(emailUsuario)
			.orElseThrow(() -> new Exception("Usuário não encontrado"));
	}

	public void atualizarTodos(List<Usuario> alunos) {
		alunoProfRepository.saveAll(alunos);
	}
}
