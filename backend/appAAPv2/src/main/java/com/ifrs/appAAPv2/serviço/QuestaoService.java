package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.repositorio.QuestaoRepository;
import com.ifrs.appAAPv2.representation.mapper.QuestaoMapper;
import com.ifrs.appAAPv2.representation.response.QuestaoCardResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestaoService {

    @Autowired
    QuestaoRepository questaoRepository;
    @Autowired
    UserDetailImpl userDetails;
    @Autowired
    UsuarioService usuarioService;


    QuestaoMapper questaoMapper = new QuestaoMapper();

    public List<QuestaoCardResponse> buscarCardsDeQuestoes() throws Exception {
        Usuario eu = getMe();
        List<Integer> minhasTurmas = eu.getTurma().stream()
            .map(Turma::getId)
            .collect(Collectors.toList());

        List<Questao> questoes = questaoRepository.findAllMyQuestionByTurma(minhasTurmas);

        if(questoes.size() == 0){
            questoes = questaoRepository.findFirstFour();
        }

        return questoes.stream().map(questao -> questaoMapper.toResponseList(questao)).collect(Collectors.toList());
    }

    public Questao buscarQuestao(Integer idQuestao/*, Integer idUsuario*/) throws UnexpectedException {
        return questaoRepository.findById(idQuestao).orElseThrow(() -> new UnexpectedException("Essa foi inesperada"));
    }

    private Usuario getMe() throws Exception {
        String emailUsuario = userDetails.me();
        return usuarioService
            .buscarUsuarioPorEmail(emailUsuario)
            .orElseThrow(() -> new Exception("Usuário não encontrado"));
    }
}
