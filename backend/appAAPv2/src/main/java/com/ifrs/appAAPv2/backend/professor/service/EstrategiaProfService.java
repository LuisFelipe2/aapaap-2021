package com.ifrs.appAAPv2.backend.professor.service;

import com.ifrs.appAAPv2.backend.professor.repository.AjudaProfRepository;
import com.ifrs.appAAPv2.backend.professor.repository.EstrategiaProfRepository;
import com.ifrs.appAAPv2.backend.professor.repository.ProcedimentoProfRepository;
import com.ifrs.appAAPv2.backend.professor.repository.QuestaoProfRepository;
import com.ifrs.appAAPv2.backend.professor.representation.mapper.EstrategiaMapper;
import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaIndexRequest;
import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.EstrategiaResponse;
import com.ifrs.appAAPv2.backend.professor.validate.ValidateEstrategia;
import com.ifrs.appAAPv2.domain.*;
import com.ifrs.appAAPv2.serviço.UserDetailImpl;
import com.ifrs.appAAPv2.serviço.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EstrategiaProfService {

	@Autowired
	AjudaProfRepository ajudaProfRepository;

	@Autowired
	ProcedimentoProfRepository procedimentoProfRepository;

	@Autowired
	UserDetailImpl userDetails;

	@Autowired
	UsuarioService usuarioService;

	EstrategiaMapper estrategiaMapper = new EstrategiaMapper();

	@Autowired
	QuestaoProfRepository questaoProfRepository;

	@Autowired
	EstrategiaProfRepository estrategiaProfRepository;

	public void criarEstrategia(EstrategiaRequest estrategiaRequest) throws Exception {
		ValidateEstrategia.validarParametrosNull(estrategiaRequest);

		Usuario usuario = getMe();

		Questao questao = questaoProfRepository.findById(estrategiaRequest.getIdQuestao())
			.orElseThrow(() -> new Exception("Id de questão não válido"));

		boolean NaoTemATurmaDaQuestao = usuario.getTurma().stream().filter(turma ->
			questao.getTurma().contains(turma)).count() == 0;

		if(NaoTemATurmaDaQuestao){
			throw new Exception("Não possui a turma da questão para poder cadastrar ajuda");
		}

		List<Procedimento> procedimentos = procedimentoProfRepository.findAllById(estrategiaRequest.getProcedimentosId());
		if(procedimentos.size() != estrategiaRequest.getProcedimentosId().size()){
			throw new Exception("Um ou mais procedimentos não foram encontradas pelos ids selecionados");
		}

		Estrategia estrategia = estrategiaMapper.toDomain(estrategiaRequest, procedimentos, questao);
		estrategiaProfRepository.save(estrategia);
	}

	public void indexarEstrategiaNoProcedimento(EstrategiaIndexRequest estrategiaIndexRequest) throws Exception {
		ValidateEstrategia.validarParametrosNullIndex(estrategiaIndexRequest);

		List<Procedimento> procedimentos = procedimentoProfRepository.findAllById(estrategiaIndexRequest.getProcedimentosId());
		if(procedimentos.size() == estrategiaIndexRequest.getProcedimentosId().size()){
			throw new Exception("Um ou mais procedimentos não foram encontradas pelos ids selecionados");
		}

		Estrategia estrategia = estrategiaProfRepository.findById(estrategiaIndexRequest.getIdEstrategia())
			.orElseThrow(() -> new Exception("Não foi possível encontrar a estratégia com o id informado"));

		estrategia.getProcedimentos().addAll(procedimentos);
		estrategiaProfRepository.save(estrategia);
	}

	public Set<EstrategiaResponse> buscarMinhaEstrategias() throws Exception {
		Usuario eu = getMe();

		List<Estrategia> estrategias = estrategiaProfRepository.findAllMyStrategys(eu.getEmail());
		Set<EstrategiaResponse> estrategiasFiltradas = new HashSet<>();

		return estrategias.stream()
			.map(estrategia -> estrategiaMapper.toResponse(estrategia))
			.filter(estrategiasFiltradas::add)
			.collect(Collectors.toSet());
	}

	private Usuario getMe() throws Exception {
		String emailUsuario = userDetails.me();
		return usuarioService
			.buscarUsuarioPorEmail(emailUsuario)
			.orElseThrow(() -> new Exception("Usuário não encontrado"));
	}

	public void atualizarEstrategia(EstrategiaRequest estrategiaRequest, Integer estrategiaId) throws Exception {
		ValidateEstrategia.validarParametrosNull(estrategiaRequest);

		Usuario usuario = getMe();

		Questao questao = questaoProfRepository.findById(estrategiaRequest.getIdQuestao())
			.orElseThrow(() -> new Exception("Id de questão não válido"));

		boolean NaoTemATurmaDaQuestao = usuario.getTurma().stream().filter(turma ->
			questao.getTurma().contains(turma)).count() == 0;

		if(NaoTemATurmaDaQuestao){
			throw new Exception("Não possui a turma da questão para poder cadastrar ajuda");
		}

		List<Procedimento> procedimentos = procedimentoProfRepository.findAllById(estrategiaRequest.getProcedimentosId());
		if(procedimentos.size() != estrategiaRequest.getProcedimentosId().size()){
			throw new Exception("Um ou mais procedimentos não foram encontradas pelos ids selecionados");
		}

		Estrategia estrategia = estrategiaMapper.toDomain(estrategiaRequest, procedimentos, questao);
		estrategia.setId(estrategiaId);
		estrategiaProfRepository.save(estrategia);
	}

	public void deletarEstrategia(Integer id) {
		estrategiaProfRepository.deleteById(id);
	}
}
