package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.LogAjuda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AjudaLogRepository extends JpaRepository<LogAjuda, Integer> {
    @Query(value = "select * from log_ajuda where id_tentativa = ?1", nativeQuery = true)
    List<LogAjuda> findAllByIdTentativa(Integer idTentativa);
}
