package com.ifrs.appAAPv2.backend.professor.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.repository.dto.AlunoDto;
import com.ifrs.appAAPv2.backend.professor.representation.response.AlunoResponse;
import com.ifrs.appAAPv2.domain.Usuario;

public class UsuarioMapper {
	public AlunoResponse toResponse(AlunoDto usuario) {
		AlunoResponse alunoResponse = new AlunoResponse();
		alunoResponse.setEmail(usuario.getEmail());
		alunoResponse.setNome(usuario.getNome());
		alunoResponse.setId(usuario.getId());
		alunoResponse.setTurma(usuario.getTurma());
		return alunoResponse;
	}
}
