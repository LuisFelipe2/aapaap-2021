package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.Turma;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.repositorio.UsuarioRepository;
import com.ifrs.appAAPv2.representation.mapper.UserMapper;
import com.ifrs.appAAPv2.representation.request.AuthRequest;
import com.ifrs.appAAPv2.representation.request.UserRequest;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserDetailImpl implements UserDetailsService {

    @Autowired
    UsuarioRepository repository;

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    TurmaService turmaService;

    @Autowired
    PasswordEncoder passwordEncoder;

    UserMapper userMapper = new UserMapper();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = repository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("Não existe esse usuario"));

        return org.springframework.security.core.userdetails.User
                .builder()
                .username(usuario.getEmail())
                .password(usuario.getSenha())
                .authorities(usuario.getId().toString(), usuario.getNome())
                .roles(new String[]{""})
                .build();
    }

    public Usuario salvarUsuario(UserRequest userRequest) throws Exception {

        boolean emailJaCadastrado = usuarioService.buscarUsuarioPorEmail(userRequest.getEmail()).isPresent();
        if(emailJaCadastrado) throw new Exception("Usuario já cadastrado");

        Usuario usuario = userMapper.toDomain(userRequest);

        if(userRequest.getIdTurma() != null && userRequest.getIdTurma() != 0){
            Turma turma = turmaService.buscarTumaPorId(userRequest.getIdTurma());
            List<Turma> turmas = new ArrayList<>();
            turmas.add(turma);
            usuario.setTurma(turmas);
        }

        String senhaCriptografada = critografarSenha(userRequest.getSenha());
        usuario.setSenha(senhaCriptografada);

        return repository.save(usuario);
    }
    public String critografarSenha(String senha){
        return passwordEncoder.encode(senha);
    }

    public UserDetails autenticar(AuthRequest authRequest) {
        UserDetails user = loadUserByUsername(authRequest.getLogin());
        boolean senhaCorreta = passwordEncoder.matches(authRequest.getSenha(), user.getPassword());

        if(senhaCorreta){
            return user;
        }

        throw new UsernameNotFoundException("Credenciais Inválidas");
    }

    public String me() throws Exception {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            throw new Exception("Parece que o usuario  não é instancia de userDetail");
        }
    }
}
