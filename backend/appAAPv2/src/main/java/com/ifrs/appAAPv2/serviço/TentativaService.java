package com.ifrs.appAAPv2.serviço;

import com.ifrs.appAAPv2.domain.Questao;
import com.ifrs.appAAPv2.domain.Tentativa;
import com.ifrs.appAAPv2.domain.Usuario;
import com.ifrs.appAAPv2.repositorio.TentativaRepository;
import com.ifrs.appAAPv2.representation.request.TentativaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TentativaService {

    @Autowired
    TentativaRepository tentativaRepository;
    @Autowired
    UserDetailImpl userDetail;
    @Autowired
    QuestaoService questaoService;
    @Autowired
    UsuarioService usuarioService;

    public Integer salvarTentativa(Integer idQuestao) throws Exception {
        String usuarioEmail = userDetail.me();
        Usuario usuario = usuarioService.buscarUsuarioPorEmail(usuarioEmail).orElseThrow(() -> new Exception("Usuario não cadastrado"));

        Optional<Tentativa> tentativaExistente = existsPorQuestaoIdUserId(idQuestao, usuario.getId());
        if(tentativaExistente.isPresent())
            if(tentativaExistente.get().getResposta()==null)
                return tentativaExistente.get().getId();
            else
                return null;

        Questao questao = questaoService.buscarQuestao(idQuestao);
        Tentativa tentativa = new Tentativa();
        tentativa.setIdQuestao(questao);
        tentativa.setIdUsuario(usuario);

        Tentativa tentativaResponse = tentativaRepository.save(tentativa);

        return tentativaResponse.getId();
    }

    private Optional<Tentativa> existsPorQuestaoIdUserId(Integer idQuestao, Integer id) {
        return tentativaRepository.existsPorQuestaoIdUserId(idQuestao, id);
    }

    public Tentativa responder(TentativaRequest tetativaRequest ) throws Exception {
        Tentativa tentativa = tentativaRepository.findById(tetativaRequest.getIdTentativa()).orElseThrow(() -> new Exception("Não foi pssível localizar tentativa"));
        tentativa.setResposta(tetativaRequest.getResposta());
        return  tentativaRepository.save(tentativa);
    }

    public Optional<Tentativa> buscarTentativaPorQuestaoIdUserId(Integer id_questio, Integer id_user){
        return tentativaRepository.findByIdQuestionAndIdUser(id_questio,id_user);
    }

    public Optional<Tentativa> buscarPorId(Integer idTentativa) {
        return tentativaRepository.findById(idTentativa);
    }

    public Tentativa buscarTentativa(Integer idQuestao) throws Exception {
        String usuarioEmail = userDetail.me();
        Usuario usuario = usuarioService.buscarUsuarioPorEmail(usuarioEmail).orElseThrow(() -> new Exception("Usuario não cadastrado"));
        return existsPorQuestaoIdUserId(idQuestao, usuario.getId()).orElseThrow(() -> new Exception("Usuario não cadastrado"));
    }
}
