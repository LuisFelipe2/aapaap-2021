package com.ifrs.appAAPv2.JwtService;

import com.ifrs.appAAPv2.domain.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;

@Service
public class JwtService {

    private final Long expiracao = 5l;
    private final String chaveAssinatura = "JaMudeiASenha";

    public String gerarToken(Usuario usuario){

        LocalDateTime agora = LocalDateTime.now().plusHours(expiracao);
        Date dataExpiracao = Date.from(agora.atZone(ZoneId.systemDefault()).toInstant());

//        HashMap<String, Object> claims = new HashMap<>();
//        claims.put("nome", usuario.getNome());
//        claims.put("idTurma", usuario.getIdTurma());

        return Jwts.builder()
                .setSubject(usuario.getEmail())
                .setExpiration(dataExpiracao)
//                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, chaveAssinatura)
                .compact();

    }

    public Claims obterClaims(String token) throws ExpiredJwtException {
        return Jwts
                .parser()
                .setSigningKey(chaveAssinatura)
                .parseClaimsJws(token)
                .getBody();
    }

    public String obterLoginUsuario(String token) throws ExpiredJwtException{
        return (String) obterClaims(token).getSubject();
    }

    public boolean tokenValido(String token){
        try{
            Claims claims = obterClaims(token);
            Date dataExpiracao = claims.getExpiration();
            LocalDateTime localDateAtual = dataExpiracao.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return localDateAtual.isAfter(LocalDateTime.now());
        }catch (Exception e){
            return false;
        }
    }


}
