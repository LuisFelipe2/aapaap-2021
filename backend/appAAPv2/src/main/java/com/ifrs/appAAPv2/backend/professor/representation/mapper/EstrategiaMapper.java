package com.ifrs.appAAPv2.backend.professor.representation.mapper;

import com.ifrs.appAAPv2.backend.professor.representation.request.EstrategiaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.EstrategiaResponse;
import com.ifrs.appAAPv2.domain.Estrategia;
import com.ifrs.appAAPv2.domain.Procedimento;
import com.ifrs.appAAPv2.domain.Questao;

import java.util.List;
import java.util.stream.Collectors;

public class EstrategiaMapper {
	public Estrategia toDomain(EstrategiaRequest estrategiaRequest, List<Procedimento> procedimentos, Questao questao) {
		Estrategia estrategia = new Estrategia();
		estrategia.setIdQuestao(questao);
		estrategia.setProcedimentos(procedimentos);
		estrategia.setTexto(estrategiaRequest.getTexto());
		estrategia.setGrauComplexidade(estrategiaRequest.getGrauComplexidade());
		return estrategia;
	}

	public EstrategiaResponse toResponse(Estrategia estrategia) {
		EstrategiaResponse response = new EstrategiaResponse();
		response.setId(estrategia.getId());
		response.setTexto(estrategia.getTexto());
		response.setQuestaoId(estrategia.getIdQuestao().getId());
		response.setGrauComplexidade(estrategia.getGrauComplexidade());
		response.setProcedimentoIds(estrategia.getProcedimentos().stream()
			.map(Procedimento::getId)
			.collect(Collectors.toList()));
		return response;
	}
}
