package com.ifrs.appAAPv2.repositorio;

import com.ifrs.appAAPv2.domain.Conceito;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConceitoRepository extends JpaRepository<Conceito, Integer> {

}
