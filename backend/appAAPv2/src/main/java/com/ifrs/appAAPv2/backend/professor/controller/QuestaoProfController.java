package com.ifrs.appAAPv2.backend.professor.controller;

import com.ifrs.appAAPv2.backend.professor.representation.request.QuestaoRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.QuestaoResponse;
import com.ifrs.appAAPv2.backend.professor.service.QuestaoProfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/privado/professor/questao")
public class QuestaoProfController {

	@Autowired
	QuestaoProfService questaoProfService;

	@PostMapping
	public void criarQuestao(@RequestBody QuestaoRequest questaoRequest) throws Exception {
		questaoProfService.criarQuestao(questaoRequest);
	}
	@GetMapping
	public List<QuestaoResponse> buscarMinhasQuestoes() throws Exception {
		return questaoProfService.buscarMinhasQuestoes();
	}
	@GetMapping("/{turmaId}")
	public List<QuestaoResponse> buscarMinhasQuestoesPorTurma(@PathVariable Integer turmaId) throws Exception {
		return questaoProfService.buscarMinhasQuestoesPorTurma(turmaId);
	}

	@DeleteMapping("/{id}")
	public void deletarTurmaPorId(@PathVariable Integer id) throws Exception {
		questaoProfService.deletarQuestaoPorId(id);
	}
}
