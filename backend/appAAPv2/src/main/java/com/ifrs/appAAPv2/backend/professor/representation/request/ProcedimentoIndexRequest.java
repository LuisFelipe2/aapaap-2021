package com.ifrs.appAAPv2.backend.professor.representation.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcedimentoIndexRequest {
	Integer procedimentoId;
	List<String> ajudasId;
}
