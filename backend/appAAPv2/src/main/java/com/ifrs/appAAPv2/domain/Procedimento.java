package com.ifrs.appAAPv2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Procedimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String mensagem;

    Boolean valido;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="rel_procedimento_ajuda", joinColumns=
            {@JoinColumn(name="id_procedimento")}, inverseJoinColumns=
            {@JoinColumn(name="id_ajuda")})
    List<Ajuda> ajudas;

//    @ManyToMany(mappedBy = "procedimentos")
//    List<Estrategia> estrategias;]

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="rel_questao_procedimento", joinColumns=
        {@JoinColumn(name="id_procedimento")}, inverseJoinColumns=
        {@JoinColumn(name="id_questao")})
    List<Questao> questoesId;

}
