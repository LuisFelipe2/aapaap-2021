package com.ifrs.appAAPv2.backend.professor.controller;

import com.ifrs.appAAPv2.backend.professor.representation.request.TurmaRequest;
import com.ifrs.appAAPv2.backend.professor.representation.response.TurmaResponse;
import com.ifrs.appAAPv2.backend.professor.service.TurmaProfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/privado/professor/turma")
public class TurmaProfController {

	@Autowired
	TurmaProfService turmaProfService;

	@PostMapping
	public void criarTurma(@RequestBody TurmaRequest turmaRequest) throws Exception {
		turmaProfService.CriarTurma(turmaRequest);
	}

	@GetMapping
	public List<TurmaResponse> buscarMinhasTurma() throws Exception {
		return turmaProfService.buscarMinhasTurmas();
	}

	@DeleteMapping("/{id}")
	public void deletarTurmaPorId(@PathVariable Integer id) throws Exception {
		turmaProfService.deletarTurmaPorId(id);
	}
}
